INSERT INTO statetype (id, name, type, rule) VALUES (1, 'Temperature', 'FLOAT', '{}');
INSERT INTO statetype (id, name, type, rule) VALUES (2, 'Luminance', 'FLOAT', '{}');
INSERT INTO statetype (id, name, type, rule) VALUES (3, 'Contact', 'ENUM', '{"values":{"Closed":"0", "Open":"1"}}');
INSERT INTO statetype (id, name, type, rule) VALUES (4, 'Percent', 'INTEGER', '{"minValue":0,"maxValue":100}');
INSERT INTO statetype (id, name, type, rule) VALUES (5, 'Switch', 'ENUM', '{"values":{"Off":"OFF", "On":"ON"}}');
INSERT INTO statetype (id, name, type, rule) VALUES (6, 'Alarm', null, null);