package com.devshot.lifebit.ui.notifications;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import lombok.Getter;
import lombok.Setter;

@Getter
public class NotificationsViewModel extends ViewModel {
    private MutableLiveData<String> events = new MutableLiveData<>();
}