package com.devshot.lifebit.bus;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;

import androidx.core.app.NotificationCompat;

import java.util.concurrent.atomic.AtomicInteger;

import lombok.extern.slf4j.Slf4j;

import static android.content.Context.NOTIFICATION_SERVICE;

@Slf4j(topic = "push")
public class NotificationBus {
    private final AtomicInteger notificationCounter = new AtomicInteger(0);
    private final static String NOTIFICATION_CHANNEL = "ALARMS";
    private final NotificationManager notificationManager;
    private final Context context;

    public NotificationBus(Activity activity) {
        this.notificationManager = (NotificationManager) activity.getSystemService(NOTIFICATION_SERVICE);
        this.context = activity;

        NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL, "Home alarms", NotificationManager.IMPORTANCE_DEFAULT);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        notificationManager.createNotificationChannel(channel);
    }

    public void send(Notification notification) {
        log.info("Android push - {}", notification);
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(notificationCounter.incrementAndGet(), notification);
    }

    public NotificationCompat.Builder builder() {
        return new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL);
    }
}
