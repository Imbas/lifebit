package com.devshot.lifebit.queue;

import android.os.Bundle;

import com.devshot.commons.message.service.MessageConfiguration;

import java.time.Duration;

import lombok.Getter;

/**
 * MQTT client config.
 *
 * @author Oleg Ivashkevich
 * @since 13.07.2017
 */
@Getter
public class EclipsePahoMqttConfig implements MessageConfiguration {
    /**
     * Mqtt client url to server.
     */
    private final String url;

    /**
     * Mqtt client name.
     */
    private final String name;

    /**
     * Mqtt username.
     */
    private final String username;

    /**
     * Mqtt user password.
     */
    private final String userpassword;

    /**
     * Mqtt notification topic.
     */
    private final String notificationTopic;

    /**
     * Mqtt user event topic.
     */
    private final String userEventTopic;

    /**
     * Mqtt reconnect.
     */
    private final Boolean reconnect;

    /**
     * Mqtt reconnect max delay.
     */
    private final Duration reconnectDelay;

    /**
     * Mqtt connection timeout.
     */
    private final Duration timeout;

    public EclipsePahoMqttConfig(Bundle metaConfig) {
        url = metaConfig.getString("mqtt.url");
        name = metaConfig.getString("mqtt.name");
        username = metaConfig.getString("mqtt.user.name");
        userpassword = metaConfig.getString("mqtt.user.password");
        notificationTopic = metaConfig.getString("mqtt.topic.notification");
        userEventTopic = metaConfig.getString("mqtt.topic.userevent");

        reconnect = metaConfig.getBoolean("mqtt.reconnect");
        reconnectDelay = Duration.parse(metaConfig.getString("mqtt.reconnect.delay"));
        timeout = Duration.parse(metaConfig.getString("mqtt.timeout"));
    }
}
