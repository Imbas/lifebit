package com.devshot.lifebit.bus;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.devshot.lifebit.control.model.data.GeoLocation;
import com.devshot.lifebit.control.model.enums.AndroidUserEventType;
import com.devshot.lifebit.service.BEService;

import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "gps")
@RequiredArgsConstructor
public class GPSBus implements LocationListener {
    //FIXME: config
    private final static double RADIUS = 0.0005d;

    @Getter
    private final GPSPermissionInitializer permissionInitializer = new GPSPermissionInitializer();
    private final BEService beService;
    private final EventBus eventBus;

    private final AtomicBoolean home = new AtomicBoolean(false);

    @Override
    public void onLocationChanged(@NonNull Location location) {
        beService.getStatus().thenAccept(serverStatus -> {
            if (serverStatus == null) {
                log.warn("Server status unavailable!");
                return;
            }

            GeoLocation serverLocation = serverStatus.getLocation();

            double diffLatitude = Math.abs(location.getLatitude() - serverLocation.getLatitude());
            double diffLongitude = Math.abs(location.getLongitude() - serverLocation.getLongitude());
            double diffDistance = Math.sqrt(diffLatitude * diffLatitude + diffLongitude * diffLongitude);

            log.info("Calculation GEO - server({}, {}), android({}, {}), distance - {}",
                    serverLocation.getLatitude(), serverLocation.getLongitude(),
                    location.getLatitude(), location.getLongitude(), diffDistance);

            if (diffDistance <= RADIUS) {
                if (home.compareAndSet(false, true)) {
                    log.info("The distance({}) to home lower RADIUS ({}) - user geo near home event", diffDistance, RADIUS);
                    eventBus.pushUserEvent(AndroidUserEventType.GEO_NEAR_HOME, null);
                }
            } else {
                home.set(false);
            }
        });
    }

    public class GPSPermissionInitializer implements ActivityCompat.OnRequestPermissionsResultCallback {
        private final static long LOCATION_REFRESH_TIME = 5000;
        private final static float LOCATION_REFRESH_DISTANCE = 5;
        private final static int PERMISSION_REQUEST_CODE = 1;

        private LocationManager locationManager;

        public void initialize(Activity activity) {
            this.locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_CODE);
            } else {
                initGeo(locationManager);
            }
        }

        @SuppressLint("MissingPermission")
        private void initGeo(LocationManager locationManager) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                    LOCATION_REFRESH_DISTANCE, GPSBus.this);
        }

        @Override
        public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
            if (locationManager == null) {
                return;
            }

            if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == 2
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                initGeo(locationManager);
            }
        }
    }
}
