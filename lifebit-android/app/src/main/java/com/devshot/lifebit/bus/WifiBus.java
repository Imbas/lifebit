package com.devshot.lifebit.bus;

import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;

import androidx.annotation.NonNull;

import com.devshot.lifebit.control.model.enums.AndroidUserEventType;
import com.devshot.lifebit.service.BEService;

import java.util.concurrent.atomic.AtomicBoolean;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "wi-fi")
public class WifiBus {
    private final BEService beService;
    private final EventBus eventBus;

    private final AtomicBoolean home = new AtomicBoolean(false);

    public WifiBus(ConnectivityManager connectivityManager, BEService beService, EventBus eventBus) {
        this.beService = beService;
        this.eventBus = eventBus;

        connectivityManager.registerNetworkCallback(new NetworkRequest.Builder().addTransportType(NetworkCapabilities.TRANSPORT_WIFI).build(),
                new ConnectivityManager.NetworkCallback() {
                    @Override
                    public void onAvailable(@NonNull Network network) {
                        super.onAvailable(network);
                        onWifiConnected();
                    }

                    @Override
                    public void onLost(@NonNull Network network) {
                        super.onLost(network);
                        onWifiDisconnected();
                    }
                });
    }

    public void onWifiConnected() {
        beService.checkLocalAvailability().thenAccept(available -> {
            if (available) {
                log.info("Connection to Wi-Fi network, server is available locally - network home on event");
                eventBus.pushUserEvent(AndroidUserEventType.NETWORK_HOME_ON, null);
                home.set(true);
            }
        });
    }

    //FIXME not system thread
    public void onWifiDisconnected() {
        if (home.compareAndSet(true, false)) {
            log.info("Disconnection from Wi-Fi network, server is unavailable locally - network home off event");
            eventBus.pushUserEvent(AndroidUserEventType.NETWORK_HOME_OFF, null);
        }
    }
}
