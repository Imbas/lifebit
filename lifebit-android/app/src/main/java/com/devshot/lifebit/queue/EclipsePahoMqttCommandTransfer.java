package com.devshot.lifebit.queue;

import android.content.Context;

import com.devshot.commons.general.function.TriConsumer;
import com.devshot.commons.message.service.StringCommandTransfer;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import lombok.extern.slf4j.Slf4j;

import static java.util.Objects.nonNull;

/**
 * Android apache MQTT implementation of {@link StringCommandTransfer}.
 * "channel" is "topic" in Apache MQTT terms.
 */
@Slf4j(topic = "messaging")
//FIXME destroy - mqttClient.close();
public class EclipsePahoMqttCommandTransfer implements StringCommandTransfer {

    /**
     * Map holding consumer for each channel. Currently only one consumer per channel is supported.
     */
    private final Map<String, Consumer<String>> listeners = new ConcurrentHashMap<>();

    /**
     * Main MQTT transfer client.
     */
    private final MqttAndroidClient mqttClient;

    /**
     * Common delayer of tasks, need to be executed after client will be started.
     * ** Created for listeners registration after the async client start up.
     */
    private final CompletableFuture<IMqttToken> startupDelayer = new CompletableFuture<>();

    public EclipsePahoMqttCommandTransfer(Context context, EclipsePahoMqttConfig mqttConfig) {
        this.mqttClient = new MqttAndroidClient(context, mqttConfig.getUrl(), mqttConfig.getName(), new MemoryPersistence(), MqttAndroidClient.Ack.MANUAL_ACK);

        connect(mqttConfig);
    }

    private void connect(EclipsePahoMqttConfig mqttConfig) {
        handle(() -> {
            MqttConnectOptions options = new MqttConnectOptions();
            options.setUserName(mqttConfig.getUsername());
            options.setPassword(mqttConfig.getUserpassword().toCharArray());
            options.setConnectionTimeout((int) mqttConfig.getTimeout().getSeconds());
            options.setCleanSession(true);
            options.setAutomaticReconnect(mqttConfig.getReconnect());
            options.setMaxReconnectDelay((int) mqttConfig.getReconnectDelay().toMillis());
            mqttClient.connect(options, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    log.info("Eclipse Paho client connected");
                    startupDelayer.complete(asyncActionToken);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    log.error("Cannot connect Eclipse Paho client", exception);
                    startupDelayer.completeExceptionally(exception);
                }
            });
        });
    }

    @Override
    public boolean isConnected() {
        return mqttClient.isConnected();
    }

    @Override
    public void send(String command, String channel) {
        startupDelayer.thenAccept(token -> handle(() -> mqttClient.publish(channel, new MqttMessage(command.getBytes()))));
    }

    /**
     * WARNING! This is async topic register!
     *
     * @param listener  to do something (dispatch message), accepts command as string and channel
     * @param channel   channel name
     * @param broadcast flag of listening of channel, true - command will be listen all instances listen,
     */
    @Override
    public synchronized void registerListener(Consumer<String> listener, String channel, boolean broadcast) {
        if (!broadcast) {
            throw new UnsupportedOperationException("Unbroadcast topics is not implemented yet.");
        }

        listeners.computeIfAbsent(channel, key -> {
            registerMqttMessageListener(channel, this::handleCommand);
            return listener;
        });
    }

    /**
     * Invoke all listeners which have registered for certain channel.
     *
     * @param command message
     * @param channel channel
     */
    private void handleCommand(String command, String channel, MqttMessage message) {
        try {
            Consumer<String> consumer = listeners.get(channel);
            if (nonNull(consumer)) {
                consumer.accept(command);
            }
        } catch (Throwable ex) {
            //shouldn't have effect on broadcast channels.
            log.error("Failed with exception: ", ex);
            return;
        }
        try {
            mqttClient.acknowledgeMessage(String.valueOf(message.getId()));
        } catch (Throwable e) {
            log.error("Some error when acknowledge message: ", e);
        }
    }

    /**
     * Creates listener that listens messages directly from MQTT server.
     * When command from given channel comes, given consumer is invoked to process it.
     *
     * @param topic    kafka topic to listen
     * @param consumer consumer that accepts command, topic, ack
     */
    private void registerMqttMessageListener(String topic, TriConsumer<String, String, MqttMessage> consumer) {
        startupDelayer.thenAccept(token -> handle(() -> mqttClient.subscribe(topic, 0, (t, message) -> consumer.accept(message.toString(), t, message))));
    }

    private interface MqttRunnable<T> {
        void action() throws MqttException;
    }

    private void handle(MqttRunnable supplier) {
        try {
            supplier.action();
        } catch (MqttException e) {
            throw new EclipsePahoMqttException(e);
        }
    }

    public static class EclipsePahoMqttException extends RuntimeException {
        public EclipsePahoMqttException(Throwable cause) {
            super(cause);
        }
    }
}
