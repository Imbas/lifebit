package com.devshot.lifebit.http;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import org.springframework.cloud.openfeign.support.SpringMvcContract;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import feign.Feign;
import feign.Retryer;
import feign.auth.BasicAuthRequestInterceptor;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import lombok.RequiredArgsConstructor;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;

@RequiredArgsConstructor
public class FeignClientBuilder {
    private final static Duration TIMEOUT = Duration.ofSeconds(3);
    private final HttpConfig config;

    private final OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(TIMEOUT)
            .readTimeout(TIMEOUT)
            .writeTimeout(TIMEOUT)
            .callTimeout(TIMEOUT)
            .connectionPool(new ConnectionPool(10, 5, TimeUnit.MINUTES))
            .build();

    public <T> T buildClient(Class<T> targetClass, String url, boolean retry) {
        //FIXME copypaste from jCommons
        ObjectMapper json = new ObjectMapper();
        json.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        json.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        json.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        json.registerModule(new JavaTimeModule());
        json.registerModule(new Jdk8Module());

        Feign.Builder builder = Feign.builder()
                .decoder(new JacksonDecoder(json))
                .encoder(new JacksonEncoder(json))
                .client(new feign.okhttp.OkHttpClient(client))
                .contract(new SpringMvcContract())
                .requestInterceptor(new BasicAuthRequestInterceptor(config.getUsername(), config.getUserpassword()));
        if (retry) {
            builder.retryer(new Retryer.Default(1000, 1000, 3));
        } else {
            builder.retryer(Retryer.NEVER_RETRY);
        }

        return builder.target(targetClass, url);
    }
}