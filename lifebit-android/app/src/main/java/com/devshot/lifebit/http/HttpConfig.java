package com.devshot.lifebit.http;

import android.os.Bundle;

import lombok.Getter;

@Getter
public class HttpConfig {

    /**
     * Server http url (local).
     */
    private final String urlLocal;

    /**
     * Server http url (remote).
     */
    private final String urlRemote;

    /**
     * Server http username.
     */
    private final String username;

    /**
     * Server http user password.
     */
    private final String userpassword;

    public HttpConfig(Bundle metaConfig) {
        urlLocal = metaConfig.getString("http.url.local");
        urlRemote = metaConfig.getString("http.url.remote");
        username = metaConfig.getString("http.user.name");
        userpassword = metaConfig.getString("http.user.password");
    }
}
