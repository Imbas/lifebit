package com.devshot.lifebit.service;

import com.devshot.lifebit.control.model.data.ActiveDevice;
import com.devshot.lifebit.http.HttpConfig;
import com.devshot.lifebit.control.model.data.ServerStatus;
import com.devshot.lifebit.http.HttpClients;
import com.devshot.lifebit.model.enums.HomeMode;

import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BEService {
    //FIXME close pool
    private final ExecutorService pool = Executors.newFixedThreadPool(2);
    private final HttpClients httpClients;

    public BEService(HttpConfig config) {
        this.httpClients = new HttpClients(config);
    }

    public CompletableFuture<ServerStatus> getStatus() {
        return CompletableFuture.supplyAsync(() -> httpClients.getStatusRemoteService().status(), pool).exceptionally(ex -> {
            log.error(ex.getMessage(), ex);
            return null;
        });
    }

    public CompletableFuture<Map<String, ActiveDevice>> getDashboard() {
        return CompletableFuture.supplyAsync(() -> httpClients.getDeviceService().dashboard(), pool).exceptionally(ex -> {
            log.error(ex.getMessage(), ex);
            return null;
        });
    }

    public CompletableFuture<Boolean> checkLocalAvailability() {
        return CompletableFuture.supplyAsync(() -> httpClients.getStatusLocalService().status(), pool).thenApply(status -> true).exceptionally(ex -> false);
    }

    public CompletableFuture<Object> setMode(HomeMode mode) {
        return CompletableFuture.supplyAsync(() -> {
            httpClients.getStatusRemoteService().mode(mode);
            return null;
        }, pool).exceptionally(ex -> {
            log.error(ex.getMessage(), ex);
            return null;
        });
    }
}
