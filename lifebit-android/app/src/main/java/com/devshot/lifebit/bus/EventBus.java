package com.devshot.lifebit.bus;

import android.app.Activity;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.devshot.commons.message.converter.XmlCommandConverter;
import com.devshot.commons.message.converter.XmlCommandUUIDConverter;
import com.devshot.commons.message.data.BlankResult;
import com.devshot.commons.message.data.CommandConfiguration;
import com.devshot.commons.message.service.CommandService;
import com.devshot.commons.message.service.impl.CommandServiceImpl;
import com.devshot.lifebit.R;
import com.devshot.lifebit.control.model.enums.AndroidNotificationType;
import com.devshot.lifebit.control.model.enums.AndroidUserEventType;
import com.devshot.lifebit.control.model.message.AndroidNotificationInfo;
import com.devshot.lifebit.control.model.message.AndroidUserEventInfo;
import com.devshot.lifebit.control.model.message.configuration.AndroidNotificationCommandConfiguration;
import com.devshot.lifebit.control.model.message.configuration.AndroidUserEventCommandConfiguration;
import com.devshot.lifebit.queue.EclipsePahoMqttCommandTransfer;
import com.devshot.lifebit.queue.EclipsePahoMqttConfig;
import com.devshot.lifebit.ui.notifications.NotificationsViewModel;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.AnnotationStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.transform.RegistryMatcher;
import org.simpleframework.xml.transform.Transform;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "events")
//FIXME destroy - executors.shutdown();
public class EventBus {

    @Getter
    private final CommandService commandService;
    private final CommandConfiguration<AndroidNotificationInfo, BlankResult> notificationConfiguration;
    private final CommandConfiguration<AndroidUserEventInfo, BlankResult> userEventConfiguration;
    private final NotificationBus notificationBus;

    @Setter
    private NotificationsViewModel notificationsViewModel;

    private final Cache<LocalDateTime, AndroidNotificationInfo> eventCache = CacheBuilder.newBuilder().expireAfterWrite(1, TimeUnit.MINUTES).build();
    private final EclipsePahoMqttConfig config;

    private final ExecutorService executors = Executors.newCachedThreadPool();

    public EventBus(Activity activity, EclipsePahoMqttConfig config, NotificationBus notificationBus) {
        RegistryMatcher matcher = new RegistryMatcher();

        //FIXME: conversion to jCommons
        matcher.bind(ZonedDateTime.class, new Transform<ZonedDateTime>() {
            @Override
            public ZonedDateTime read(String value) {
                return ZonedDateTime.parse(value);
            }

            @Override
            public String write(ZonedDateTime value) {
                return value.toString();
            }
        });

        Serializer xml = new Persister(new AnnotationStrategy(), matcher);
        this.config = config;
        this.commandService = new CommandServiceImpl(new EclipsePahoMqttCommandTransfer(activity, config), config, executors, new XmlCommandConverter(xml), new XmlCommandUUIDConverter(xml));

        this.notificationConfiguration = new AndroidNotificationCommandConfiguration(config.getNotificationTopic());
        this.userEventConfiguration = new AndroidUserEventCommandConfiguration(config.getUserEventTopic());
        this.notificationBus = notificationBus;
    }

    public void subscribe() {
        commandService.registerListener(command -> processEvent(command.getInfo()), notificationConfiguration);
    }

    public void pushUserEvent(AndroidUserEventType type, String info) {
        commandService.send(userEventConfiguration.createCommand(new AndroidUserEventInfo(type, ZonedDateTime.now(), info, config.getName())));
    }

    private void processEvent(AndroidNotificationInfo info) {
        log.info("Android notification here - {}", info);

        if (info.getType() != AndroidNotificationType.ALARM) {
            return;
        }

        LocalDateTime datetime = info.getDate().withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
        eventCache.put(datetime, info);
        if (notificationsViewModel != null) {
            notificationsViewModel.getEvents().postValue(eventCache.asMap().values().stream().map(this::event).reduce("", (e1, e2) -> e1 + "\n" + e2));
        }

        notificationBus.send(notificationBus.builder()
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Home alarm!")
                .setContentText(info.getInfo())
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .build());
    }

    private String event(AndroidNotificationInfo info) {
        return info.getType() + " : " + info.getInfo();
    }
}
