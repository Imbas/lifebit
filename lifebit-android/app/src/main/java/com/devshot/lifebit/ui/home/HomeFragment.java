package com.devshot.lifebit.ui.home;

import android.graphics.Color;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.devshot.lifebit.MainActivity;
import com.devshot.lifebit.R;
import com.devshot.lifebit.model.enums.HomeMode;
import com.devshot.lifebit.service.BEService;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);

        TextView textView = root.findViewById(R.id.home_view);
        textView.setMovementMethod(new ScrollingMovementMethod());
        homeViewModel.getHome().observe(getViewLifecycleOwner(), textView::setText);

        MainActivity activity = (MainActivity) getActivity();
        BEService beService = activity.getBeService();

        Button statusButton = root.findViewById(R.id.status_button);
        statusButton.setOnClickListener(v -> beService.getStatus().thenAccept(status -> homeViewModel.postStatus(status)));

        Button dashboardButton = root.findViewById(R.id.dashboard_button);
        dashboardButton.setOnClickListener(v -> beService.getDashboard().thenAccept(dashboard -> homeViewModel.postDashboard(dashboard)));

        ToggleButton modeButton = root.findViewById(R.id.mode_button);
        modeButton.setOnCheckedChangeListener((v, isChecked) -> {
            beService.setMode(isChecked ? HomeMode.ARMED : HomeMode.DISARMED);
            v.setBackgroundColor(isChecked ? Color.RED : Color.GREEN);
        });

        beService.getStatus().thenAccept(status -> {
            modeButton.setChecked(status.getMode() == HomeMode.ARMED);
            modeButton.setBackgroundColor(status.getMode() == HomeMode.ARMED ? Color.RED : Color.GREEN);
        });

        return root;
    }
}