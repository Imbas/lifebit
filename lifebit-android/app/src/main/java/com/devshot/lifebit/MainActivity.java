package com.devshot.lifebit;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.devshot.lifebit.bus.EventBus;
import com.devshot.lifebit.bus.GPSBus;
import com.devshot.lifebit.bus.NotificationBus;
import com.devshot.lifebit.bus.WifiBus;
import com.devshot.lifebit.http.HttpConfig;
import com.devshot.lifebit.queue.EclipsePahoMqttConfig;
import com.devshot.lifebit.service.BEService;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import lombok.Getter;

public class MainActivity extends AppCompatActivity {

    @Getter
    private BEService beService;

    @Getter
    private NotificationBus notificationBus;

    @Getter
    private EventBus eventBus;

    @Getter
    private GPSBus gpsBus;

    @Getter
    private WifiBus wifiBus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);

        Bundle meta = getMetaConfig(this);

        beService = new BEService(new HttpConfig(meta));

        notificationBus = new NotificationBus(this);
        eventBus = new EventBus(this, new EclipsePahoMqttConfig(meta), notificationBus);
        eventBus.subscribe();

        gpsBus = new GPSBus(beService, eventBus);
        gpsBus.getPermissionInitializer().initialize(this);

        wifiBus = new WifiBus((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE), beService, eventBus);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        gpsBus.getPermissionInitializer().onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private Bundle getMetaConfig(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA).metaData;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}