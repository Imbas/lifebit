package com.devshot.lifebit.ui.home;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.devshot.lifebit.control.model.data.ActiveDevice;
import com.devshot.lifebit.control.model.data.ServerStatus;

import java.util.Map;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
public class HomeViewModel extends ViewModel {
    private final MutableLiveData<String> home = new MutableLiveData<>();

    public void postDashboard(Map<String, ActiveDevice> dashboard) {
        StringBuilder builder = new StringBuilder();
        dashboard.forEach((label, device) -> {
            builder.append(label).append(":").append("\n");
            device.getComponents().forEach(component -> builder.append("  - ").append(component.getComponent().getName()).append("\t\t  -  ").append(component.getState()).append("\n"));
        });

        home.postValue(builder.toString());
    }

    public void postStatus(ServerStatus status) {
        home.postValue(status.toString());
    }
}