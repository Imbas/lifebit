package com.devshot.lifebit.http;

import com.devshot.lifebit.control.api.DeviceApi;
import com.devshot.lifebit.control.api.StatusApi;

import lombok.Getter;

@Getter
public class HttpClients {
    private final FeignClientBuilder clientBuilder;

    private final StatusApi statusRemoteService;
    private final StatusApi statusLocalService;
    private final DeviceApi deviceService;

    public HttpClients(HttpConfig config) {
        this.clientBuilder = new FeignClientBuilder(config);
        this.statusRemoteService = clientBuilder.buildClient(StatusApi.class, config.getUrlRemote(), false);
        this.statusLocalService = clientBuilder.buildClient(StatusApi.class, config.getUrlLocal(), false);
        this.deviceService = clientBuilder.buildClient(DeviceApi.class, config.getUrlRemote(), false);
    }
}
