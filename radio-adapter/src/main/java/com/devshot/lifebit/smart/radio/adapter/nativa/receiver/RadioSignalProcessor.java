package com.devshot.lifebit.smart.radio.adapter.nativa.receiver;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPinDigitalInput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;

import java.util.List;

/**
 * @author Oleg Ivashkevich
 * date: 14.09.2017
 */
public class RadioSignalProcessor {

    private final List<RadioSignalHandler> handlers;
    private final GpioPinDigitalInput input;
    private final RadioSignalAsyncDecoder decoder = new RadioSignalAsyncDecoder();

    public RadioSignalProcessor(GpioController controller, List<RadioSignalHandler> handlers, Pin pin) {
        this.handlers = handlers;

        // provision gpio pin as an input pin with its internal pull down resistor enabled
        input = controller.provisionDigitalInputPin(pin, PinPullResistance.PULL_DOWN);

        // set shutdown state for this input pin
        input.setShutdownOptions(true);

        // create and register gpio pin listener
        input.addListener((GpioPinListenerDigital) this::processStateChange);
    }

    private void processStateChange(GpioPinDigitalStateChangeEvent event) {
        Integer result = decoder.pinStateChange(event.getState());
        if (result != null) {
            handleResult(result);
        }
    }

    private void handleResult(int result) {
        //TODO: not in GPIO thread please
        if (handlers != null)
            handlers.forEach(handler -> handler.handle(result));
    }

    // stop all GPIO activity/threads by shutting down the GPIO controller
    // (this method will forcefully shutdown all GPIO monitoring threads and scheduled tasks)
    // controller.shutdown();   <--- implement this method call if you wish to terminate the Pi4J GPIO controller
}
