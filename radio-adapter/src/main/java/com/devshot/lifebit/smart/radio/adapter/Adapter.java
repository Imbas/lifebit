package com.devshot.lifebit.smart.radio.adapter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@EnableAutoConfiguration
@ComponentScan("com.devshot.lifebit.smart.radio.configuration")
public class Adapter {
    private static ConfigurableApplicationContext context;

    public static void main(String[] args) {
        context = SpringApplication.run(Adapter.class, args);
    }
}