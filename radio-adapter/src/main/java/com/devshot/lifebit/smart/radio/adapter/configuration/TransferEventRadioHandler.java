package com.devshot.lifebit.smart.radio.adapter.configuration;

import com.devshot.commons.message.service.CommandService;
import com.devshot.lifebit.smart.radio.adapter.nativa.receiver.RadioSignalHandler;
import com.devshot.lifebit.smart.radio.message.RadioEventCommandConfiguration;
import com.devshot.lifebit.smart.radio.model.RadioEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @author Oleg Ivashkevich
 * date: 12.09.2020
 */
@Component
@RequiredArgsConstructor
public class TransferEventRadioHandler implements RadioSignalHandler {
    private final CommandService commandService;
    private final RadioEventCommandConfiguration configuration;

    @Override
    public void handle(int signal) {
        //TODO cache
        commandService.send(configuration.createCommand(new RadioEvent(String.valueOf(signal), System.currentTimeMillis())));
    }
}
