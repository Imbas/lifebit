package com.devshot.lifebit.smart.radio.adapter.nativa.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;
import java.util.List;

/**
 * @author Oleg Ivashkevich
 * date: 13.10.2018
 */
@Data
@AllArgsConstructor
public class Protocol {
    private final String name;
    private final int pulseLength;
    private final int syncFactorLow;
    private final int syncFactorHigh;
    private final int zeroLow;
    private final int zeroHigh;
    private final int oneLow;
    private final int oneHigh;

    public final static Protocol PROTOCOL_1 = new Protocol("PROTOCOL_1",350, 31, 1, 3, 1, 1, 3);
    public final static Protocol PROTOCOL_2 = new Protocol("PROTOCOL_2",650, 10, 1, 2, 1, 1, 2);
    public final static Protocol PROTOCOL_3 = new Protocol("PROTOCOL_3",100, 71, 30, 11, 4, 6, 9);
    public final static Protocol PROTOCOL_4 = new Protocol("PROTOCOL_4",380, 6, 1, 3, 1, 1, 3);
    public final static Protocol PROTOCOL_5 = new Protocol("PROTOCOL_5",500, 14, 6, 2, 1, 1, 2);

    public final static List<Protocol> PROTOCOLS = Arrays.asList(PROTOCOL_1, PROTOCOL_2, PROTOCOL_3, PROTOCOL_4, PROTOCOL_5);
}
