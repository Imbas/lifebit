package com.devshot.lifebit.smart.radio.adapter.nativa.receiver;

import com.devshot.lifebit.smart.radio.adapter.nativa.model.Protocol;
import com.pi4j.io.gpio.PinState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * **All time variables in microseconds.
 *
 * @author Oleg Ivashkevich
 * date: 06.11.2017
 */
public class RadioSignalAsyncDecoder {
    private final static Logger LOG = LoggerFactory.getLogger("radio");

    private final static int RCSWITCH_MAX_CHANGES = 67;
    private final static long MAX_PAUSE = 4600;
    private final static long RECEIVE_TOLERANCE = 60;
    private final static long LAST_FIRST_DIFF_MAX = 200;

    private final static List<Long> timings = new ArrayList<>();
    private int repeats = 0;
    private AtomicLong lastSwitchTime = new AtomicLong(0);

    private final Lock sync = new ReentrantLock();

    public Integer pinStateChange(PinState state) {
        long now = TimeUnit.NANOSECONDS.toMicros(System.nanoTime());
        long duration = now - lastSwitchTime.get();
        lastSwitchTime.set(now);

        sync.lock();
        try {
            return logic(duration);
        } finally {
            sync.unlock();
        }
    }

    private Integer logic(long duration) {
        if (timings.size() >= RCSWITCH_MAX_CHANGES) {
            LOG.warn("RC-SWITCH max change exceed ({})", RCSWITCH_MAX_CHANGES);
            return refresh(duration);
        }

        if (duration <= MAX_PAUSE) {
            //main data process - to keep all timings
            timings.add(duration);
            return null;
        }

        if (timings.size() <= 7) {
            LOG.info("Skipping 4bit value");
            return refresh(duration);
        }

        if (Math.abs(duration - timings.get(0)) >= LAST_FIRST_DIFF_MAX) {
            LOG.info("Skipping unlike value by first last difference");
            return refresh(duration);
        }

        repeats++;

        if (repeats != 2) {
            LOG.info("Skipping value for repeat");
            return refresh(duration);
        }

        //finally - receive target value

        LOG.info("Protocol timings' set received - {}", timings);

        Integer result = null;
        for (Protocol protocol : Protocol.PROTOCOLS) {
            result = receiveProtocol(protocol, timings);
            if (result != null) {
                LOG.info("Protocol received - {}", protocol.getName());
                break;
            }
        }

        if (result == null) {
            LOG.error("Failed to parse protocol");
        }

        repeats = 0;

        refresh(duration);
        return result;
    }

    private Integer refresh(long current) {
        timings.clear();
        timings.add(current);
        return null;
    }

    private Integer receiveProtocol(Protocol protocol, List<Long> timings) {
        int changeCount = timings.size();

        long code = 0;
        long delay = timings.get(0) / protocol.getSyncFactorLow();
        long delayTolerance = delay * RECEIVE_TOLERANCE / 100;

        for (int i = 1; i < changeCount - 1; i = i + 2) {
            long one = timings.get(i);
            long two = timings.get(i + 1);

            code <<= 1;

            if (Math.abs(one - delay * protocol.getZeroHigh()) < delayTolerance
                    && Math.abs(two - delay * protocol.getZeroLow()) < delayTolerance) {
                //zero
            } else if (Math.abs(one - delay * protocol.getOneHigh()) < delayTolerance
                    && Math.abs(two - delay * protocol.getOneLow()) < delayTolerance) {
                //one
                code |= 1;
            } else {
                // Failed
                return null;
            }
        }
        //missed some global changes

        return (int) code;
    }
}
