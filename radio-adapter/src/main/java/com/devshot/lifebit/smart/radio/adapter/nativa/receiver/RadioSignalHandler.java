package com.devshot.lifebit.smart.radio.adapter.nativa.receiver;

/**
 * @author Oleg Ivashkevich
 * date: 06.11.2017
 */
public interface RadioSignalHandler {
    void handle(int signal);
}
