package com.devshot.lifebit.smart.radio.adapter.nativa.configuration;

import com.devshot.lifebit.smart.radio.adapter.nativa.receiver.RadioSignalHandler;
import com.devshot.lifebit.smart.radio.adapter.nativa.receiver.RadioSignalProcessor;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.RaspiPin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * Main radio receiver configuration - GPIO control.
 *
 * @author Oleg Ivashkevich
 * date: 14.09.2017
 */
@Configuration
public class RadioReceiverContextConfiguration {
    @Bean
    public GpioController gpioController() {
        return GpioFactory.getInstance();
    }

    @Bean
    public RadioSignalProcessor radioSignalProcessor(GpioController controller,
                                                     @Autowired(required = false) List<RadioSignalHandler> handlers) {
        //TODO: configuration for pin number
        return new RadioSignalProcessor(controller, handlers, RaspiPin.GPIO_02);
    }
}
