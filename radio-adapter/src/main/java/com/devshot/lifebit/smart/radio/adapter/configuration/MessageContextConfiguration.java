package com.devshot.lifebit.smart.radio.adapter.configuration;

import com.devshot.commons.general.GeneralContextConfiguration;
import com.devshot.commons.message.mqtt.configuration.EclipsePahoMqttContextConfiguration;
import com.devshot.lifebit.smart.radio.message.RadioEventCommandConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Oleg Ivashkevich
 * date: 08.05.2020
 */
@Configuration
@Import({
        GeneralContextConfiguration.class,
        MessageContextConfiguration.class,
        EclipsePahoMqttContextConfiguration.class,
        RadioEventCommandConfiguration.class
})
public class MessageContextConfiguration {
}
