package sources;

import com.pi4j.io.gpio.PinState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * **All time variables in microseconds.
 *
 * @author Oleg Ivashkevich
 * date: 06.11.2017
 */
public class RadioSignalAsyncDecoderOld {
    private final static Logger LOG = LoggerFactory.getLogger("radio");

    private final static int RCSWITCH_MAX_CHANGES = 67;
    private final static long MAX_PAUSE = 4600;
    private final static long RECEIVE_TOLERANCE = 60;
    private final static long LAST_FIRST_DIFF_MAX = 200;

    private volatile int changeCount = 0;
    private volatile long lastTime = 0;
    private volatile int repeatCount = 0;

    private final int[] timings = new int[RCSWITCH_MAX_CHANGES];

    public Integer pinStateChange(PinState state) {

        long time = System.nanoTime() / 1000;
        int duration = (int) (time - lastTime);

        Integer result = null;

        if (duration > MAX_PAUSE) {
            // A long stretch without signal level change occurred. This could
            // be the gap between two transmission.
            if (diff(duration, timings[0]) < LAST_FIRST_DIFF_MAX) {
                // This long signal is close in length to the long signal which
                // started the previously recorded timings; this suggests that
                // it may indeed by a a gap between two transmissions (we assume
                // here that a sender will send the signal multiple times,
                // with roughly the same gap between them).
                repeatCount++;
                if (repeatCount == 2) {
                    for (Protocol protocol : Protocol.PROTOCOLS) {
                        result = receiveProtocol(changeCount, protocol);
                        if (result != null) {
                            // receive succeeded for protocol i
                            break;
                        }
                    }
                    repeatCount = 0;
                }
            }
            changeCount = 0;
        }

        // detect overflow
        if (changeCount >= RCSWITCH_MAX_CHANGES) {
            changeCount = 0;
            repeatCount = 0;
        }

        timings[changeCount++] = duration;
        lastTime = time;

        return result != null && result != 0 ? result : null;
    }

    private Integer receiveProtocol(int changeCount, Protocol protocol) {

        long code = 0;
        int delay = timings[0] / protocol.getSyncFactorLow();
        int delayTolerance = (int) (delay * RECEIVE_TOLERANCE / 100);

        for (int i = 1; i < changeCount - 1; i += 2) {
            code <<= 1;
            if (diff(timings[i], delay * protocol.getZeroHigh()) < delayTolerance &&
                    diff(timings[i + 1], delay * protocol.getZeroLow()) < delayTolerance) {
                // zero
            } else if (diff(timings[i], delay * protocol.getOneHigh()) < delayTolerance &&
                    diff(timings[i + 1], delay * protocol.getOneLow()) < delayTolerance) {
                // one
                code |= 1;
            } else {
                // Failed
                return null;
            }
        }

        if (changeCount > 7) {    // ignore very short transmissions: no device sends them, so this must be noise
            LOG.info("Protocol received - {}", protocol.getName());
            return (int) code;
        }

        return 0;
    }

    private static int diff(int a, int b) {
        return Math.abs(a - b);
    }
}
