package com.devshot.lifebit.control.service.impl.openhab;

import com.devshot.commons.x_model.jpa.Namer;
import com.devshot.lifebit.common.dao.ComponentTypeDAO;
import com.devshot.lifebit.common.dao.DeviceTypeDAO;
import com.devshot.lifebit.control.converter.OpenhabThingToDeviceConverter;
import com.devshot.lifebit.control.model.pojo.Device;
import com.devshot.lifebit.control.model.pojo.Location;
import com.devshot.lifebit.model.enums.DeviceProtocol;
import com.devshot.lifebit.model.pojo.ComponentType;
import com.devshot.lifebit.model.pojo.DeviceModel;
import com.devshot.lifebit.smart.openhab.remote.api.OpenhabItems;
import com.devshot.lifebit.smart.openhab.remote.api.OpenhabLinks;
import com.devshot.lifebit.smart.openhab.remote.api.OpenhabThings;
import com.devshot.lifebit.smart.openhab.remote.model.OpenhabChannel;
import com.devshot.lifebit.smart.openhab.remote.model.OpenhabItem;
import com.devshot.lifebit.smart.openhab.remote.model.OpenhabThing;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Special openhab things manager.
 *
 * @author Oleg Ivashkevich
 * date: 24.07.2017
 */
@Component
@RequiredArgsConstructor
public class OpenhabThingManager {
    private final OpenhabThings openhabThings;
    private final OpenhabItems openhabItems;
    private final OpenhabLinks openhabLinks;

    private final OpenhabThingToDeviceConverter thingToDeviceConverter;

    private final DeviceTypeDAO deviceTypeDAO;
    private final ComponentTypeDAO componentTypeDAO;

    public List<Device> getDevices() {
        List<OpenhabThing> things = openhabThings.getAll();
        List<Device> devices = thingToDeviceConverter.convert(things);

        //device models connection
        Map<String, DeviceModel> types = deviceTypeDAO.findAllByProtocol(DeviceProtocol.ZWAVE)
                .stream().collect(Collectors.toMap(DeviceModel::getExternalId, Function.identity()));
        devices = devices.stream().peek(device -> device.setModel(types.get(device.getModel().getExternalId())))
                .filter(device -> device.getModel() != null).collect(Collectors.toList());

        //device component types connection
        Map<String, ComponentType> componentTypes = componentTypeDAO.findAll()
                .stream().collect(Collectors.toMap(ComponentType::getExternalId, Function.identity()));
        devices.forEach(device -> {
            device.setComponents(device.getComponents().stream()
                    .peek(component -> component.setType(componentTypes.get(component.getType().getExternalId())))
                    .filter(component -> component.getType() != null).collect(Collectors.toList()));
        });

        //location connection
        Map<String, Location> locations =
                devices.stream().map(Device::getLocation).collect(Collectors.toMap(Namer::getName, Function.identity(), (l, r) -> l));
        devices.forEach(device -> {
            device.setLocation(locations.get(device.getLocation().getName()));
            if (device.getLocation().getName() == null) {
                device.getLocation().setName("Other");
            }
        });

        return devices;
    }

    /**
     * Функционал создания новых items для каналов устройства
     *
     * @param thing устройство
     * @return созданные items (пары связок канал - item)
     */
    @Deprecated
    public List<Pair<OpenhabChannel, OpenhabItem>> autoCreateLinks(OpenhabThing thing) {
        //создание и привязка item для каналов
        List<Pair<OpenhabChannel, OpenhabItem>> links = new ArrayList<>();
        for (OpenhabChannel channel : thing.getChannels()) {
            //рассмотрение глюка неудалённого item, привязывающегося автоматически
            if (!channel.getLinkedItemNames().isEmpty()) {
                String name = channel.getLinkedItemNames().get(0);
                openhabLinks.removeFromChannel(name, channel.getUid());
                //TODO: какие-то проблемы на вторичном добавлении - не удаляется item
                openhabItems.remove(name);
            }

            OpenhabItem item = new OpenhabItem();
            {
                item.setName(makeItemName(thing.getUID(), channel.getId()));
                item.setLabel(channel.getItemType());
                item.setType(channel.getItemType());
            }
            openhabItems.save(item.getName(), item);
            openhabLinks.addToChannel(item.getName(), channel.getUid());
            links.add(Pair.of(channel, item));
        }

        return links;
    }

    /**
     * Удаление устройства и его item связок
     * * игнорируется 404 ошибка, нету - значит удалён ранее
     *
     * @param thingUUID uuid устройства
     */
    @Deprecated
    public void removeThing(String thingUUID) {
        OpenhabThing thing;
        try {
            thing = openhabThings.get(thingUUID);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND)
                return;
            throw e;
        }

        openhabThings.remove(thingUUID, true);

        thing.getChannels().forEach(channel -> {
            if (!channel.getLinkedItemNames().isEmpty())
                openhabItems.remove(channel.getLinkedItemNames().get(0));
        });
    }

    private String makeItemName(String thingUUID, String channelId) {
        return clearNameForItem(thingUUID) + "_" + clearNameForItem(channelId);
    }

    private String clearNameForItem(String source) {
        return RegExUtils.replacePattern(source, "[^a-zA-Z0-9_]", "_");
    }
}
