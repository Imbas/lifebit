package com.devshot.lifebit.control.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@Configuration
@ComponentScan("com.devshot.lifebit.control.converter")
public class ConverterContextConfiguration {
}
