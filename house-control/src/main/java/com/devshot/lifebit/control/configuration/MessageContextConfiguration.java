package com.devshot.lifebit.control.configuration;

import com.devshot.lifebit.control.config.AndroidConfig;
import com.devshot.lifebit.control.model.message.configuration.AndroidUserEventCommandConfiguration;
import com.devshot.lifebit.control.model.message.configuration.AndroidNotificationCommandConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Message ActiveMQ initialization.
 *
 * @author Oleg Ivashkevich
 * date: 26.10.2020
 */
@Configuration
@ComponentScan("com.devshot.lifebit.control.message")
public class MessageContextConfiguration {

    /**
     * Outgoing message queue configuration - for OUT android messages.
     */
    @Bean
    public AndroidNotificationCommandConfiguration androidNotificationCommandConfiguration(AndroidConfig config) {
        return new AndroidNotificationCommandConfiguration(config.getAndroidNotificationTopic());
    }

    /**
     * Incoming message queue configuration - for IN android messages.
     */
    @Bean
    public AndroidUserEventCommandConfiguration androidUserEventCommandConfiguration(AndroidConfig config) {
        return new AndroidUserEventCommandConfiguration(config.getAndroidUserEventTopic());
    }
}
