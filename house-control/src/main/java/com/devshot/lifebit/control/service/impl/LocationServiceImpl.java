package com.devshot.lifebit.control.service.impl;

import com.devshot.lifebit.control.dao.LocationDAO;
import com.devshot.lifebit.control.model.pojo.Location;
import com.devshot.lifebit.control.service.LocationService;
import com.devshot.lifebit.control.service.abstraction.AbstractServiceJpaAdapter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 * Сервис по работе с расположением устройств
 *
 * @author Oleg Ivashkevich
 * date: 22.07.2017
 */
@Service
@RequiredArgsConstructor
public class LocationServiceImpl implements LocationService, AbstractServiceJpaAdapter<Integer, Location> {
    private final LocationDAO locationDAO;

    @Override
    public JpaRepository<Location, Integer> repository() {
        return locationDAO;
    }
}
