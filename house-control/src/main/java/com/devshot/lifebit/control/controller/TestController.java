package com.devshot.lifebit.control.controller;

import com.devshot.commons.message.service.CommandService;
import com.devshot.lifebit.control.model.enums.AndroidNotificationType;
import com.devshot.lifebit.control.model.message.AndroidNotificationInfo;
import com.devshot.lifebit.control.model.message.configuration.AndroidNotificationCommandConfiguration;
import com.devshot.lifebit.control.model.pojo.Event;
import com.devshot.lifebit.control.service.impl.event.EventBUS;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;
import java.util.Collection;

/**
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestController {
    private final CommandService commandService;
    private final AndroidNotificationCommandConfiguration eventCommandConfiguration;
    private final EventBUS eventBUS;

    @PutMapping("/event")
    public void event(@RequestBody String event) {
        commandService.send(eventCommandConfiguration.createCommand(new AndroidNotificationInfo(AndroidNotificationType.ALARM, ZonedDateTime.now()
                , event)));
    }

    @GetMapping("/event")
    public Collection<Event> events() {
        return eventBUS.getCache().asMap().values();
    }
}
