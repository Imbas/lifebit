package com.devshot.lifebit.control.service.impl.command;

import com.devshot.lifebit.control.dao.ComponentDAO;
import com.devshot.lifebit.control.exception.ScenarioExecutionException;
import com.devshot.lifebit.control.service.ScenarioCommandAction;
import com.devshot.lifebit.model.domain.command.ScenarioDeviceCommand;
import com.devshot.lifebit.model.enums.CommandBase;
import com.devshot.lifebit.model.enums.ScenarioCommandType;
import com.devshot.lifebit.smart.openhab.remote.api.OpenhabItems;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author Oleg Ivashkevich
 * date: 27.08.2020
 */
@Component
@RequiredArgsConstructor
@Slf4j(topic = "action")
public class ScenarioDeviceCommandAction implements ScenarioCommandAction<ScenarioDeviceCommand> {
    private final ComponentDAO componentDAO;

    // -------------- REMOTE -------------- //
    private final OpenhabItems openhabItems;
    // ------------------------------------ //

    @Override
    public ScenarioCommandType getType() {
        return ScenarioCommandType.SCENARIO_DEVICE;
    }

    @Override
    public void execute(ScenarioDeviceCommand command) {
        if (command.getBase() != CommandBase.SET) {
            throw new ScenarioExecutionException("Only SET commands are supported!");
        }
        com.devshot.lifebit.control.model.pojo.Component component = componentDAO.findByScenarioDeviceId(command.getScenarioDeviceId());
        if (component == null) {
            throw new ScenarioExecutionException("No component for scenario device!");
        }

        //FIXME state conversion : component.getType().getStateType().getRule().getValues()
        openhabItems.sendCommand(component.getExternalId(), command.getState());
        log.info("Command {} - sent to device ({}) component({})", command, component.getDevice().getName(), component.getName());
    }
}
