package com.devshot.lifebit.control.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Configuration
@EntityScan({
        "com.devshot.lifebit.control.model.pojo",
        "com.devshot.lifebit.control.converter.jpa"
})
@EnableJpaRepositories("com.devshot.lifebit.control.dao")
public class DaoContextConfiguration {
}