package com.devshot.lifebit.control.service.impl.command;

import com.devshot.lifebit.control.dao.ComponentDAO;
import com.devshot.lifebit.control.service.ScenarioCommandAction;
import com.devshot.lifebit.model.domain.command.ScenarioAllLightsOffCommand;
import com.devshot.lifebit.model.enums.ScenarioCommandType;
import com.devshot.lifebit.model.enums.ScenarioDeviceType;
import com.devshot.lifebit.model.enums.StateType;
import com.devshot.lifebit.smart.openhab.remote.api.OpenhabItems;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Oleg Ivashkevich
 * date: 27.08.2020
 */
@Component
@RequiredArgsConstructor
@Slf4j(topic = "action")
public class ScenarioAllLightsOffCommandAction implements ScenarioCommandAction<ScenarioAllLightsOffCommand> {
    private final ComponentDAO componentDAO;

    // -------------- REMOTE -------------- //
    private final OpenhabItems openhabItems;
    // ------------------------------------ //

    @Override
    public ScenarioCommandType getType() {
        return ScenarioCommandType.ALL_LIGHTS_OFF;
    }

    @Override
    public void execute(ScenarioAllLightsOffCommand command) {
        List<com.devshot.lifebit.control.model.pojo.Component> components = componentDAO.findByScenarioDeviceType(ScenarioDeviceType.LIGHT);
        components.forEach(component -> {
            String state = chooseOffState(component.getType().getStateType());
            if (state == null) {
                return;
            }
            openhabItems.sendCommand(component.getExternalId(), state);
            log.info("Off command state {} - sent to device ({}) component({})", state, component.getDevice().getName(), component.getName());
        });
    }

    //FIXME: openhub constants
    private String chooseOffState(StateType stateType) {
        switch (stateType) {
            case SWITCH: return "OFF";
            case PERCENT: return "0";
            default: log.warn("Cannot perform state for non executable device component"); return null;
        }
    }
}
