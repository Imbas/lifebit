package com.devshot.lifebit.control.dao;

import com.devshot.lifebit.control.model.pojo.Component;
import com.devshot.lifebit.model.enums.ScenarioDeviceType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Oleg Ivashkevich
 * date: 17.07.2017
 */
@Repository
public interface ComponentDAO extends JpaRepository<Component, Integer> {
    Component findByExternalId(String externalId);

    Component findByScenarioDeviceId(Integer scenarioDeviceId);

    List<Component> findByScenarioDeviceType(ScenarioDeviceType type);
}
