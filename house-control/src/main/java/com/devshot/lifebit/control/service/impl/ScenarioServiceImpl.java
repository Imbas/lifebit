package com.devshot.lifebit.control.service.impl;

import com.devshot.lifebit.common.dao.ScenarioDAO;
import com.devshot.lifebit.control.model.pojo.Component;
import com.devshot.lifebit.control.model.pojo.Event;
import com.devshot.lifebit.control.service.ScenarioCommandAction;
import com.devshot.lifebit.control.service.ScenarioService;
import com.devshot.lifebit.control.service.abstraction.AbstractServiceJpaAdapter;
import com.devshot.lifebit.control.service.abstraction.EventBusProcessor;
import com.devshot.lifebit.model.domain.ScenarioConditionCheckBox;
import com.devshot.lifebit.model.enums.ScenarioCommandType;
import com.devshot.lifebit.model.pojo.Scenario;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Scenario execution main service.
 *
 * @author Oleg Ivashkevich
 * date: 22.07.2017
 */
@Service
@Slf4j
public class ScenarioServiceImpl implements ScenarioService, AbstractServiceJpaAdapter<Integer, Scenario>, EventBusProcessor {
    private final ScenarioDAO scenarioDAO;
    private final ThreadPoolTaskExecutor executor;
    private final HomeInfoHolder homeInfoHolder;
    private final Map<ScenarioCommandType, ScenarioCommandAction> actions;

    private final ConcurrentMap<Integer, Pair<Scenario, Future>> activeFlowScenarios = new ConcurrentHashMap<>();

    public ScenarioServiceImpl(ScenarioDAO scenarioDAO,
                               ThreadPoolTaskExecutor executor,
                               HomeInfoHolder homeInfoHolder,
                               List<ScenarioCommandAction> actions) {
        this.scenarioDAO = scenarioDAO;
        this.executor = executor;
        this.homeInfoHolder = homeInfoHolder;
        this.actions = actions.stream().collect(Collectors.toMap(ScenarioCommandAction::getType, Function.identity()));
        //TODO: availibility check! all enums
    }

    @Override
    public JpaRepository<Scenario, Integer> repository() {
        return scenarioDAO;
    }

    @Override
    public void execute(Scenario scenario) {
        log.info("Scenario {} execution started", scenario.getName());

        scenario.getCommands().forEach(command -> {
            ScenarioCommandAction action = actions.get(command.getType());
            //noinspection unchecked
            action.execute(command);
            log.info("Scenario {} - command {} executed", scenario.getName(), command);
        });
        if (scenario.getFlow() != null) {
            activeFlowScenarios.remove(scenario.getFlow());
        }

        log.info("Scenario {} execution finished", scenario.getName());
    }

    // ------------------------------------ EXECUTIONS BY EVENTS ----------------------------------

    @Override
    public void process(Event event) {
        //get all cached scenarios
        List<Scenario> scenarios = getAll();

        scenarios.forEach(scenario -> {
            if (!scenarioCheck(scenario, event)) {
                return;
            }
            log.info("Scenario {} was granted for execution", scenario.getName());
            //in a case of flow scenarios - new scenario cancels old one
            if (scenario.getFlow() != null) {
                activeFlowScenarios.compute(scenario.getFlow(), (flow, oldScenario) -> {
                    if (oldScenario != null) {
                        oldScenario.getValue().cancel(true);
                        log.info("Old scenario {} from flow {} was cancelled from execution by new one {}",
                                oldScenario.getKey().getName(), scenario.getFlow(), scenario.getName());
                    }
                    return Pair.of(scenario, executor.submit(() -> execute(scenario)));
                });
            } else {
                CompletableFuture.runAsync(() -> execute(scenario), executor);
            }
        });
    }

    private boolean scenarioCheck(Scenario scenario, Event event) {
        ScenarioConditionCheckBox box = ScenarioConditionCheckBox.builder()
                .targetEventType(event.getType())
                .device(Optional.ofNullable(event.getComponent()).orElse(new Component()).getScenarioDevice())
                .usersAtHome(homeInfoHolder.getUsersAtHome())
                .homeMode(homeInfoHolder.getMode().get())
                .build();
        return scenario.isEnabled() && scenario.getCondition().check(box)
                && (scenario.getSchedule() == null || scenario.getSchedule().check());
    }
}
