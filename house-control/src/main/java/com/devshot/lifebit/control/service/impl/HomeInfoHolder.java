package com.devshot.lifebit.control.service.impl;

import com.devshot.lifebit.model.enums.HomeMode;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author Oleg Ivashkevich
 * date: 15.11.2020
 */
@Getter
@Component
public class HomeInfoHolder {
    private final Set<String> usersAtHome = ConcurrentHashMap.newKeySet();
    private final AtomicReference<HomeMode> mode = new AtomicReference<>(HomeMode.DISARMED);
}
