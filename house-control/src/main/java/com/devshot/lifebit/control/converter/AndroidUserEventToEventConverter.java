package com.devshot.lifebit.control.converter;

import com.devshot.lifebit.control.model.enums.EventState;
import com.devshot.lifebit.control.model.message.AndroidUserEventInfo;
import com.devshot.lifebit.control.model.pojo.Event;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * @author Oleg Ivashkevich
 * date: 27.10.2018
 */
@Mapper(componentModel = "spring", imports = EventState.class)
public interface AndroidUserEventToEventConverter {
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "data", source = "info")
    @Mapping(target = "registerDate", source = "date")
    @Mapping(target = "state", constant = "APPROVED")
    Event convert(AndroidUserEventInfo event);
}
