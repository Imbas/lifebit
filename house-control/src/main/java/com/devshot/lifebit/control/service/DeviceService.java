package com.devshot.lifebit.control.service;

import com.devshot.commons.general.service.AbstractService;
import com.devshot.lifebit.control.model.data.ActiveComponent;
import com.devshot.lifebit.control.model.data.ActiveDevice;
import com.devshot.lifebit.control.model.domain.DeviceCandidate;
import com.devshot.lifebit.control.model.pojo.Device;
import com.devshot.lifebit.model.domain.command.DeviceCommand;
import com.devshot.lifebit.model.enums.DeviceProtocol;
import com.devshot.lifebit.model.pojo.DeviceModel;

import java.util.List;
import java.util.Map;

/**
 * Device control service.
 *
 * @author Oleg Ivashkevich
 * date: 22.07.2017
 */
public interface DeviceService extends AbstractService<Integer, Device> {

    /**
     * Get all devices active components mapped to devices with all states.
     * @return device active components
     */
    Map<String, ActiveDevice> getDeviceDashboard();

    /**
     * Synchronize all devices with from backend.
     */
    void synchronizeDevices();

    /**
     * Get all device types.
     */
    List<DeviceModel> getDeviceTypes(DeviceProtocol protocol);

    /**
     * Выполнение команды на устройстве
     * Комада GET получает текущее состояние item
     * Команда SET отправляет action команду на изменение состояния
     * Команда SWITCH доступна для ValueType - ENUM и StateType из 2х состояний - изменяет текущее на противоположное
     *
     * @param command команда
     * @return результат выполнения, текущее значение state, если команда типа GET
     */
    String command(DeviceCommand command);
}
