package com.devshot.lifebit.control.message;

import com.devshot.commons.message.data.BlankResult;
import com.devshot.commons.message.data.Command;
import com.devshot.commons.message.data.CommandConfiguration;
import com.devshot.commons.message.register.CommandListener;
import com.devshot.lifebit.control.converter.AndroidUserEventToEventConverter;
import com.devshot.lifebit.control.model.enums.AndroidUserEventType;
import com.devshot.lifebit.control.model.message.AndroidUserEventInfo;
import com.devshot.lifebit.control.model.message.configuration.AndroidUserEventCommandConfiguration;
import com.devshot.lifebit.control.model.pojo.Event;
import com.devshot.lifebit.control.service.impl.HomeInfoHolder;
import com.devshot.lifebit.control.service.impl.event.EventBUS;
import com.devshot.lifebit.control.service.impl.event.EventIdentifier;
import com.devshot.lifebit.model.pojo.EventType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author Oleg Ivashkevich
 * date: 19.08.2020
 */
@Service
@Slf4j(topic = "android-event")
@RequiredArgsConstructor
public class AndroidUserEventMessageListener implements CommandListener<AndroidUserEventInfo, BlankResult> {
    private final EventBUS eventBus;
    private final AndroidUserEventCommandConfiguration commandConfiguration;
    private final AndroidUserEventToEventConverter androidEventConverter;
    private final EventIdentifier eventIdentifier;
    private final HomeInfoHolder homeInfoHolder;

    @Override
    public CommandConfiguration<AndroidUserEventInfo, BlankResult> getCommandConfiguration() {
        return commandConfiguration;
    }

    @Override
    public void onCommandReceived(Command<AndroidUserEventInfo, BlankResult> command) {
        AndroidUserEventInfo userEvent = command.getInfo();
        log.debug("Android EVENT : {} - {} - {}", userEvent.getType(), userEvent.getDate(), userEvent.getInfo());

        processEntrance(userEvent);
        Event event = androidEventConverter.convert(userEvent);

        EventType type = eventIdentifier.identifyForUser(userEvent.getType().getId());
        if (type == null) {
            log.debug("Unknown event type, cannot identify, type - {}", userEvent.getType());
            return;
        }
        event.setType(type);

        log.debug("Android event initialized and transferring to MAIN Event BUS");
        eventBus.onEventReceived(event);
    }

    private void processEntrance(AndroidUserEventInfo info) {
        if (info.getType() == AndroidUserEventType.NETWORK_HOME_ON) {
            log.info("User entrance IN - {}", info.getUserId());
            homeInfoHolder.getUsersAtHome().add(info.getUserId());
        }
        if (info.getType() == AndroidUserEventType.NETWORK_HOME_OFF) {
            log.info("User entrance OUT - {}", info.getUserId());
            homeInfoHolder.getUsersAtHome().remove(info.getUserId());
        }
    }
}
