package com.devshot.lifebit.control.configuration;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@Configuration
@ComponentScan("com.devshot.lifebit.control.service")
@EnableCaching
@EnableScheduling
public class ServiceContextConfiguration {
}
