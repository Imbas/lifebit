package com.devshot.lifebit.control.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Oleg Ivashkevich
 * date: 19.10.2020
 */
@Getter
@Component
public class CommonConfig {

    /**
     * Temporary server coordinates.
     */
    @Value("${geo.location.latitude}")
    private Double geolocationLatitude;

    /**
     * Temporary server coordinates.
     */
    @Value("${geo.location.longitude}")
    private Double geolocationLongitude;
}
