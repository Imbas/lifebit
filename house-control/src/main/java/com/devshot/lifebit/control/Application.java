package com.devshot.lifebit.control;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
//FIXME check task pools in app! few beans
@EnableAutoConfiguration
@ComponentScan("com.devshot.lifebit.control.configuration")
public class Application {
    private static ConfigurableApplicationContext context;

    public static void main(String[] args) {
        context = SpringApplication.run(Application.class, args);
    }
}
