package com.devshot.lifebit.control.exception;

import com.devshot.commons.general.vo.exception.FormatterException;

/**
 * @author Oleg Ivashkevich
 * date: 12.05.2020
 */
public class NotFoundException extends FormatterException {
    public NotFoundException(String message, Object... args) {
        super(message, args);
    }
}
