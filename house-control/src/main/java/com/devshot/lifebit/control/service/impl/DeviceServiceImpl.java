package com.devshot.lifebit.control.service.impl;

import com.devshot.lifebit.common.dao.DeviceTypeDAO;
import com.devshot.lifebit.control.dao.DeviceDAO;
import com.devshot.lifebit.control.dao.LocationDAO;
import com.devshot.lifebit.control.model.data.ActiveComponent;
import com.devshot.lifebit.control.model.data.ActiveDevice;
import com.devshot.lifebit.control.model.pojo.Device;
import com.devshot.lifebit.control.service.DeviceService;
import com.devshot.lifebit.control.service.abstraction.AbstractServiceJpaAdapter;
import com.devshot.lifebit.control.service.impl.openhab.OpenhabItemManager;
import com.devshot.lifebit.control.service.impl.openhab.OpenhabThingManager;
import com.devshot.lifebit.model.domain.command.DeviceCommand;
import com.devshot.lifebit.model.enums.DeviceProtocol;
import com.devshot.lifebit.model.pojo.DeviceModel;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Device job logic.
 *
 * @author Oleg Ivashkevich
 * date: 22.07.2017
 */
@Service
@RequiredArgsConstructor
public class DeviceServiceImpl implements DeviceService, AbstractServiceJpaAdapter<Integer, Device> {
    private final DeviceDAO deviceDAO;
    private final DeviceTypeDAO deviceTypeDAO;
    private final LocationDAO locationDAO;

    private final OpenhabThingManager thingManager;
    private final OpenhabItemManager itemManager;

    @Override
    public JpaRepository<Device, Integer> repository() {
        return deviceDAO;
    }

    @Override
    public Map<String, ActiveDevice> getDeviceDashboard() {
        Map<Device, List<ActiveComponent>> components =
                itemManager.getAll().stream().collect(Collectors.groupingBy(component -> component.getComponent().getDevice()));
        return components.entrySet().stream().collect(Collectors.toMap(
                entry -> entry.getKey().getName(), entry -> new ActiveDevice(entry.getKey(), entry.getValue())));
    }

    @Override
    public List<DeviceModel> getDeviceTypes(DeviceProtocol protocol) {
        return deviceTypeDAO.findAllByProtocol(protocol);
    }

    @Override
    public String command(DeviceCommand command) {
        throw new UnsupportedOperationException();
    }

    @Override
    @Transactional
    public void synchronizeDevices() {
        List<Device> devices = thingManager.getDevices();

        //final clear and save
        deviceDAO.deleteAllInBatch();
        locationDAO.deleteAllInBatch();

        deviceDAO.saveAll(devices);
    }
}
