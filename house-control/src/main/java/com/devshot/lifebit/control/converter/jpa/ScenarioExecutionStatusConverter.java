package com.devshot.lifebit.control.converter.jpa;

import com.devshot.commons.general.conversion.jpa.IdentifiableEnumAttributeConverter;
import com.devshot.lifebit.control.model.enums.ScenarioExecutionStatus;

import javax.persistence.Converter;

/**
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Converter(autoApply = true)
public final class ScenarioExecutionStatusConverter extends IdentifiableEnumAttributeConverter<String, ScenarioExecutionStatus> {
}
