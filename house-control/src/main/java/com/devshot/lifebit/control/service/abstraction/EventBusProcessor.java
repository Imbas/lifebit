package com.devshot.lifebit.control.service.abstraction;

import com.devshot.lifebit.control.model.pojo.Event;

/**
 * @author Oleg Ivashkevich
 * date: 10.10.2020
 */
public interface EventBusProcessor {
    void process(Event event);
}
