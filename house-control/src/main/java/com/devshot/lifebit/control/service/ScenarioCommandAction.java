package com.devshot.lifebit.control.service;

import com.devshot.lifebit.model.domain.command.ScenarioCommand;
import com.devshot.lifebit.model.enums.ScenarioCommandType;

/**
 * @author Oleg Ivashkevich
 * date: 27.08.2020
 */
public interface ScenarioCommandAction<COMMAND extends ScenarioCommand> {
    ScenarioCommandType getType();

    void execute(COMMAND command);
}
