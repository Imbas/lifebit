package com.devshot.lifebit.control.service;

import com.devshot.commons.general.service.AbstractService;
import com.devshot.lifebit.model.pojo.Scenario;

/**
 * @author Oleg Ivashkevich
 * date: 22.07.2017
 */
public interface ScenarioService extends AbstractService<Integer, Scenario> {
    void execute(Scenario scenario);
}
