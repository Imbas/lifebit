package com.devshot.lifebit.control.service.impl;

import com.devshot.lifebit.control.config.CommonConfig;
import com.devshot.lifebit.control.model.data.ActiveComponent;
import com.devshot.lifebit.control.model.data.GeoLocation;
import com.devshot.lifebit.control.model.data.ServerStatus;
import com.devshot.lifebit.control.model.enums.DoorStatus;
import com.devshot.lifebit.control.model.enums.EventState;
import com.devshot.lifebit.control.service.impl.event.EventBUS;
import com.devshot.lifebit.model.enums.HomeMode;
import com.devshot.lifebit.control.model.enums.LightStatus;
import com.devshot.lifebit.control.model.enums.state.SwitchState;
import com.devshot.lifebit.control.model.pojo.Event;
import com.devshot.lifebit.control.service.StatusService;
import com.devshot.lifebit.control.service.impl.openhab.OpenhabItemManager;
import com.devshot.lifebit.model.enums.EventOwnerType;
import com.devshot.lifebit.model.enums.ScenarioDeviceType;
import com.devshot.lifebit.model.pojo.EventType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * @author Oleg Ivashkevich
 * date: 05.11.2020
 */
@Service
@RequiredArgsConstructor
public class StatusServiceImpl implements StatusService {
    private final static EventType MODE_TYPE = new EventType("Home mode changed", EventOwnerType.SYSTEM, false);

    private final CommonConfig commonConfig;
    private final OpenhabItemManager itemManager;
    private final HomeInfoHolder infoHolder;
    private final EventBUS eventBUS;

    @Override
    public ServerStatus getStatus() {
        return new ServerStatus(
                LocalDateTime.now(),
                infoHolder.getMode().get(),
                new GeoLocation(commonConfig.getGeolocationLatitude(), commonConfig.getGeolocationLongitude()),
                DoorStatus.LOCKED,
                lightStatus()
        );
    }

    private LightStatus lightStatus() {
        List<ActiveComponent> components = itemManager.getByType(ScenarioDeviceType.LIGHT);
        return components.stream().anyMatch(component -> component.getState() != null && component.getState().switchState() == SwitchState.ON) ?
                LightStatus.ON : LightStatus.OFF;
    }

    @Override
    public void mode(HomeMode mode) {
        infoHolder.getMode().set(mode);
        eventBUS.onEventReceived(Event.builder().type(MODE_TYPE).registerDate(ZonedDateTime.now()).state(EventState.APPROVED).build());
    }
}
