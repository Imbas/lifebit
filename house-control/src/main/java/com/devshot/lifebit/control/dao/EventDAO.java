package com.devshot.lifebit.control.dao;

import com.devshot.lifebit.control.model.pojo.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Oleg Ivashkevich
 * date: 17.07.2017
 */
@Repository
public interface EventDAO extends JpaRepository<Event, Long> {
}
