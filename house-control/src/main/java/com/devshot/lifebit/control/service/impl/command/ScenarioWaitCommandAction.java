package com.devshot.lifebit.control.service.impl.command;

import com.devshot.lifebit.control.service.ScenarioCommandAction;
import com.devshot.lifebit.model.domain.command.ScenarioWaitCommand;
import com.devshot.lifebit.model.enums.ScenarioCommandType;
import org.springframework.stereotype.Component;

/**
 * @author Oleg Ivashkevich
 * date: 27.08.2020
 */
@Component
public class ScenarioWaitCommandAction implements ScenarioCommandAction<ScenarioWaitCommand> {
    @Override
    public ScenarioCommandType getType() {
        return ScenarioCommandType.WAIT;
    }

    @Override
    public void execute(ScenarioWaitCommand command) {
        try {
            Thread.sleep(command.getDuration().toMillis());
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
