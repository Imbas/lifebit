package com.devshot.lifebit.control.service.impl.event;

import com.devshot.lifebit.common.dao.EventTypeDAO;
import com.devshot.lifebit.control.model.pojo.Event;
import com.devshot.lifebit.control.util.EventConditionChecker;
import com.devshot.lifebit.model.domain.condition.event.DeviceStateCondition;
import com.devshot.lifebit.model.domain.condition.event.UserEventCondition;
import com.devshot.lifebit.model.enums.EventOwnerType;
import com.devshot.lifebit.model.pojo.EventType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Oleg Ivashkevich
 * date: 13.11.2020
 */
@Component
@RequiredArgsConstructor
@Slf4j(topic = "event")
public class EventIdentifier {
    private final EventTypeDAO eventTypeDAO;

    /**
     * Identify event type by event information - for device component.
     * Find event type by state type rule. First event for component type by rule.
     *
     * @param event incoming event
     * @return event type
     */
    public EventType identifyForDevice(Event event) {
        List<EventType> types = eventTypeDAO.findAllByComponentType(event.getComponent().getType());

        for (EventType type : types) {
            if (EventConditionChecker.check((DeviceStateCondition) type.getCondition(), event)) {
                log.debug("Event condition granted for component ({}) - {}", event.getComponent(), type.getCondition());
                return type;
            }
        }
        return null;
    }

    public EventType identifyForUser(String userEventId) {
        List<EventType> types = eventTypeDAO.findAllByOwnerType(EventOwnerType.USER);

        for (EventType type : types) {
            if (EventConditionChecker.check((UserEventCondition) type.getCondition(), userEventId)) {
                log.debug("Event condition granted for type ({}) - {}", userEventId, type.getCondition());
                return type;
            }
        }
        return null;
    }
}
