package com.devshot.lifebit.control.converter;

import com.devshot.lifebit.control.model.pojo.Component;
import com.devshot.lifebit.smart.openhab.remote.model.OpenhabChannel;
import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

/**
 * @author Oleg Ivashkevich
 * date: 27.10.2018
 */
@Mapper(componentModel = "spring")
public interface OpenhabChannelToActiveComponentDeviceConverter {
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "name", source = "label")
    @Mapping(target = "externalId", source = "linkedItemNames")
    @Mapping(target = "type.externalId", source = "channelTypeUID")
    Component convert(OpenhabChannel channel);

    List<Component> convert(List<OpenhabChannel> channels);

    default String map(List<String> items) {
        return CollectionUtils.isNotEmpty(items) ? items.get(0) : null;
    }
}
