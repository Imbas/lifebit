package com.devshot.lifebit.control.controller;

import com.devshot.lifebit.control.api.StatusApi;
import com.devshot.lifebit.control.model.data.ServerStatus;
import com.devshot.lifebit.model.enums.HomeMode;
import com.devshot.lifebit.control.service.StatusService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@RestController
@RequiredArgsConstructor
public class StatusController implements StatusApi {
    private final StatusService statusService;

    @Override
    public ServerStatus status() {
        return statusService.getStatus();
    }

    @Override
    public void mode(@PathVariable(name = "mode") HomeMode mode) {
        statusService.mode(mode);
    }
}
