package com.devshot.lifebit.control.service;

import com.devshot.commons.general.service.AbstractService;
import com.devshot.lifebit.control.model.pojo.Location;

/**
 * @author Oleg Ivashkevich
 * date: 22.07.2017
 */
public interface LocationService extends AbstractService<Integer, Location> {
}
