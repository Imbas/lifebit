package com.devshot.lifebit.control.configuration;

import com.devshot.commons.general.GeneralContextConfiguration;
import com.devshot.commons.hazelcast.leader.HazelcastLeaderContextConfiguration;
import com.devshot.commons.http.HttpContextConfiguration;
import com.devshot.commons.message.MessageContextConfiguration;
import com.devshot.commons.message.mqtt.configuration.EclipsePahoMqttContextConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Oleg Ivashkevich
 * date: 08.05.2020
 */
@Configuration
@Import({
        GeneralContextConfiguration.class,
        HttpContextConfiguration.class,
        MessageContextConfiguration.class,
        EclipsePahoMqttContextConfiguration.class,
        HazelcastLeaderContextConfiguration.class
})
public class JCommonsContextConfiguration {
}
