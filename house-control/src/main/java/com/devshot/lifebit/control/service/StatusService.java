package com.devshot.lifebit.control.service;

import com.devshot.lifebit.control.model.data.ServerStatus;
import com.devshot.lifebit.model.enums.HomeMode;

/**
 * @author Oleg Ivashkevich
 * date: 22.07.2017
 */
public interface StatusService {
    /**
     * Generate server status.
     * @return status
     */
    ServerStatus getStatus();

    /**
     * Set home mode.
     * @param mode mode
     */
    void mode(HomeMode mode);
}
