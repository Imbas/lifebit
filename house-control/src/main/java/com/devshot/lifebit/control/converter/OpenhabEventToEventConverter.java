package com.devshot.lifebit.control.converter;

import com.devshot.lifebit.control.model.enums.EventState;
import com.devshot.lifebit.control.model.pojo.Event;
import com.devshot.lifebit.smart.openhab.model.OpenhabEvent;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * @author Oleg Ivashkevich
 * date: 27.10.2018
 */
@Mapper(componentModel = "spring", imports = EventState.class)
public interface OpenhabEventToEventConverter {
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "data", source = "state")
    @Mapping(target = "registerDate", source = "date")
    @Mapping(target = "state", constant = "APPROVED")
    Event convert(OpenhabEvent event);

    default ZonedDateTime map(Long date) {
        return ZonedDateTime.ofInstant(Instant.ofEpochMilli(date), ZoneId.systemDefault());
    }
}
