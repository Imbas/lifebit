package com.devshot.lifebit.control.service.abstraction;

import com.devshot.commons.general.service.AbstractService;
import com.devshot.commons.general.vo.Identifiable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author Oleg Ivashkevich
 * date: 03.05.2020
 */
public interface AbstractServiceJpaAdapter<ID extends Serializable, ENTITY extends Identifiable<ID>> extends AbstractService<ID, ENTITY> {
    JpaRepository<ENTITY, ID> repository();

    @Override
    default Optional<ENTITY> get(ID id) {
        return repository().findById(id);
    }

    @Override
    default List<ENTITY> getAll() {
        return repository().findAll();
    }

    @Override
    default ID add(ENTITY entity) {
        return repository().save(entity).getId();
    }

    @Override
    default void change(ENTITY entity) {
        repository().save(entity);
    }

    @Override
    default void remove(ID id) {
        repository().deleteById(id);
    }

    @Override
    default void remove(Collection<ID> ids) {
        ids.forEach(id -> repository().deleteById(id));
    }
}
