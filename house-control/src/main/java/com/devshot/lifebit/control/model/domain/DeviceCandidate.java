package com.devshot.lifebit.control.model.domain;

import com.devshot.lifebit.control.model.enums.DeviceCandidateStatus;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Кандидат на добавление устройства
 *
 * @author Oleg Ivashkevich
 * date: 22.07.2017
 */
public class DeviceCandidate {
    @JsonProperty("label")
    public final String label;

    @JsonProperty("openhabUUID")
    public final String openhabUUID;

    @JsonProperty("status")
    public final DeviceCandidateStatus status;

    @JsonCreator
    public DeviceCandidate(@JsonProperty("label") String label, @JsonProperty("openhabUUID") String openhabUUID,
                           @JsonProperty("status") DeviceCandidateStatus status) {
        this.label = label;
        this.openhabUUID = openhabUUID;
        this.status = status;
    }

    @Override
    @JsonIgnore
    public String toString() {
        return "DeviceCandidate{" +
                "label='" + label + '\'' +
                ", openhabUUID='" + openhabUUID + '\'' +
                ", status=" + status +
                '}';
    }
}
