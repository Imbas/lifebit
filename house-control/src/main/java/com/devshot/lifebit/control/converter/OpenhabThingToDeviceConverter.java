package com.devshot.lifebit.control.converter;

import com.devshot.lifebit.control.model.pojo.Device;
import com.devshot.lifebit.smart.openhab.remote.model.OpenhabThing;
import org.mapstruct.*;

import java.util.List;

/**
 * @author Oleg Ivashkevich
 * date: 27.10.2018
 */
@Mapper(componentModel = "spring", uses = {
        OpenhabChannelToActiveComponentDeviceConverter.class
})
public interface OpenhabThingToDeviceConverter {
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "externalId", source = "UID")
    @Mapping(target = "name", source = "label")
    @Mapping(target = "location.name", source = "location")
    @Mapping(target = "model.externalId", source = "thingTypeUID")
    @Mapping(target = "components", source = "channels")
    Device convert(OpenhabThing thing);

    List<Device> convert(List<OpenhabThing> things);

    @AfterMapping
    default void after(@MappingTarget Device device) {
        if (device.getComponents() != null) {
            device.getComponents().forEach(child -> {
                child.setDevice(device);
            });
        }
    }
}
