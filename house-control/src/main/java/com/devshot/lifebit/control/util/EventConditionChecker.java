package com.devshot.lifebit.control.util;

import com.devshot.commons.general.utils.AssertUtils;
import com.devshot.lifebit.control.model.pojo.Event;
import com.devshot.lifebit.model.domain.EventCondition;
import com.devshot.lifebit.model.domain.condition.event.DeviceStateCondition;
import com.devshot.lifebit.model.domain.condition.event.EventAbsenceCondition;
import com.devshot.lifebit.model.domain.condition.event.UserEventCondition;
import com.devshot.lifebit.model.enums.ValueType;
import lombok.experimental.UtilityClass;

import java.time.ZonedDateTime;

/**
 * @author Oleg Ivashkevich
 * date: 07.09.2020
 */
@UtilityClass
//FIXME - may be create eventCheckBox and to keep check logic in the condotions?
public class EventConditionChecker {
    public boolean check(EventCondition condition, EventConditionBox box) {
        if (condition instanceof DeviceStateCondition) {
            return check((DeviceStateCondition) condition, box.getLatest().stream().findFirst().orElseThrow(
                    () -> new IllegalArgumentException("At least 1 event required for DeviceStateCondition")));
        }
        if (condition instanceof EventAbsenceCondition) {
            return check((EventAbsenceCondition) condition, box);
        }
        throw new UnsupportedOperationException("Unsupported condition for check!");
    }

    public boolean check(UserEventCondition condition, String userEventId) {
        return condition.getUserEventId().equals(userEventId);
    }

    public boolean check(DeviceStateCondition condition, Event event) {
        ValueType type = event.getComponent().getType().getStateType().getType();
        String newState = event.getData();
        String expectedState = condition.getExpectedState();

        switch (condition.getChangeRule()) {
            case EQUAL: {
                return newState.equals(expectedState);
            }
            case LESS: {
                switch (type) {
                    case ENUM:
                    case STRING:
                        throw new UnsupportedOperationException("Number rule is available only for number value types");
                    case INTEGER: {
                        int expected = Integer.parseInt(expectedState);
                        return Integer.parseInt(newState) < expected;
                    }
                    case FLOAT: {
                        float expected = Float.parseFloat(expectedState);
                        return Float.parseFloat(newState) < expected;
                    }
                    default:
                        throw new AssertionError("Unsupported value type!");
                }
            }
            case OVER: {
                switch (type) {
                    case ENUM:
                    case STRING:
                        throw new UnsupportedOperationException("Number rule is available only for number value types");
                    case INTEGER: {
                        int expected = Integer.parseInt(expectedState);
                        return Integer.parseInt(newState) > expected;
                    }
                    case FLOAT: {
                        float expected = Float.parseFloat(expectedState);
                        return Float.parseFloat(newState) > expected;
                    }
                    default:
                        throw new AssertionError("Unsupported value type!");
                }
            }
            default:
                throw new AssertionError("Unsupported changeRule type!");
        }
    }

    public boolean check(EventAbsenceCondition condition, EventConditionBox box) {
        AssertUtils.notNull(box.getTargetType(), "Target type required for EventAbsenceCondition check");
        AssertUtils.notNull(box.getLatest(), "Latest required for EventAbsenceCondition check");

        ZonedDateTime date = ZonedDateTime.now().minus(condition.getDuration());
        //last conditional event or event by target type
        Event target = box.getLatest().stream()
                .filter(event -> event.getType().equals(box.getTargetType())
                        || event.getType().getId().equals(condition.getEventTypeId())
                        && (condition.getScenarioDeviceId() == null || event.getComponent() != null && event.getComponent().getScenarioDevice() != null
                        && event.getComponent().getScenarioDevice().getId().equals(condition.getScenarioDeviceId())))
                .reduce((a, b) -> b).orElse(null);
        return target != null
                && target.getRegisterDate().isBefore(date)
                && !target.getType().equals(box.getTargetType());
    }
}
