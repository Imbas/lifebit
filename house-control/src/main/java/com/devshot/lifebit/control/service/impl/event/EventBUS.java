package com.devshot.lifebit.control.service.impl.event;

import com.devshot.commons.general.utils.JExceptionUtils;
import com.devshot.commons.hazelcast.leader.HazelcastLeaderManager;
import com.devshot.lifebit.common.dao.EventTypeDAO;
import com.devshot.lifebit.control.model.enums.EventState;
import com.devshot.lifebit.control.model.pojo.Event;
import com.devshot.lifebit.control.service.abstraction.EventBusProcessor;
import com.devshot.lifebit.model.domain.condition.CronCondition;
import com.devshot.lifebit.model.enums.EventOwnerType;
import com.devshot.lifebit.model.pojo.EventType;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Main event BUS of applciation. All event MUST go here.
 * Leader flag - mqtt topic is broadcast - leader activate the BUS.
 *
 * @author Oleg Ivashkevich
 * date: 19.08.2020
 */
//TODO FUTURE: include leader logic to commandService - don't process commands if slave
@Service
@Slf4j(topic = "MAIN EVENT BUS")
@RequiredArgsConstructor
public class EventBUS {
    private final EventTypeDAO eventTypeDAO;
    private final List<EventBusProcessor> processors;
    private final HazelcastLeaderManager leaderManager;
    private final ThreadPoolTaskExecutor executor;
    private final TaskScheduler scheduler;

    // ------------------------------------ EXECUTIONS ----------------------------------
    private final static int WORKERS = 5;
    private final static int QUEUE = 100;
    private final static int OFFER = 1;
    private final static int MAX_EVENTS = 10000;
    private final static int EVENTS_LIVE_TIME_H = 24;

    private final BlockingQueue<Event> mainEventQueue = new ArrayBlockingQueue<>(QUEUE);

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        JExceptionUtils.handle((JExceptionUtils.RunnableWithException) () -> {
            for (int i = 0; i < WORKERS; i++) {
                executor.execute(this::workerAction);
            }
            planScheduleEvents();
        }, "Some error during start workers and plan periodic events");
    }

    private void workerAction() {
        while (!Thread.currentThread().isInterrupted()) {
            JExceptionUtils.handle((JExceptionUtils.RunnableWithException) () -> {
                //hold on
                Event event = mainEventQueue.take();
                processEvent(event);
            }, "Some error during event processing");
        }
    }

    private void mainEventQueuePut(Event event) {
        JExceptionUtils.handle((JExceptionUtils.RunnableWithException) () -> {
            mainEventQueue.offer(event, OFFER, TimeUnit.SECONDS);
        }, "Some error of event queue");
    }

    //TODO FUTURE support refresh scope - own scheduler and executor service
    private void planScheduleEvents() {
        List<EventType> types = eventTypeDAO.findAllByOwnerType(EventOwnerType.SCHEDULE);
        types.forEach(type -> {
            scheduler.schedule(() -> {
                Event event = new Event();
                event.setState(EventState.APPROVED);
                event.setRegisterDate(ZonedDateTime.now());
                event.setType(type);

                mainEventQueuePut(event);
            }, new CronTrigger(((CronCondition) type.getCondition()).getCron()));
        });
    }
    // ----------------------------------------------------------------------------------

    // ------------------------------------- EVENT LOGIC --------------------------------
    @Getter
    private final Cache<String, Event> cache = CacheBuilder.newBuilder()
            .expireAfterWrite(Duration.ofHours(EVENTS_LIVE_TIME_H))
            .maximumSize(MAX_EVENTS)
            .build();

    public void onEventReceived(Event event) {
        if (!leaderManager.isLeader()) {
            return;
        }

        log.info("Incoming event : {}[{}] - {} - {} : goes to the main event queue", event.getType().getName(), event.getRegisterDate(),
                event.getData(),
                event.getComponent());

        mainEventQueuePut(event);
    }

    /**
     * Main async event processing action for worker.
     *
     * @param event incoming event from the queue
     */
    private void processEvent(Event event) {
        log.info("Event {}[{}] - processing...", event.getType().getName(), event.getRegisterDate());

        if (event.getType().isNeedToSave()) {
            cache.put(event.buildKey(), event);
            //TODO FUTURE: db event save
        }

        processors.forEach(processor -> {
            JExceptionUtils.handle((JExceptionUtils.RunnableWithException) () -> {
                processor.process(event);
            }, "Some error during event processing by processor - " + processor.getClass());
        });
    }

    //Temporary no need logic
//    private void complexCheck(Event event) {
//        List<EventType> complexTypes = eventTypeDAO.findAllByOwnerType(EventOwnerType.COMPLEX);
//        //FIXME unoptimized sort per each cache get
//        Collection<Event> latest = cache.asMap().values().stream().sorted(Comparator.comparing(Event::getRegisterDate)).collect(Collectors.toList
//        ());
//
//        //FIXME: very unoptimized direct search in collection by every complex event type per every event
//        for (EventType type : complexTypes) {
//            if (EventConditionChecker.check(type.getCondition(), EventConditionBox.builder().latest(latest).targetType(type).build())) {
//                log.info("Complex event {} was created", type.getName());
//                Event complex = new Event();
//                complex.setType(type);
//                complex.setRegisterDate(ZonedDateTime.now());
//                complex.setState(EventState.APPROVED);
//                mainEventQueuePut(complex);
//            }
//        }
//    }

    // ----------------------------------------------------------------------------------
}
