package com.devshot.lifebit.control.service.impl.event.bus_processors;

import com.devshot.commons.message.service.CommandService;
import com.devshot.lifebit.control.model.enums.AndroidNotificationType;
import com.devshot.lifebit.control.model.message.AndroidNotificationInfo;
import com.devshot.lifebit.control.model.message.configuration.AndroidNotificationCommandConfiguration;
import com.devshot.lifebit.control.model.pojo.Event;
import com.devshot.lifebit.control.service.abstraction.EventBusProcessor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * @author Oleg Ivashkevich
 * date: 11.01.2021
 */
@Component
@Slf4j(topic = "android-notification")
@RequiredArgsConstructor
public class AndroidNotificationsEventProcessor implements EventBusProcessor {
    private final CommandService commandService;
    private final AndroidNotificationCommandConfiguration eventCommandConfiguration;

    private final static String MESSAGE_TEMPLATE_DEVICE = "[%s] event from [%s] device, [%s] location, details - %s";
    private final static String MESSAGE_TEMPLATE = "[%s] event, data - %s";

    @Override
    public void process(Event event) {
        if (!event.getType().isNeedToNotify()) {
            return;
        }

        log.info("Notification event - {}", event.getType().getName());

        String message = makeMessage(event);
        commandService.send(eventCommandConfiguration.createCommand(
                new AndroidNotificationInfo(AndroidNotificationType.ALARM, event.getRegisterDate(), message)));
    }

    private String makeMessage(Event event) {
        switch (event.getType().getOwnerType()) {
            case DEVICE:
                return String.format(MESSAGE_TEMPLATE_DEVICE, event.getType().getName(), event.getComponent().getDevice().getName(),
                        event.getComponent().getDevice().getLocation().getName(), event.getType().getDescription());
            default:
                return String.format(MESSAGE_TEMPLATE, event.getType().getName(), event.getData());
        }
    }
}
