package com.devshot.lifebit.control.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Oleg Ivashkevich
 * date: 19.10.2020
 */
@Getter
@Component
public class AndroidConfig {

    /**
     * Topic for android devices - for messaging broadcost android notification, sending to all androids.
     */
    @Value("${android.topic.notification}")
    private String androidNotificationTopic;

    /**
     * Topic for android devices - for messaging special user event on android, sending to server.
     */
    @Value("${android.topic.userevent}")
    private String androidUserEventTopic;
}
