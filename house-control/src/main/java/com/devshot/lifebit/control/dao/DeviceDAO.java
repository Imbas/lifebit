package com.devshot.lifebit.control.dao;

import com.devshot.lifebit.control.model.pojo.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Oleg Ivashkevich
 * date: 17.07.2017
 */
@Repository
public interface DeviceDAO extends JpaRepository<Device, Integer> {

}
