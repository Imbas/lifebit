package com.devshot.lifebit.control.model.domain;

import com.devshot.lifebit.model.domain.command.DeviceCommand;
import com.devshot.lifebit.model.enums.CommandBase;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * Команда для item устройства
 *
 * @author Oleg Ivashkevich
 * date: 25.07.2017
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@NoArgsConstructor
public class ItemDeviceCommand extends DeviceCommand {

    @NotNull
    @JsonProperty("deviceItemId")
    private Integer deviceItemId;

    public ItemDeviceCommand(CommandBase type, String state, Integer deviceItemId) {
        super(type, state);
        this.deviceItemId = deviceItemId;
    }
}
