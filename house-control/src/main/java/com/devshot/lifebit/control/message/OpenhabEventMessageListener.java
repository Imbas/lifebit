package com.devshot.lifebit.control.message;

import com.devshot.commons.message.data.BlankResult;
import com.devshot.commons.message.data.Command;
import com.devshot.commons.message.data.CommandConfiguration;
import com.devshot.commons.message.register.CommandListener;
import com.devshot.lifebit.control.converter.OpenhabEventToEventConverter;
import com.devshot.lifebit.control.dao.ComponentDAO;
import com.devshot.lifebit.control.model.pojo.Component;
import com.devshot.lifebit.control.model.pojo.Event;
import com.devshot.lifebit.control.service.impl.event.EventBUS;
import com.devshot.lifebit.control.service.impl.event.EventIdentifier;
import com.devshot.lifebit.model.pojo.EventType;
import com.devshot.lifebit.smart.openhab.message.OpenhabEventCommandConfiguration;
import com.devshot.lifebit.smart.openhab.model.OpenhabEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author Oleg Ivashkevich
 * date: 19.08.2020
 */
@Service
@Slf4j(topic = "openhab-event")
@RequiredArgsConstructor
public class OpenhabEventMessageListener implements CommandListener<OpenhabEvent, BlankResult> {
    private final EventBUS eventBus;
    private final OpenhabEventCommandConfiguration commandConfiguration;
    private final OpenhabEventToEventConverter eventConverter;
    private final ComponentDAO componentDAO;
    private final EventIdentifier eventIdentifier;

    private final static String UNKNOWN_STATE = "Uninitialized";

    @Override
    public CommandConfiguration<OpenhabEvent, BlankResult> getCommandConfiguration() {
        return commandConfiguration;
    }

    @Override
    //TODO: exceptions?
    //TODO: event service logic?
    public void onCommandReceived(Command<OpenhabEvent, BlankResult> command) {
        OpenhabEvent habEvent = command.getInfo();
        log.debug("Openhab EVENT : {} - {} - {}", habEvent.getSensorId(), habEvent.getState(), habEvent.getDate());
        Event event = eventConverter.convert(habEvent);

        if (UNKNOWN_STATE.equals(event.getData())) {
            log.debug("Unknown event state - {}, sensor id - {}", event.getData(), habEvent.getSensorId());
            return;
        }

        //key event device component
        Component component = componentDAO.findByExternalId(habEvent.getSensorId());
        if (component == null) {
            log.debug("Unknown event, sensor id - {}", habEvent.getSensorId());
            return;
        }
        event.setComponent(component);

        EventType type = eventIdentifier.identifyForDevice(event);
        if (type == null) {
            log.debug("Unknown event type, cannot identify, sensor id - {}", habEvent.getSensorId());
            return;
        }
        event.setType(type);

        log.debug("Openhab event initialized and transferring to MAIN Event BUS, sensor id - {}", habEvent.getSensorId());
        eventBus.onEventReceived(event);
    }
}
