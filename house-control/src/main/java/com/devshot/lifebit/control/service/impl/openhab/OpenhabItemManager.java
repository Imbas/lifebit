package com.devshot.lifebit.control.service.impl.openhab;

import com.devshot.lifebit.control.converter.OpenhabStatesConverter;
import com.devshot.lifebit.control.dao.ComponentDAO;
import com.devshot.lifebit.control.model.data.ActiveComponent;
import com.devshot.lifebit.control.model.data.state.ComponentState;
import com.devshot.lifebit.control.model.data.state.FloatState;
import com.devshot.lifebit.control.model.data.state.IntegerState;
import com.devshot.lifebit.control.model.pojo.Component;
import com.devshot.lifebit.model.enums.ScenarioDeviceType;
import com.devshot.lifebit.model.enums.StateType;
import com.devshot.lifebit.smart.openhab.remote.api.OpenhabItems;
import com.devshot.lifebit.smart.openhab.remote.model.OpenhabItem;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Special openhub items manager.
 *
 * @author Oleg Ivashkevich
 * date: 24.07.2017
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class OpenhabItemManager {
    private final static String UNKNOWN = "NULL";

    private final ComponentDAO componentDAO;
    private final OpenhabItems openhabItems;
    private final OpenhabStatesConverter statesConverter;

    public List<ActiveComponent> getAll() {
        return getByType(null);
    }

    public List<ActiveComponent> getByType(ScenarioDeviceType type) {
        List<Component> components = type != null ? componentDAO.findByScenarioDeviceType(type) : componentDAO.findAll();
        Map<String, OpenhabItem> items = openhabItems.getAll(null, null, false)
                .stream().collect(Collectors.toMap(OpenhabItem::getName, Function.identity()));

        return components.stream().map(component -> {
            OpenhabItem item = items.get(component.getExternalId());
            if (item == null) {
                log.warn("Not found item ({}) - please do sync to Openhub", component.getExternalId());
                return null;
            }

            ComponentState state = mapState(item.getState(), component.getType().getStateType());
            return new ActiveComponent(component, state);
        }).filter(Objects::nonNull).collect(Collectors.toList());
    }

    private ComponentState mapState(String state, StateType type) {
        if (state == null || state.equals(UNKNOWN)) {
            return null;
        }

        switch (type) {
            case CONTACT:
                return statesConverter.contactMap(state);
            case SWITCH:
                return statesConverter.switchMap(state);
            case LUMINANCE:
                return new FloatState(state);
            case TEMPERATURE:
                return statesConverter.temperatureMap(state);
            case PERCENT:
                return new IntegerState(state);
            case ALARM:
                return null;
            default:
                throw new UnsupportedOperationException("Unsupported state for value mapping - " + type);
        }
    }
}
