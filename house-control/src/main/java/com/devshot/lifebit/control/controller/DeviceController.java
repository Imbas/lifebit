package com.devshot.lifebit.control.controller;

import com.devshot.lifebit.control.api.DeviceApi;
import com.devshot.lifebit.control.model.data.ActiveComponent;
import com.devshot.lifebit.control.model.data.ActiveDevice;
import com.devshot.lifebit.control.model.pojo.Device;
import com.devshot.lifebit.control.service.DeviceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@RestController
@RequiredArgsConstructor
public class DeviceController implements DeviceApi {
    private final DeviceService deviceService;

    public List<Device> getAll() {
        return deviceService.getAll();
    }

    public Map<String, ActiveDevice> dashboard() {
        return deviceService.getDeviceDashboard();
    }

    public void sync() {
        deviceService.synchronizeDevices();
    }
}
