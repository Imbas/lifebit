package com.devshot.lifebit.control.util;

import com.devshot.lifebit.control.model.pojo.Event;
import com.devshot.lifebit.model.pojo.EventType;
import lombok.Builder;
import lombok.Data;

import java.util.Collection;

/**
 * @author Oleg Ivashkevich
 * date: 08.09.2020
 */
@Data
@Builder
public class EventConditionBox {
    private final Collection<Event> latest;
    private final EventType targetType;
}
