package com.devshot.lifebit.control.converter;

import com.devshot.lifebit.control.model.data.state.FloatState;
import com.devshot.lifebit.control.model.enums.state.ContactState;
import com.devshot.lifebit.control.model.enums.state.SwitchState;
import org.mapstruct.Mapper;
import org.mapstruct.ValueMapping;

/**
 * @author Oleg Ivashkevich
 * date: 27.10.2018
 */
@Mapper(componentModel = "spring")
public interface OpenhabStatesConverter {
    enum OpenhabSwitchState {
        ON, OFF
    }

    enum OpenhabContactState {
        OPEN, CLOSED
    }

    @ValueMapping(source = "ON", target = "ON")
    @ValueMapping(source = "OFF", target = "OFF")
    SwitchState switchMap(OpenhabSwitchState state);

    @ValueMapping(source = "CLOSED", target = "CLOSED")
    @ValueMapping(source = "OPEN", target = "OPENED")
    ContactState contactMap(OpenhabContactState state);

    default SwitchState switchMap(String state) {
        return switchMap(OpenhabSwitchState.valueOf(state));
    }

    default ContactState contactMap(String state) {
        return contactMap(OpenhabContactState.valueOf(state));
    }

    default FloatState temperatureMap(String state) {
        return new FloatState(state.substring(0, state.length() - 3));
    }
}
