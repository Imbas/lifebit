package com.devshot.lifebit.model.domain.condition;

import com.devshot.lifebit.model.domain.EventCondition;
import com.devshot.lifebit.model.domain.ScenarioCondition;
import com.devshot.lifebit.model.domain.ScenarioConditionCheckBox;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Groovy-script condition.
 *
 * @author Oleg Ivashkevich
 * date: 15.07.2017
 */
@Data
public class GroovyCondition implements ScenarioCondition, EventCondition {
    @NotBlank
    @JsonProperty("script")
    private String script;

    @Override
    public boolean check(@NotNull ScenarioConditionCheckBox box) {
        throw new UnsupportedOperationException();
    }
}
