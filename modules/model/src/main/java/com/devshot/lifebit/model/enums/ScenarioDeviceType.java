package com.devshot.lifebit.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Oleg Ivashkevich
 * date: 29.11.2020
 */
@Getter
@RequiredArgsConstructor
public enum ScenarioDeviceType implements Identifiable<String> {
    SENSOR("SENSOR"), LIGHT("LIGHT"), DOOR("DOOR"), OTHER("OTHER");

    public final String id;
}
