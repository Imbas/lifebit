package com.devshot.lifebit.model.pojo;

import com.devshot.commons.x_model.jpa.Namer;
import com.devshot.lifebit.model.enums.ScenarioDeviceType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Scenario device - device using in scenario execution to put commands to special devices without any connections to concrete house control.
 *
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name = "ScenarioDevice")
public class ScenarioDevice extends Namer {
    @Column(name = "type", nullable = false)
    private ScenarioDeviceType type;
}
