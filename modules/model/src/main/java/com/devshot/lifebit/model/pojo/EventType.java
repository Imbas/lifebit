package com.devshot.lifebit.model.pojo;

import com.devshot.commons.x_model.jpa.NamerWithDescription;
import com.devshot.lifebit.model.domain.EventCondition;
import com.devshot.lifebit.model.enums.EventOwnerType;
import lombok.*;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Event type, happened in system.
 *
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Getter
@Setter
@ToString(callSuper = true)
@NoArgsConstructor

@Entity
@Table(name = "EventType")
//TODO: DeviceStateCondition - check mandatory of componentType
//TODO: DeviceStateCondition - only for exists componentType
public class EventType extends NamerWithDescription {
    /**
     * Owner of happened event.
     */
    @NotNull
    @Column(name = "owner_type", nullable = false)
    private EventOwnerType ownerType;

    /**
     * Need to ask user, is this happened event right or wrong?
     */
    @NotNull
    @Column(name = "need_to_confirm", nullable = false)
    private boolean needToConfirm;

    /**
     * Need to send to user PUSH-notification abount the event.
     */
    @NotNull
    @Column(name = "need_to_notify", nullable = false)
    private boolean needToNotify;

    /**
     * Need to save to DB and hold in cache.
     */
    @NotNull
    @Column(name = "need_to_save", nullable = false)
    private boolean needToSave;

    /**
     * Main variable - condition of event identification on system.
     */
    @NotNull
    @Column(name = "init_condition", nullable = false, length = 1024)
    @Valid
    private EventCondition condition;

    /**
     * Possible device component for the event type. Null if not device event.
     */
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = ComponentType.class)
    @JoinColumn(name = "component_type_id")
    private ComponentType componentType;

    public EventType(@NotNull String name, @NotNull EventOwnerType ownerType, @NotNull boolean needToSave) {
        this.name = name;
        this.ownerType = ownerType;
        this.needToSave = needToSave;
    }
}
