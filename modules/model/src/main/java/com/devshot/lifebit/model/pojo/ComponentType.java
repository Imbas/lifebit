package com.devshot.lifebit.model.pojo;

import com.devshot.commons.x_model.jpa.Namer;
import com.devshot.lifebit.model.enums.CommandBase;
import com.devshot.lifebit.model.enums.StateType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Device component type - device active sub components with active functions.
 *
 * @author Oleg Ivashkevich
 * date: 16.02.2020
 */
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name = "ComponentType")
public class ComponentType extends Namer {

    /**
     * External device type (model) identifier in external system like openhub.
     */
    @Column(name = "external_id", unique = true)
    private String externalId;

    /**
     * State type - value type for state.
     */
    @NotNull
    @Column(name = "state_type", nullable = false)
    private StateType stateType;

    /**
     * Available commands to execute.
     */
    @NotEmpty
    @Column(name = "commands", nullable = false)
    private List<CommandBase> commands;
}
