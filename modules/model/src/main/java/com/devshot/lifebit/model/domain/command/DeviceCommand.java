package com.devshot.lifebit.model.domain.command;

import com.devshot.lifebit.model.enums.CommandBase;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Command for send to device.
 *
 * @author Oleg Ivashkevich
 * date: 15.07.2017
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class DeviceCommand {
    @NotNull
    @JsonProperty("base")
    private CommandBase base;

    @JsonProperty("state")
    private String state;

    @Valid
    @JsonIgnore
    public boolean validation() {
        return base != CommandBase.SET || StringUtils.isNotEmpty(state);
    }
}
