package com.devshot.lifebit.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Oleg Ivashkevich
 * date: 29.11.2020
 */
@Getter
@RequiredArgsConstructor
public enum StateType implements Identifiable<String> {
    TEMPERATURE("TEMPERATURE", ValueType.FLOAT),
    LUMINANCE("LUMINANCE", ValueType.FLOAT),
    PERCENT("PERCENT", ValueType.INTEGER),
    CONTACT("CONTACT", ValueType.ENUM),
    SWITCH("SWITCH", ValueType.ENUM),
    ALARM("ALARM", null);

    private final String id;
    private final ValueType type;
}
