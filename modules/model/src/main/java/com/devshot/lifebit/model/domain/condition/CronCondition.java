package com.devshot.lifebit.model.domain.condition;

import com.devshot.lifebit.model.domain.EventCondition;
import com.devshot.lifebit.model.domain.ScenarioCondition;
import com.devshot.lifebit.model.domain.ScenarioConditionCheckBox;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Schedule condition.
 *
 * @author Oleg Ivashkevich
 * date: 08.09.2020
 */
@Data
public class CronCondition implements ScenarioCondition, EventCondition {

    /**
     * Cron. Example - '0/10 * * * * *' - per 10 secinds.
     */
    @NotNull
    @JsonProperty("cron")
    private String cron;

    @Override
    public boolean check(@NotNull ScenarioConditionCheckBox box) {
        throw new UnsupportedOperationException();
    }
}
