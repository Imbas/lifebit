package com.devshot.lifebit.model.enums;

/**
 * Tag is the house door locked.
 *
 * @author Oleg Ivashkevich
 * date: 05.11.2020
 */
public enum HomeMode {
   ARMED, DISARMED, CLOSED
}
