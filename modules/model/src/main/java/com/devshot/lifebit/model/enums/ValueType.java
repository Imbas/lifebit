package com.devshot.lifebit.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Getter
@RequiredArgsConstructor
public enum ValueType implements Identifiable<String> {
    INTEGER("INTEGER"), FLOAT("FLOAT"), STRING("STRING"), ENUM("ENUM");

    public final String id;
}
