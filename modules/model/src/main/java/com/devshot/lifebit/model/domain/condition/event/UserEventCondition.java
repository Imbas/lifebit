package com.devshot.lifebit.model.domain.condition.event;

import com.devshot.lifebit.model.domain.EventCondition;
import com.devshot.lifebit.model.enums.ChangeStateRule;
import com.devshot.lifebit.model.enums.ValueType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * User event condition.
 *
 * @author Oleg Ivashkevich
 * date: 15.07.2017
 */
@Data
public class UserEventCondition implements EventCondition {

    /**
     * User event id representation.
     */
    @NotBlank
    @JsonProperty("userEventId")
    private String userEventId;
}
