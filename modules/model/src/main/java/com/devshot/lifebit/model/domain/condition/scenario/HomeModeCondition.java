package com.devshot.lifebit.model.domain.condition.scenario;

import com.devshot.lifebit.model.domain.ScenarioCondition;
import com.devshot.lifebit.model.domain.ScenarioConditionCheckBox;
import com.devshot.lifebit.model.enums.HomeMode;
import com.devshot.lifebit.model.enums.UserHomeRule;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Home mode condition. For check home mode when changed.
 *
 * @author Oleg Ivashkevich
 * date: 15.07.2017
 */
@Data
public class HomeModeCondition implements ScenarioCondition {

    @NotNull
    @JsonProperty("mode")
    private HomeMode mode;

    @Override
    public boolean check(@NotNull ScenarioConditionCheckBox box) {
        return box.getHomeMode() == mode;
    }
}
