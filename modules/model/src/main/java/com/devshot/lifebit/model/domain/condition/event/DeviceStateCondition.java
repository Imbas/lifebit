package com.devshot.lifebit.model.domain.condition.event;

import com.devshot.lifebit.model.domain.EventCondition;
import com.devshot.lifebit.model.enums.ChangeStateRule;
import com.devshot.lifebit.model.enums.ValueType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Device state change event condition.
 *
 * @author Oleg Ivashkevich
 * date: 15.07.2017
 */
@Data
public class DeviceStateCondition implements EventCondition {

    @NotNull
    @JsonProperty("changeRule")
    //TODO json special mapper
    private ChangeStateRule changeRule;

    /**
     * Expected device state.
     */
    @NotBlank
    @JsonProperty("expectedState")
    private String expectedState;

    @JsonIgnore
    //TODO: call check
    public boolean isValid(ValueType type) {
        switch (changeRule) {
            case EQUAL: {
                return type == ValueType.STRING || type == ValueType.ENUM;
            }
            case LESS:
            case OVER: {
                switch (type) {
                    case ENUM:
                    case STRING:
                        return false;
                    case INTEGER: {
                        try {
                            //noinspection ResultOfMethodCallIgnored
                            Integer.valueOf(expectedState);
                            return true;
                        } catch (NumberFormatException e) {
                            return false;
                        }
                    }
                    case FLOAT: {
                        try {
                            //noinspection ResultOfMethodCallIgnored
                            Float.valueOf(expectedState);
                            return true;
                        } catch (NumberFormatException e) {
                            return false;
                        }
                    }
                    default:
                        throw new AssertionError("Unsupported value type!");
                }
            }
            default:
                throw new AssertionError("Unsupported changeRule type!");
        }
    }
}
