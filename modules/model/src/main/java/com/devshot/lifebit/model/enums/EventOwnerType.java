package com.devshot.lifebit.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Owner of happened event.
 *
 * @author Oleg Ivashkevich
 * date: 17.07.2017
 */
@Getter
@RequiredArgsConstructor
public enum EventOwnerType implements Identifiable<String> {
    DEVICE("DEVICE"), USER("USER"), COMPLEX("COMPLEX"), SCHEDULE("SCHEDULE"), SYSTEM("SYSTEM");

    public final String id;
}
