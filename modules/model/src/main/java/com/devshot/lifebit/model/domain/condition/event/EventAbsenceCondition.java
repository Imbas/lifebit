package com.devshot.lifebit.model.domain.condition.event;

import com.devshot.lifebit.model.domain.EventCondition;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.Duration;

/**
 * Some event absence for a specified duration time.
 *
 * @author Oleg Ivashkevich
 * date: 28.08.2020
 */
@Data
public class EventAbsenceCondition implements EventCondition {
    /**
     * Identifier of target event type.
     */
    @NotNull
    @JsonProperty("eventTypeId")
    private Integer eventTypeId;

    /**
     * Source scenario device identifier for happened event.
     * If null - we check only event type as common absence case.
     */
    @JsonProperty("scenarioDeviceId")
    private Integer scenarioDeviceId;

    /**
     * Absence duration.
     */
    @NotNull
    @JsonProperty("duration")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Duration duration;
}
