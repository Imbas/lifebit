package com.devshot.lifebit.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Command base.
 *
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Getter
@RequiredArgsConstructor
public enum CommandBase implements Identifiable<String> {
    GET("GET"), SET("SET"), SWITCH("SWITCH");

    public final String id;
}
