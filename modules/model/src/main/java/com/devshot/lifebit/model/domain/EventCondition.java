package com.devshot.lifebit.model.domain;

import com.devshot.lifebit.model.domain.condition.*;
import com.devshot.lifebit.model.domain.condition.event.DeviceStateCondition;
import com.devshot.lifebit.model.domain.condition.event.EventAbsenceCondition;
import com.devshot.lifebit.model.domain.condition.event.UserEventCondition;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Condition of event identification on system.
 *
 * @author Oleg Ivashkevich
 * date: 15.07.2017
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = DeviceStateCondition.class, name = "DeviceState"),
        @JsonSubTypes.Type(value = UserEventCondition.class, name = "UserEvent"),
        @JsonSubTypes.Type(value = EventAbsenceCondition.class, name = "EventAbsence"),
        @JsonSubTypes.Type(value = CronCondition.class, name = "Cron"),
        @JsonSubTypes.Type(value = GroovyCondition.class, name = "Groovy"),
})
public interface EventCondition {
}
