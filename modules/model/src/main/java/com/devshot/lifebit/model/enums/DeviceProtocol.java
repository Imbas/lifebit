package com.devshot.lifebit.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Protocol of device.
 *
 * @author Oleg Ivashkevich
 * date: 22.07.2017
 */
@Getter
@RequiredArgsConstructor
public enum DeviceProtocol implements Identifiable<String> {
    ZWAVE("ZWAVE", true, "zwave", "zwave:device"),
    RADIO433("RADIO433", false, null, null);

    public final String id;
    public final boolean discovery;
    public final String discoveryName;
    public final String unknownModelName;
}
