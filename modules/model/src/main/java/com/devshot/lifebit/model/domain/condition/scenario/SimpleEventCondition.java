package com.devshot.lifebit.model.domain.condition.scenario;

import com.devshot.lifebit.model.domain.ScenarioCondition;
import com.devshot.lifebit.model.domain.ScenarioConditionCheckBox;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * One happened target event for scenario device - on which this condition will be invoked.
 *
 * @author Oleg Ivashkevich
 * date: 15.07.2017
 */
@Data
public class SimpleEventCondition implements ScenarioCondition {
    /**
     * Identifier of target event type.
     */
    @NotNull
    @JsonProperty("eventTypeId")
    private Integer eventTypeId;

    /**
     * Source scenario device identifier for happened event.
     */
    @NotNull
    @JsonProperty("scenarioDeviceId")
    private Integer scenarioDeviceId;

    @Override
    public boolean check(@NotNull ScenarioConditionCheckBox box) {
        return box.getTargetEventType() != null && eventTypeId.equals(box.getTargetEventType().getId())
                && (scenarioDeviceId == null || box.getDevice() != null && scenarioDeviceId.equals(box.getDevice().getId()));
    }
}
