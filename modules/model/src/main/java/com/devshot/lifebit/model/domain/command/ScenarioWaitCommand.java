package com.devshot.lifebit.model.domain.command;

import com.devshot.lifebit.model.enums.ScenarioCommandType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.Duration;

/**
 * Wait command for scenario.
 *
 * @author Oleg Ivashkevich
 * date: 25.07.2017
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ScenarioWaitCommand implements ScenarioCommand {
    @Override
    @JsonIgnore
    public ScenarioCommandType getType() {
        return ScenarioCommandType.WAIT;
    }

    /**
     * Wait duration.
     */
    @NotNull
    @JsonProperty("duration")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Duration duration;
}
