package com.devshot.lifebit.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Rule of state change.
 *
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Getter
@RequiredArgsConstructor
public enum ChangeStateRule implements Identifiable<String> {
    /**
     * Состояние изменилось на указанное значение
     */
    EQUAL("EQUAL"),

    /**
     * Состояние уменьшилось ниже указанного значнения
     */
    LESS("LESS"),

    /**
     * Состояние превысело указанное значнение
     */
    OVER("OVER");

    public final String id;
}
