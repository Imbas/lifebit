package com.devshot.lifebit.model.pojo;

import com.devshot.commons.x_model.jpa.NamerWithDescription;
import com.devshot.lifebit.model.enums.DeviceProtocol;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Device type - common device with executable active sub active components.
 *
 * @author Oleg Ivashkevich
 * date: 16.02.2020
 */
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name = "DeviceModel")
public class DeviceModel extends NamerWithDescription {

    /**
     * Protocol of device.
     */
    @NotNull
    @Column(name = "protocol", nullable = false)
    private DeviceProtocol protocol;

    /**
     * Category of device.
     */
    @Column(name = "category")
    private Integer category;

    /**
     * External device type (model) identifier in external system like openhub.
     */
    @Column(name = "external_id", unique = true)
    private String externalId;

    /**
     * Optional for check - possible component types for the device type.
     */
//    @ManyToMany
//    private List<ComponentType> components;

    /**
     * Optional for check - possible event types for device type.
     */
//    @ManyToMany
//    private List<EventType> eventTypes;
}
