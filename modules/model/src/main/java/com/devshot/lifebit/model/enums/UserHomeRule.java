package com.devshot.lifebit.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Rule of user home entrance.
 *
 * @author Oleg Ivashkevich
 * date: 15.11.2020
 */
@Getter
@RequiredArgsConstructor
public enum UserHomeRule implements Identifiable<String> {
    /**
     * Nobody is at home.
     */
    NOBODY("NOBODY"),

    /**
     * At least one user at home.
     */
    AL_LEAST_ONE("AL_LEAST_ONE"),

    /**
     * Special user home by id.
     */
    USER_ID("USER_ID");

    public final String id;
}
