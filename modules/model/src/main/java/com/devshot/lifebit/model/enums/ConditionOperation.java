package com.devshot.lifebit.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Condition operation - for composites.
 *
 * @author Oleg Ivashkevich
 * date: 15.11.2020
 */
@Getter
@RequiredArgsConstructor
public enum ConditionOperation implements Identifiable<String> {
    AND("AND"), OR("OR");

    public final String id;
}
