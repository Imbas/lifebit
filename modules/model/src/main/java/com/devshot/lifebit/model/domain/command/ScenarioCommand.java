package com.devshot.lifebit.model.domain.command;

import com.devshot.lifebit.model.enums.ScenarioCommandType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author Oleg Ivashkevich
 * date: 27.08.2020
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ScenarioDeviceCommand.class, name = "ScenarioDevice"),
        @JsonSubTypes.Type(value = ScenarioWaitCommand.class, name = "ScenarioWait"),
        @JsonSubTypes.Type(value = ScenarioAllLightsOffCommand.class, name = "ScenarioAllLightsOff")
})
public interface ScenarioCommand {
    @JsonIgnore
    ScenarioCommandType getType();
}
