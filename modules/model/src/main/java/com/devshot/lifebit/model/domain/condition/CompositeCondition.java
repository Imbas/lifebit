package com.devshot.lifebit.model.domain.condition;

import com.devshot.lifebit.model.domain.EventCondition;
import com.devshot.lifebit.model.domain.ScenarioCondition;
import com.devshot.lifebit.model.domain.ScenarioConditionCheckBox;
import com.devshot.lifebit.model.enums.ConditionOperation;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * Groovy-script condition.
 *
 * @author Oleg Ivashkevich
 * date: 15.07.2017
 */
@Data
public class CompositeCondition implements ScenarioCondition, EventCondition {
    @NotNull
    @JsonProperty("operation")
    private ConditionOperation operation;

    @NotNull
    @JsonProperty("conditionOne")
    private ScenarioCondition conditionOne;

    @NotNull
    @JsonProperty("conditionTwo")
    private ScenarioCondition conditionTwo;

    @Override
    public boolean check(@NotNull ScenarioConditionCheckBox box) {
        switch (operation) {
            case AND:
                return conditionOne.check(box) && conditionTwo.check(box);
            case OR:
                return conditionOne.check(box) || conditionTwo.check(box);
            default:
                throw new UnsupportedOperationException("Not supported composite operation");
        }
    }
}
