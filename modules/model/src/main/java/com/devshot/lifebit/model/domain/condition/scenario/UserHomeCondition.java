package com.devshot.lifebit.model.domain.condition.scenario;

import com.devshot.lifebit.model.domain.ScenarioCondition;
import com.devshot.lifebit.model.domain.ScenarioConditionCheckBox;
import com.devshot.lifebit.model.enums.UserHomeRule;
import com.devshot.lifebit.model.pojo.EventType;
import com.devshot.lifebit.model.pojo.ScenarioDevice;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * User home condition. For check when users at home.
 *
 * @author Oleg Ivashkevich
 * date: 15.07.2017
 */
@Data
public class UserHomeCondition implements ScenarioCondition {

    @NotNull
    @JsonProperty("rule")
    private UserHomeRule rule;

    /**
     * Special user id.
     */
    @NotBlank
    @JsonProperty("userId")
    private String userId;

    @Override
    public boolean check(@NotNull ScenarioConditionCheckBox box) {
        switch (rule) {
            case NOBODY:
                return box.getUsersAtHome().isEmpty();
            case AL_LEAST_ONE:
                return !box.getUsersAtHome().isEmpty();
            case USER_ID:
                return box.getUsersAtHome().contains(userId);
            default:
                throw new UnsupportedOperationException("Not supported user home rule");
        }
    }
}
