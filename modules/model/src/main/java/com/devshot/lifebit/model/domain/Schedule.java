package com.devshot.lifebit.model.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

/**
 * Schedule.
 *
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
//TODO validation
public class Schedule {
    @JsonProperty("startTime")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalTime startTime;

    @JsonProperty("endTime")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalTime endTime;

    public boolean check() {
        LocalTime now = LocalTime.now();
        return startTime.isBefore(endTime)
                ? !now.isBefore(startTime) && !now.isAfter(endTime)
                : !now.isBefore(startTime) && !now.isAfter(LocalTime.of(23, 59))
                || !now.isBefore(LocalTime.of(0, 0)) && !now.isAfter(endTime);
    }
}
