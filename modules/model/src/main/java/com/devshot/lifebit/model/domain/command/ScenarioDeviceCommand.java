package com.devshot.lifebit.model.domain.command;

import com.devshot.lifebit.model.enums.ScenarioCommandType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.validation.constraints.NotNull;

/**
 * Device command for scenario.
 *
 * @author Oleg Ivashkevich
 * date: 25.07.2017
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ScenarioDeviceCommand extends DeviceCommand implements ScenarioCommand {
    @Override
    @JsonIgnore
    public ScenarioCommandType getType() {
        return ScenarioCommandType.SCENARIO_DEVICE;
    }

    /**
     * Scenario device identifier.
     */
    @NotNull
    @JsonProperty("scenarioDeviceId")
    private Integer scenarioDeviceId;
}
