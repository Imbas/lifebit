package com.devshot.lifebit.model.domain.command;

import com.devshot.lifebit.model.enums.ScenarioCommandType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Special command for scenario - turn off all lights.
 *
 * @author Oleg Ivashkevich
 * date: 25.07.2017
 */
@Data
public class ScenarioAllLightsOffCommand implements ScenarioCommand {
    @Override
    @JsonIgnore
    public ScenarioCommandType getType() {
        return ScenarioCommandType.ALL_LIGHTS_OFF;
    }
}
