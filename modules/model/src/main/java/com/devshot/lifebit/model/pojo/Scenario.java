package com.devshot.lifebit.model.pojo;

import com.devshot.commons.x_model.jpa.NamerWithDescription;
import com.devshot.lifebit.model.domain.ScenarioCondition;
import com.devshot.lifebit.model.domain.Schedule;
import com.devshot.lifebit.model.domain.command.ScenarioCommand;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Scenario for Home Automation.
 *
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name = "Scenario")
public class Scenario extends NamerWithDescription {

    /**
     * Main variable - condition of scenario execution.
     */
    @NotNull
    @Valid
    @Column(name = "condition", nullable = false, length = 1024)
    private ScenarioCondition condition;

    /**
     * Command to execute by scenario.
     */
    @NotEmpty
    @Valid
    @Column(name = "commands", nullable = false)
    private List<ScenarioCommand> commands;

    /**
     * Need to confirm scenario by user before execution.
     */
    @NotNull
    @Column(name = "need_to_confirm", nullable = false)
    private boolean needToConfirm;

    /**
     * Disable scenario tool.
     */
    @NotNull
    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    /**
     * Working schedule.
     */
    @Column(name = "schedule")
    private Schedule schedule;

    /**
     * Scenario execution flow.
     * In one moment can be only 1 scenario from 1 flow. Latest scenario cancels current flow scenario.
     */
    @Column(name = "flow")
    private Integer flow;
}
