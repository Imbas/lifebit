package com.devshot.lifebit.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Command type for send.
 *
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Getter
@RequiredArgsConstructor
public enum ScenarioCommandType implements Identifiable<String> {
    SCENARIO_DEVICE("SCENARIO_DEVICE"), WAIT("WAIT"), ALL_LIGHTS_OFF("ALL_LIGHTS_OFF");

    public final String id;
}
