package com.devshot.lifebit.model.domain;

import com.devshot.lifebit.model.enums.HomeMode;
import com.devshot.lifebit.model.pojo.EventType;
import com.devshot.lifebit.model.pojo.ScenarioDevice;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Set;

/**
 * @author Oleg Ivashkevich
 * date: 15.11.2020
 */
@Data
@Builder
@RequiredArgsConstructor
public class ScenarioConditionCheckBox {
    /**
     * Target event type for execution - event on which happening we check the scenario.
     */
    private final EventType targetEventType;

    /**
     * Specified scenario device of happened event, if null - no check.
     */
    private final ScenarioDevice device;

    /**
     * Users that present at home - ids.
     */
    private final Set<String> usersAtHome;

    /**
     * Current home mode.
     */
    private final HomeMode homeMode;
}
