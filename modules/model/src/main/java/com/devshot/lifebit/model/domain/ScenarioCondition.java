package com.devshot.lifebit.model.domain;

import com.devshot.lifebit.model.domain.condition.CompositeCondition;
import com.devshot.lifebit.model.domain.condition.CronCondition;
import com.devshot.lifebit.model.domain.condition.GroovyCondition;
import com.devshot.lifebit.model.domain.condition.scenario.HomeModeCondition;
import com.devshot.lifebit.model.domain.condition.scenario.SimpleEventCondition;
import com.devshot.lifebit.model.domain.condition.scenario.UserHomeCondition;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.validation.constraints.NotNull;

/**
 * Scenario happaning condition.
 *
 * @author Oleg Ivashkevich
 * date: 15.07.2017
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = SimpleEventCondition.class, name = "SimpleEvent"),
        @JsonSubTypes.Type(value = UserHomeCondition.class, name = "UserHome"),
        @JsonSubTypes.Type(value = HomeModeCondition.class, name = "HomeMode"),
        @JsonSubTypes.Type(value = CronCondition.class, name = "Cron"),
        @JsonSubTypes.Type(value = GroovyCondition.class, name = "Groovy"),
        @JsonSubTypes.Type(value = CompositeCondition.class, name = "Composite")
})
public interface ScenarioCondition {
    /**
     * Check scenario execution.
     *
     * @param box special parameters for condition check
     * @return invocation
     */
    boolean check(@NotNull ScenarioConditionCheckBox box);
}
