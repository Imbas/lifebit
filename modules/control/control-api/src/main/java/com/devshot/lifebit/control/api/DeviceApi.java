package com.devshot.lifebit.control.api;

import com.devshot.lifebit.control.model.data.ActiveDevice;
import com.devshot.lifebit.control.model.pojo.Device;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

/**
 * @author Oleg Ivashkevich
 * date: 07.11.2020
 */
@RequestMapping("/device")
public interface DeviceApi {
    @GetMapping("/")
    List<Device> getAll();

    @GetMapping("/dashboard")
    Map<String, ActiveDevice> dashboard();

    @PutMapping("/sync")
    void sync();
}
