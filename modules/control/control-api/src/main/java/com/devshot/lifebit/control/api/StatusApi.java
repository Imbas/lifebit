package com.devshot.lifebit.control.api;

import com.devshot.lifebit.control.model.data.ServerStatus;
import com.devshot.lifebit.model.enums.HomeMode;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Oleg Ivashkevich
 * date: 07.11.2020
 */
@RequestMapping("/status")
public interface StatusApi {
    @GetMapping
    ServerStatus status();

    @PutMapping("/mode/{mode}")
    void mode(@PathVariable(name = "mode") HomeMode mode);
}
