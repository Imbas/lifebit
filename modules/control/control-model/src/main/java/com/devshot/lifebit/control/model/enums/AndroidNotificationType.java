package com.devshot.lifebit.control.model.enums;

/**
 * @author Oleg Ivashkevich
 * date: 26.10.2020
 */
public enum AndroidNotificationType {
    ALARM, UPDATE
}
