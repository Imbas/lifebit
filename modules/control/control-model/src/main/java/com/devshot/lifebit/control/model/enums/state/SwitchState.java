package com.devshot.lifebit.control.model.enums.state;

import com.devshot.lifebit.control.model.data.state.ComponentState;

/**
 * @author Oleg Ivashkevich
 * date: 29.11.2020
 */
public enum SwitchState implements ComponentState {
    ON, OFF;

    @Override
    public SwitchState switchState() {
        return this;
    }
}
