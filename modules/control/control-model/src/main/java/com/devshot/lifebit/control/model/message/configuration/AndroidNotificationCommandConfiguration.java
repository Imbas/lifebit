package com.devshot.lifebit.control.model.message.configuration;

import com.devshot.commons.message.data.BlankResult;
import com.devshot.commons.message.data.CommandConfiguration;
import com.devshot.lifebit.control.model.message.AndroidNotificationInfo;

/**
 * @see AndroidNotificationInfo
 * @author Oleg Ivashkevich
 * date: 09.05.2020
 */
public class AndroidNotificationCommandConfiguration extends CommandConfiguration<AndroidNotificationInfo, BlankResult> {
    public AndroidNotificationCommandConfiguration(String topic) {
        super("android-notification", topic, true, AndroidNotificationInfo.class, BlankResult.class);
    }
}