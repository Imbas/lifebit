package com.devshot.lifebit.control.model.pojo;

import com.devshot.commons.x_model.jpa.Identifier;
import com.devshot.lifebit.control.model.enums.EventState;
import com.devshot.lifebit.model.pojo.EventType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

/**
 * House system event.
 *
 * @author Oleg Ivashkevich
 * date: 17.07.2017
 */
@Getter
@Setter
@ToString(callSuper = true)
@EqualsAndHashCode(of = {"type", "registerDate"}, callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder

@Entity
@Table(name = "Event")
public class Event extends Identifier {

    /**
     * Event type.
     */
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = EventType.class)
    @JoinColumn(name = "type_id", nullable = false)
    private EventType type;

    /**
     * Event remote register date.
     */
    @NotNull
    @Column(name = "register_date", nullable = false)
    private ZonedDateTime registerDate;

    /**
     * Active device component that call event.
     * May be null, when it's not device event.
     */
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Component.class)
    @JoinColumn(name = "component_id")
    private Component component;

    /**
     * User that call event.
     */
    //TODO FUTURE direct user connection
    @Column(name = "user_id")
    private Integer user;

    /**
     * Event state - used when confirmation by user enabled.
     */
    @NotNull
    @Column(name = "state", nullable = false)
    private EventState state;

    /**
     * Simple events - current state, complex ones - some event data.
     */
    @Column(name = "data")
    private String data;

    @Valid
    @JsonIgnore
    public boolean validation() {
        return component != null || user != null;
    }

    @JsonIgnore
    public String buildKey() {
        return type.getName() + ":" + registerDate;
    }
}
