package com.devshot.lifebit.control.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Event state - used when confirmation by user enabled.
 *
 * @author Oleg Ivashkevich
 * date: 17.07.2017
 */
@Getter
@RequiredArgsConstructor
public enum EventState implements Identifiable<String> {
    CREATED("CREATED"), APPROVED("APPROVED"), REJECTED("REJECTED");

    public final String id;
}
