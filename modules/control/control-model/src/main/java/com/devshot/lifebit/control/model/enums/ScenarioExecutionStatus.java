package com.devshot.lifebit.control.model.enums;

import com.devshot.commons.general.vo.Identifiable;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Scenario execution status.
 *
 * @author Oleg Ivashkevich
 * date: 17.07.2017
 */
@Getter
@RequiredArgsConstructor
public enum ScenarioExecutionStatus implements Identifiable<String> {
    CREATED("CREATED"), APPROVED("APPROVED"), REJECTED("REJECTED"), RUNNING("RUNNING"), SUCCESS("SUCCESS"), FAIL("FAIL");

    public final String id;
}
