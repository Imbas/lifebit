package com.devshot.lifebit.control.model.enums;

/**
 * Device candidat status identification.
 *
 * @author Oleg Ivashkevich
 * date: 22.07.2017
 */
public enum DeviceCandidateStatus {
    NEW, EXISTS
}
