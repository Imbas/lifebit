package com.devshot.lifebit.control.model.message;

import com.devshot.lifebit.control.model.enums.AndroidUserEventType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

/**
 * Special user event on android, sending to server.
 *
 * @author Oleg Ivashkevich
 * date: 26.10.2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AndroidUserEventInfo {
    private AndroidUserEventType type;
    private ZonedDateTime date;
    private String info;
    private String userId;
}
