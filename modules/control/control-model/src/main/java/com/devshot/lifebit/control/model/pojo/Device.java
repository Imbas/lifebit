package com.devshot.lifebit.control.model.pojo;

import com.devshot.commons.x_model.jpa.Namer;
import com.devshot.lifebit.model.pojo.DeviceModel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

import static javax.persistence.FetchType.LAZY;

/**
 * Key entity - device, concrete object.
 *
 * @author Oleg Ivashkevich
 * @see DeviceModel
 * date: 17.07.2017
 */
@Getter
@Setter
@ToString(exclude = {"components"}, callSuper = true)

@Entity
@Table(name = "Device")
public class Device extends Namer {
    /**
     * Device model (type).
     */
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = DeviceModel.class)
    @JoinColumn(name = "model_id", nullable = false)
    private DeviceModel model;

    /**
     * Device location.
     */
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Location.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "location_id", nullable = false)
    private Location location;

    /**
     * External device identifier in external system like openhub.
     */
    @Column(name = "external_id", unique = true)
    private String externalId;

    /**
     * Few active devive components.
     */
    @JsonIgnore
    @OneToMany(fetch = LAZY, targetEntity = Component.class, mappedBy = "device", cascade = CascadeType.ALL)
    private List<Component> components;

    /**
     * Проверка на совпадение неизменяемых полей
     */
    @JsonIgnore
    public boolean secureChangeEquals(Device old) {
        if (!id.equals(old.id)) return false;
        if (!model.equals(old.model)) return false;
        if (!externalId.equals(old.externalId)) return false;

        //TODO FUTURE
//        if (items.size() != old.items.size()) return false;
//        for (int i = 0; i < items.size(); i++)
//            if (!items.get(i).secureChangeEquals(old.items.get(i)))
//                return false;

        return true;
    }
}
