package com.devshot.lifebit.control.model.data.state;

import com.devshot.lifebit.control.model.enums.state.ContactState;
import com.devshot.lifebit.control.model.enums.state.SwitchState;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Oleg Ivashkevich
 * date: 30.11.2020
 */
@Data
@NoArgsConstructor
public class IntegerState implements ComponentState {
    @JsonValue
    private int state;

    public IntegerState(int state) {
        this.state = state;
    }

    public IntegerState(String state) {
        this.state = Integer.parseInt(state);
    }

    @Override
    public int percentState() {
        return state;
    }

    @Override
    public ContactState contactState() {
        return state > 1 ? ContactState.OPENED : ContactState.CLOSED;
    }

    @Override
    public SwitchState switchState() {
        return state > 1 ? SwitchState.ON : SwitchState.OFF;
    }

    @Override
    public String toString() {
        return String.valueOf(state);
    }
}
