package com.devshot.lifebit.control.model.data.state;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Oleg Ivashkevich
 * date: 30.11.2020
 */
@Data
@NoArgsConstructor
public class FloatState implements ComponentState {
    @JsonValue
    private float state;

    public FloatState(float state) {
        this.state = state;
    }

    public FloatState(double state) {
        this.state = (float) state;
    }

    public FloatState(String state) {
        this.state = Float.parseFloat(state);
    }

    @Override
    public float temperatureState() {
        return state;
    }

    @Override
    public float luminanceState() {
        return state;
    }

    @Override
    public String toString() {
        return String.valueOf(state);
    }
}
