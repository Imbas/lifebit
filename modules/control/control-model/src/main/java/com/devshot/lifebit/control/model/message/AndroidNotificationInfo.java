package com.devshot.lifebit.control.model.message;

import com.devshot.lifebit.control.model.enums.AndroidNotificationType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

/**
 * Broadcost android notification, sending to all androids.
 * 
 * @author Oleg Ivashkevich
 * date: 26.10.2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AndroidNotificationInfo {
    private AndroidNotificationType type;
    private ZonedDateTime date;
    private String info;
}
