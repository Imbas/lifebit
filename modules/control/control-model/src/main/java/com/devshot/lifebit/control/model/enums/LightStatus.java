package com.devshot.lifebit.control.model.enums;

/**
 * Tag - is any light is ON in the house.
 *
 * @author Oleg Ivashkevich
 * date: 05.11.2020
 */
public enum LightStatus {
    ON, OFF
}
