package com.devshot.lifebit.control.model.data;

import com.devshot.lifebit.control.model.enums.DoorStatus;
import com.devshot.lifebit.model.enums.HomeMode;
import com.devshot.lifebit.control.model.enums.LightStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author Oleg Ivashkevich
 * date: 26.10.2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ServerStatus {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDateTime date;
    private HomeMode mode;
    private GeoLocation location;
    private DoorStatus doorStatus;
    private LightStatus lightStatus;
}
