package com.devshot.lifebit.control.model.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author Oleg Ivashkevich
 * date: 26.10.2020
 */
@Getter
@RequiredArgsConstructor
public enum AndroidUserEventType {
    GEO_NEAR_HOME("GEO_NEAR_HOME"), NETWORK_HOME_ON("NETWORK_HOME_ON"), NETWORK_HOME_OFF("NETWORK_HOME_OFF");

    public final String id;
}
