package com.devshot.lifebit.control.model.data.state;

import com.devshot.lifebit.control.model.enums.state.ContactState;
import com.devshot.lifebit.control.model.enums.state.SwitchState;
import com.devshot.lifebit.model.domain.condition.*;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author Oleg Ivashkevich
 * date: 29.11.2020
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ContactState.class, name = "contact"),
        @JsonSubTypes.Type(value = SwitchState.class, name = "switch"),
        @JsonSubTypes.Type(value = FloatState.class, name = "float"),
        @JsonSubTypes.Type(value = IntegerState.class, name = "int"),
})
public interface ComponentState {
    default ContactState contactState() {
        throw new UnsupportedOperationException("ContactState is unsupported");
    }

    default SwitchState switchState() {
        throw new UnsupportedOperationException("SwitchState is unsupported");
    }

    default float temperatureState() {
        throw new UnsupportedOperationException("TemperatureState is unsupported");
    }

    default float luminanceState() {
        throw new UnsupportedOperationException("LuminanceState is unsupported");
    }

    default int percentState() {
        throw new UnsupportedOperationException("PercentState is unsupported");
    }

    default String state() {
        throw new UnsupportedOperationException("Usual state is unsupported");
    }
}
