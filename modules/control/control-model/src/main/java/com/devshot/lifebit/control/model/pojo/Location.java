package com.devshot.lifebit.control.model.pojo;

import com.devshot.commons.x_model.jpa.Namer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Location for devices.
 *
 * @author Oleg Ivashkevich
 * date: 17.07.2017
 */
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name = "Location")
public class Location extends Namer {
    @Column(name = "category")
    private Integer category;
}
