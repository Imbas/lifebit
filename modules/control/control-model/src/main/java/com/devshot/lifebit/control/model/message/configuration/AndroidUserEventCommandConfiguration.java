package com.devshot.lifebit.control.model.message.configuration;

import com.devshot.commons.message.data.BlankResult;
import com.devshot.commons.message.data.CommandConfiguration;
import com.devshot.lifebit.control.model.message.AndroidUserEventInfo;

/**
 * @see AndroidUserEventInfo
 * @author Oleg Ivashkevich
 * date: 09.05.2020
 */
public class AndroidUserEventCommandConfiguration extends CommandConfiguration<AndroidUserEventInfo, BlankResult> {
    public AndroidUserEventCommandConfiguration(String topic) {
        super("android-user-event", topic, true, AndroidUserEventInfo.class, BlankResult.class);
    }
}