package com.devshot.lifebit.control.model.pojo;

import com.devshot.commons.x_model.jpa.Namer;
import com.devshot.lifebit.model.pojo.ComponentType;
import com.devshot.lifebit.model.pojo.DeviceModel;
import com.devshot.lifebit.model.pojo.ScenarioDevice;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Key sub entity - device component, active command or event component, abstract common entity.
 *
 * @author Oleg Ivashkevich
 * @see DeviceModel
 * date: 17.07.2017
 */
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name = "Component")
public class Component extends Namer {
    /**
     * Device model (type).
     */
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = ComponentType.class)
    @JoinColumn(name = "type_id", nullable = false)
    private ComponentType type;

    /**
     * External device identifier in external system like openhub.
     */
    @Column(name = "external_id", unique = true)
    private String externalId;

    /**
     * Device owner for component.
     */
    @JsonIgnore
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Device.class)
    @JoinColumn(name = "device_id", nullable = false)
    private Device device;

    /**
     * Key field for scenario - scenario device connection to execute scenario commands - for device component command execution.
     */
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = ScenarioDevice.class)
    @JoinColumn(name = "scenario_device_id")
    private ScenarioDevice scenarioDevice;
}
