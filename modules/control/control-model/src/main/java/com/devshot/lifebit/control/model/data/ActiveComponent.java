package com.devshot.lifebit.control.model.data;

import com.devshot.lifebit.control.model.data.state.ComponentState;
import com.devshot.lifebit.control.model.pojo.Component;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Active component info with state.
 *
 * @author Oleg Ivashkevich
 * date: 29.11.2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActiveComponent {
    private Component component;
    private ComponentState state;
}
