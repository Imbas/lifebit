package com.devshot.lifebit.control.model.data;

import com.devshot.lifebit.control.model.pojo.Device;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Active device info with active components and states.
 *
 * @author Oleg Ivashkevich
 * date: 29.11.2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActiveDevice {
    private Device device;
    private List<ActiveComponent> components;
}
