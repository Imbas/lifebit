package com.devshot.lifebit.control.model.enums.state;

import com.devshot.lifebit.control.model.data.state.ComponentState;

/**
 * @author Oleg Ivashkevich
 * date: 29.11.2020
 */
public enum ContactState implements ComponentState {
    CLOSED, OPENED;

    @Override
    public ContactState contactState() {
        return this;
    }
}
