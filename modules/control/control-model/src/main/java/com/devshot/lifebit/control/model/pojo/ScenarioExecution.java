package com.devshot.lifebit.control.model.pojo;

import com.devshot.commons.x_model.jpa.Identifier;
import com.devshot.lifebit.control.model.enums.ScenarioExecutionStatus;
import com.devshot.lifebit.model.pojo.Scenario;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

/**
 * Scenario execution log.
 *
 * @author Oleg Ivashkevich
 * date: 17.07.2017
 */
@Getter
@Setter
@ToString(callSuper = true)

@Entity
@Table(name = "ScenarioExecution")
public class ScenarioExecution extends Identifier {

    /**
     * Scenario.
     */
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Scenario.class)
    @JoinColumn(name = "scenario_id", nullable = false)
    private Scenario scenario;

    /**
     * Scenario execution date.
     */
    @NotNull
    @Column(name = "execution_date", nullable = false)
    private ZonedDateTime executionDate;

    /**
     * Scenario execution status.
     */
    @NotNull
    @Column(name = "status", nullable = false)
    private ScenarioExecutionStatus status;
}
