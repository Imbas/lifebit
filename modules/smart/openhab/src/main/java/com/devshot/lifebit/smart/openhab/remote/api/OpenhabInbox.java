package com.devshot.lifebit.smart.openhab.remote.api;

import com.devshot.lifebit.smart.openhab.remote.model.OpenhabInboxThing;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Найденные при сканировании устройства
 *
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@RequestMapping("/rest/inbox")
public interface OpenhabInbox {
    /**
     * Get all discovered things.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    List<OpenhabInboxThing> getAll();

    /**
     * Removes the discovery result from the inbox.
     */
    @RequestMapping(value = "/{thingUID}", method = RequestMethod.DELETE)
    void remove(@PathVariable("thingUID") String thingUID);

    /**
     * Approves the discovery result by adding the thing to the registry.
     */
    @RequestMapping(value = "/{thingUID}/approve", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    void approve(@PathVariable("thingUID") String thingUID, @RequestBody String thingLabel);

    /**
     * Flags a discovery result as ignored for further processing.
     */
    @RequestMapping(value = "/{thingUID}/ignore", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    void ignore(@PathVariable("thingUID") String thingUID);

    /**
     * Removes ignore flag from a discovery result.
     */
    @RequestMapping(value = "/{thingUID}/unignore", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    void unignore(@PathVariable("thingUID") String thingUID);
}
