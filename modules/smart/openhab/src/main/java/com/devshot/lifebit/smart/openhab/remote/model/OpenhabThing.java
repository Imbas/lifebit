package com.devshot.lifebit.smart.openhab.remote.model;

import com.devshot.lifebit.smart.openhab.remote.model.enums.OpenhabStatus;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Openhab устройство
 *
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@Data
@EqualsAndHashCode(of = "UID")
public class OpenhabThing {
    @Data
    @EqualsAndHashCode(of = "status")
    public static class Status {
        @JsonProperty
        private OpenhabStatus status;

        @JsonProperty
        private String statusDetail;
    }

    @JsonProperty
    private String label;

    @JsonProperty("UID")
    private String UID;

    @JsonProperty
    private String thingTypeUID;

    @JsonProperty
    private String bridgeUID;

    @JsonProperty
    private String location;

    @JsonProperty
    private boolean editable;

    @JsonProperty("statusInfo")
    private Status status;

    @JsonProperty
    private List<OpenhabChannel> channels;
}
