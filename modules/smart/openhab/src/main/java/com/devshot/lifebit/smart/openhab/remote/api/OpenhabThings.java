package com.devshot.lifebit.smart.openhab.remote.api;

import com.devshot.lifebit.smart.openhab.remote.model.OpenhabThing;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Устройства
 *
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@RequestMapping("/rest/things")
public interface OpenhabThings {
    /**
     * Get all available things.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    List<OpenhabThing> getAll();

    /**
     * Gets thing by UID.
     */
    @RequestMapping(value = "/{thingUID}", method = RequestMethod.GET)
    OpenhabThing get(@PathVariable("thingUID") String thingUID);

    /**
     * Creates a new thing and adds it to the registry.
     */
    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    void add(@RequestBody OpenhabThing thing);

    /**
     * Updates a thing.
     */
    @RequestMapping(value = "/{thingUID}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    void change(@PathVariable("thingUID") String thingUID, @RequestBody OpenhabThing thing);

    /**
     * Removes a thing from the registry. Set 'force' to __true__ if you want the thing te be removed immediately.
     */
    @RequestMapping(value = "/{thingUID}", method = RequestMethod.DELETE)
    void remove(@PathVariable("thingUID") String thingUID, @RequestParam(value = "force", required = false) boolean force);

    /**
     * Links item to a channel. Creates item if such does not exist yet.
     * @param channelId идентификатор канала - его последний блок в UUID
     */
    @RequestMapping(value = "/{thingUID}/channels/{channelId}/link", method = RequestMethod.POST, produces = MediaType.TEXT_PLAIN_VALUE)
    void linkItem(@PathVariable("thingUID") String thingUID, @PathVariable("channelId") String channelId, @RequestBody String itemName);

    /**
     * Unlinks item from a channel.
     * @param channelId идентификатор канала - его последний блок в UUID
     * FIXME: 200 OK, но не работает - https://github.com/openhab/openhab-core/issues/170
     */
    @RequestMapping(value = "/{thingUID}/channels/{channelId}/link", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    void unlinkItem(@PathVariable("thingUID") String thingUID, @PathVariable("channelId") String channelId);
}
