package com.devshot.lifebit.smart.openhab.remote.model.enums;

/**
 * Статус устройств Openhab
 *
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
public enum OpenhabStatus {
    /**
     * This is the initial status of a thing, when it is added or the framework is being started. This status is also
     * assigned, if the initializing process failed or the binding is not available. Commands, which are sent to channels
     * will not be processed.
     */
    UNINITIALIZED,

    /**
     * This state is assigned while the binding initializes the thing. It depends on the binding how long the initializing
     * process takes. Commands, which are sent to channels will not be processed.
     */
    INITIALIZING,

    /**
     * The handler is fully initialized but due to the nature of the represented device/service it cannot really tell yet
     * whether the thing is ONLINE or OFFLINE. Therefore the thing potentially might be working correctly already and may
     * or may not process commands. But the framework is allowed to send commands, because some radio-based devices may
     * go ONLINE if a command is sent to them. The handler should take care to switch the thing to ONLINE or OFFLINE as
     * soon as possible.
     */
    UNKNOWN,

    /**
     * The device/service represented by a thing is assumed to be working correctly and can process commands.
     */
    ONLINE,

    /**
     * The device/service represented by a thing is assumed to be not working correctly and may not process commands.
     * But the framework is allowed to send commands, because some radio-based devices may go back to ONLINE, if a command
     * is sent to them.
     */
    OFFLINE,

    /**
     * The device/service represented by a thing should be removed, but the binding did not confirm the deletion yet.
     * Some bindings need to communicate with the device to unpair it from the system. Thing is probably not working and
     * commands can not be processed.
     */
    REMOVING,

    /**
     * This status indicates that the device/service represented by a thing was removed from the external system after
     * the REMOVING was initiated by the framework. Usually this status is an intermediate status because the thing gets
     * removed from the database after this status was assigned.
     */
    REMOVED
}
