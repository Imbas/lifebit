package com.devshot.lifebit.smart.openhab.remote.api;

import com.devshot.lifebit.smart.openhab.remote.model.OpenhabLink;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Связь Items с Things
 *
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@RequestMapping("/rest/links")
public interface OpenhabLinks {
    /**
     * Gets all available links.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    List<OpenhabLink> getAll();

    /**
     * Tells whether automatic link mode is active or not
     */
    @RequestMapping(value = "/auto", method = RequestMethod.GET)
    boolean isAuto();

    /**
     * Links item to a channel.
     * @param channelUID полный UUID канала
     */
    @RequestMapping(value = "/{itemName}/{channelUID}", method = RequestMethod.PUT)
    void addToChannel(@PathVariable("itemName") String itemName, @PathVariable("channelUID") String channelUID);

    /**
     * Unlinks item from a channel
     * @param channelUID полный UUID канала
     */
    @RequestMapping(value = "/{itemName}/{channelUID}", method = RequestMethod.DELETE)
    void removeFromChannel(@PathVariable("itemName") String itemName, @PathVariable("channelUID") String channelUID);
}
