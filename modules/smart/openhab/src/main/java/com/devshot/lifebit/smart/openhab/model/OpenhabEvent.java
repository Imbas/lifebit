package com.devshot.lifebit.smart.openhab.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Oleg Ivashkevich
 * date: 09.05.2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpenhabEvent {
    private String sensorId;
    private String state;
    private Long date;
}
