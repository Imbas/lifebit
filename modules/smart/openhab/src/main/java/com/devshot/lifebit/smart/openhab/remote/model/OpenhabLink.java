package com.devshot.lifebit.smart.openhab.remote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author Oleg Ivashkevich
 * date: 21.07.2017
 */
@Data
public class OpenhabLink {
    @JsonProperty
    private String channelUID;

    @JsonProperty
    private String itemName;
}
