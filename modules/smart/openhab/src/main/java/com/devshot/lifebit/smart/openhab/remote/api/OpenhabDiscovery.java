package com.devshot.lifebit.smart.openhab.remote.api;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Сканирование устройств
 *
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@RequestMapping("/rest/discovery")
public interface OpenhabDiscovery {
    /**
     * Gets all bindings that support discovery.
     * @return bindingIds
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    List<String> getAllSupportedBindings();

    /**
     * Starts asynchronous discovery process for a binding and returns the timeout in seconds of the discovery operation.
     * * Здесь и далее - запрос POST на Openhab даже без тела требует тип контента - application/json
     */
    @RequestMapping(value = "/bindings/{bindingId}/scan", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    void start(@PathVariable("bindingId") String bindingId);
}
