package com.devshot.lifebit.smart.openhab.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Oleg Ivashkevich
 * date: 26.10.2018
 */
@Configuration
@ComponentScan("com.devshot.lifebit.smart.openhab.config")
@ComponentScan("com.devshot.lifebit.smart.openhab.service")
public class OpenhabContextConfiguration {
}
