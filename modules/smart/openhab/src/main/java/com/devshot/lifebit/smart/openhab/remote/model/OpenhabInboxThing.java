package com.devshot.lifebit.smart.openhab.remote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Openhab найденное устройство (заявка)
 *
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@Data
@EqualsAndHashCode(of = "thingUID")
public class OpenhabInboxThing {
    @JsonProperty
    private String bridgeUID;

    /**
     * NEW, IGNORED
     */
    @JsonProperty
    private String flag;

    @JsonProperty
    private String label;

    @JsonProperty
    private String thingUID;

    @JsonProperty
    private String thingTypeUID;
}
