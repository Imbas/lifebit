package com.devshot.lifebit.smart.openhab.configuration;

import com.devshot.commons.general.GeneralContextConfiguration;
import com.devshot.commons.http.HttpContextConfiguration;
import com.devshot.commons.http.feign.FeignClientBuilder;
import com.devshot.lifebit.smart.openhab.config.OpenhabConfig;
import com.devshot.lifebit.smart.openhab.remote.api.*;
import feign.RequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Oleg Ivashkevich
 * date: 03.05.2020
 */
@Configuration
@Import({
        GeneralContextConfiguration.class,
        HttpContextConfiguration.class,
})
//http://demo.openhab.org:8080/doc/index.html#/
public class OpenhabHttpRemoteClientsConfiguration {

    @Bean
    public RequestInterceptor textPlainRequestInterceptor() {
        return template -> {
            if (template.body() != null) {
                template.body(new String(template.body()).replaceAll("\"", ""));
            }
        };
    }

    // ------ CLIENTS ------

    @Bean
    public OpenhabDiscovery openhabDiscovery(OpenhabConfig config, FeignClientBuilder builder) {
        return builder.buildClient(OpenhabDiscovery.class, config.getUrl());
    }

    @Bean
    public OpenhabInbox openhabInbox(OpenhabConfig config, FeignClientBuilder builder) {
        return builder.buildClient(OpenhabInbox.class, config.getUrl());
    }

    @Bean
    public OpenhabThings openhabThings(OpenhabConfig config, FeignClientBuilder builder) {
        return builder.buildClient(OpenhabThings.class, config.getUrl());
    }

    @Bean
    public OpenhabItems openhabItems(OpenhabConfig config, FeignClientBuilder builder) {
        return builder.buildClient(OpenhabItems.class, config.getUrl());
    }

    @Bean
    public OpenhabLinks openhabLinks(OpenhabConfig config, FeignClientBuilder builder) {
        return builder.buildClient(OpenhabLinks.class, config.getUrl());
    }
}
