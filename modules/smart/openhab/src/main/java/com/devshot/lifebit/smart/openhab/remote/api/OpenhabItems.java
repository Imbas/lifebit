package com.devshot.lifebit.smart.openhab.remote.api;

import com.devshot.lifebit.smart.openhab.remote.model.OpenhabItem;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Информационно-исполнительные части устройства
 *
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@RequestMapping("/rest/items")
public interface OpenhabItems {
    /**
     * Get all available items.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    List<OpenhabItem> getAll(@RequestParam(value = "type", required = false) String type,
                             @RequestParam(value = "tags", required = false) List<String> tags,
                             @RequestParam(value = "recursive", required = false) boolean recursive);

    /**
     * Gets a single item.
     */
    @RequestMapping(value = "/{itemname}", method = RequestMethod.GET)
    OpenhabItem get(@PathVariable("itemname") String itemname);

    /**
     * Sends a command to an item.
     */
    @RequestMapping(value = "/{itemname}", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    void sendCommand(@PathVariable("itemname") String itemname, @RequestBody String command);

    /**
     * Adds a new item to the registry or updates the existing item.
     * *** не менять OpenhabItem#name - запрос не отрабатывает
     */
    @RequestMapping(value = "/{itemname}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    void save(@PathVariable("itemname") String itemname, @RequestBody OpenhabItem item);

    /**
     * Removes an item from the registry.
     * *** если item привязан к устройству, то удаление происходит сразу же после отвязки
     */
    @RequestMapping(value = "/{itemname}", method = RequestMethod.DELETE)
    void remove(@PathVariable("itemname") String itemname);

    /**
     * Gets the state of an item.
     */
    @RequestMapping(value = "/{itemname}/state", method = RequestMethod.GET)
    String getState(@PathVariable("itemname") String itemname);

    /**
     * Updates the state of an item.
     * *** плохой способ отправки команд на оборудование - state изменяется, но никакого действия не проиходит
     */
    @RequestMapping(value = "/{itemname}/state", method = RequestMethod.PUT, consumes = MediaType.TEXT_PLAIN_VALUE)
    void changeState(@PathVariable("itemname") String itemname, @RequestBody Object state);

    /**
     * Adds a new member to a group item.
     */
    @RequestMapping(value = "/{itemname}/members/{memberItemName}", method = RequestMethod.PUT)
    void addToGroup(@PathVariable("itemname") String groupItemName, @PathVariable("memberItemName") String memberItemName);

    /**
     * Removes an existing member from a group item.
     */
    @RequestMapping(value = "/{itemname}/members/{memberItemName}", method = RequestMethod.DELETE)
    void removeFromGroup(@PathVariable("itemname") String groupItemName, @PathVariable("memberItemName") String memberItemName);

    /**
     * Adds a tag to an item.
     */
    @RequestMapping(value = "/{itemname}/tags/{tag}", method = RequestMethod.PUT)
    void addTag(@PathVariable("itemname") String itemname, @PathVariable("tag") String tag);

    /**
     * Removes a tag from an item.
     */
    @RequestMapping(value = "/{itemname}/tags/{tag}", method = RequestMethod.DELETE)
    void removeTag(@PathVariable("itemname") String itemname, @PathVariable("tag") String tag);
}
