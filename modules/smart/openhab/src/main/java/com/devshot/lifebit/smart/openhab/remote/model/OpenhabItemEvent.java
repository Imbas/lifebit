package com.devshot.lifebit.smart.openhab.remote.model;

import com.devshot.lifebit.smart.openhab.remote.model.converter.OpenhabEventDateDeserializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.ZonedDateTime;

/**
 * Входящее событие Openhab c MQTT-шины
 *
 * @author Oleg Ivashkevich
 * date: 22.07.2017
 */
@Data
@EqualsAndHashCode(of = "name")
public class OpenhabItemEvent {
    @JsonProperty("name")
    private String name;

    /**
     * Item state: a string representation of the item state. ON/OFF, OPEN/CLOSED and UP/DOWN states are transformed to 1/0 values, respectively.
     */
    @JsonProperty("state")
    private String state;

    @JsonProperty("date")
    @JsonDeserialize(using = OpenhabEventDateDeserializer.class)
    private ZonedDateTime date;
}
