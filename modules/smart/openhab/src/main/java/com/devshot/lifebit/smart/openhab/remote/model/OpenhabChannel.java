package com.devshot.lifebit.smart.openhab.remote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Канал для подключения информационно-исполнительных частей
 *
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@Data
@EqualsAndHashCode(of = "uid")
public class OpenhabChannel {
    @JsonProperty("linkedItems")
    private List<String> linkedItemNames;

    @JsonProperty
    private String uid;

    @JsonProperty
    private String id;

    @JsonProperty
    private String channelTypeUID;

    @JsonProperty
    private String itemType;

    @JsonProperty
    private String kind;

    @JsonProperty
    private String label;

    /**
     * Not null поле на удалённой стороне
     */
    @JsonProperty
    private List<String> defaultTags;
}