package com.devshot.lifebit.smart.openhab.message;

import com.devshot.commons.message.data.BlankResult;
import com.devshot.commons.message.data.CommandConfiguration;
import com.devshot.lifebit.smart.openhab.config.OpenhabConfig;
import com.devshot.lifebit.smart.openhab.model.OpenhabEvent;
import org.springframework.stereotype.Component;

/**
 * @author Oleg Ivashkevich
 * date: 09.05.2020
 */
@Component
public class OpenhabEventCommandConfiguration extends CommandConfiguration<OpenhabEvent, BlankResult> {

    public OpenhabEventCommandConfiguration(OpenhabConfig config) {
        super("openhab-event", config.getEventTopic(), true, OpenhabEvent.class, BlankResult.class);
    }
}