package com.devshot.lifebit.smart.openhab.remote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author Oleg Ivashkevich
 * date: 21.07.2017
 */
@Data
@EqualsAndHashCode(of = "name")
public class OpenhabItem {
    @Data
    public static class StateDescription {
        @JsonProperty
        private String pattern;

        @JsonProperty
        private boolean readOnly;

        @JsonProperty
        private List<String> options;

        @JsonProperty
        private Integer minimum;

        @JsonProperty
        private Integer maximum;

        @JsonProperty
        private Integer step;
    }

    /**
     * Идентификационный параметр, не изменяется после создания
     */
    @JsonProperty
    private String name;

    /**
     * Информационный параметр, изменить запросом save нельзя
     */
    @JsonProperty
    private String state;

    @JsonProperty
    private String type;

    @JsonProperty
    private String label;

    @JsonProperty
    private String category;

    @JsonProperty
    private List<String> tags;

    @JsonProperty
    private List<String> groupNames;

    @JsonProperty
    private List<OpenhabItem> members;

    @JsonProperty
    private String groupType;
}
