package com.devshot.lifebit.smart.openhab.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Message context for openhub.
 * WARNING - ACTIVEMQ required!
 *
 * @author Oleg Ivashkevich
 * date: 08.05.2020
 */
@Configuration
@ComponentScan("com.devshot.lifebit.smart.openhab.message")
public class OpenhabMessageContextConfiguration {
}
