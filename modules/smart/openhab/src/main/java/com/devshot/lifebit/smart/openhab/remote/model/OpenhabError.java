package com.devshot.lifebit.smart.openhab.remote.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * @author Oleg Ivashkevich
 * date: 19.07.2017
 */
@Data
public class OpenhabError {
    @JsonProperty
    private String message;

    @JsonProperty("http-code")
    private int httpCode;
}
