package com.devshot.lifebit.smart.openhab.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;

/**
 * @author Oleg Ivashkevich
 * date: 03.05.2020
 */
@Getter
@Component
public class OpenhabConfig {
    @Value("${openhab.url}")
    private String url;

    @Value("${openhab.event.topic}")
    private String eventTopic;

    @Value("${openhab.discovery.attempts:10}")
    public int discoveryAttempts;

    @Value("${openhab.discovery.attempt-interval:Pt1s}")
    public Duration discoveryAttemptInterval;

    @Value("${openhab.initialization.attempts:10}")
    public int initializationAttempts;

    @Value("${openhab.initialization.attempt-interval:Pt1s}")
    public Duration initializationAttemptInterval;
}
