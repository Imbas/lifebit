function insertEmptyOption(parameter) {
    !parameter.required && (parameter.options && parameter.options.length > 0 || parameter.context) && parameter.options.splice(0, 0, {
        label: "",
        value: null
    })
}
angular.module("PaperUI.services.rest", ["PaperUI.constants", "ngResource"]).config(function ($httpProvider) {
    var accessToken = function () {
        return $("#authentication").data("access-token")
    }();
    if ("{{ACCESS_TOKEN}}" != accessToken) {
        var authorizationHeader = function () {
            return "Bearer " + accessToken
        }();
        $httpProvider.defaults.headers.common.Authorization = authorizationHeader
    }
}).factory("itemService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/items", {}, {
        getAll: {
            method: "GET",
            isArray: !0,
            url: restConfig.restPath + "/items?recursive=false"
        },
        getByName: {method: "GET", params: {bindingId: "@itemName"}, url: restConfig.restPath + "/items/:itemName"},
        remove: {method: "DELETE", params: {itemName: "@itemName"}, url: restConfig.restPath + "/items/:itemName"},
        create: {
            method: "PUT",
            params: {itemName: "@itemName"},
            url: restConfig.restPath + "/items/:itemName",
            transformResponse: function (response, headerGetter, status) {
                var response = angular.fromJson(response);
                return 405 == status && (response.customMessage = "Item is not editable."), response
            }
        },
        updateState: {
            method: "PUT",
            params: {itemName: "@itemName"},
            url: restConfig.restPath + "/items/:itemName/state",
            headers: {"Content-Type": "text/plain"}
        },
        getItemState: {
            method: "GET",
            params: {itemName: "@itemName"},
            url: restConfig.restPath + "/items/:itemName/state",
            transformResponse: function (data) {
                return data
            },
            headers: {"Content-Type": "text/plain"}
        },
        sendCommand: {
            method: "POST",
            params: {itemName: "@itemName"},
            url: restConfig.restPath + "/items/:itemName",
            headers: {"Content-Type": "text/plain"}
        },
        addMember: {
            method: "PUT",
            params: {itemName: "@itemName", memberItemName: "@memberItemName"},
            url: restConfig.restPath + "/items/:itemName/members/:memberItemName"
        },
        removeMember: {
            method: "DELETE",
            params: {itemName: "@itemName", memberItemName: "@memberItemName"},
            url: restConfig.restPath + "/items/:itemName/members/:memberItemName"
        },
        addTag: {
            method: "PUT",
            params: {itemName: "@itemName", tag: "@tag"},
            url: restConfig.restPath + "/items/:itemName/tags/:tag"
        },
        removeTag: {
            method: "DELETE",
            params: {itemName: "@itemName", tag: "@tag"},
            url: restConfig.restPath + "/items/:itemName/tags/:tag"
        }
    })
}).factory("bindingService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/bindings", {}, {
        getAll: {method: "GET", isArray: !0},
        getConfigById: {
            method: "GET", params: {id: "@id"}, interceptor: {
                response: function (response) {
                    return response.data
                }
            }, url: restConfig.restPath + "/bindings/:id/config"
        },
        updateConfig: {
            method: "PUT",
            headers: {"Content-Type": "application/json"},
            params: {id: "@id"},
            url: restConfig.restPath + "/bindings/:id/config"
        }
    })
}).factory("inboxService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/inbox", {}, {
        getAll: {
            method: "GET",
            isArray: !0,
            transformResponse: function (data) {
                for (var results = angular.fromJson(data), i = 0; i < results.length; i++)results[i].bindingType = results[i].thingTypeUID.split(":")[0];
                return results
            }
        },
        approve: {
            method: "POST",
            params: {thingUID: "@thingUID"},
            url: restConfig.restPath + "/inbox/:thingUID/approve",
            headers: {"Content-Type": "text/plain"}
        },
        ignore: {method: "POST", params: {thingUID: "@thingUID"}, url: restConfig.restPath + "/inbox/:thingUID/ignore"},
        unignore: {
            method: "POST",
            params: {thingUID: "@thingUID"},
            url: restConfig.restPath + "/inbox/:thingUID/unignore"
        },
        remove: {method: "DELETE", params: {thingUID: "@thingUID"}, url: restConfig.restPath + "/inbox/:thingUID"}
    })
}).factory("discoveryService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/discovery", {}, {
        getAll: {method: "GET", isArray: !0},
        scan: {
            method: "POST", params: {bindingId: "@bindingId"}, transformResponse: function (data) {
                return {timeout: angular.fromJson(data)}
            }, url: restConfig.restPath + "/discovery/bindings/:bindingId/scan"
        }
    })
}).factory("thingTypeService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/thing-types", {}, {
        getAll: {method: "GET", isArray: !0},
        getByUid: {
            method: "GET",
            params: {thingTypeUID: "@thingTypeUID"},
            url: restConfig.restPath + "/thing-types/:thingTypeUID"
        },
        getFirmwares: {
            method: "GET",
            isArray: !0,
            params: {thingTypeUID: "@thingTypeUID"},
            url: restConfig.restPath + "/thing-types/:thingTypeUID/firmwares"
        }
    })
}).factory("linkService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/links", {}, {
        getAll: {method: "GET", isArray: !0},
        link: {
            method: "PUT",
            params: {itemName: "@itemName", channelUID: "@channelUID"},
            url: restConfig.restPath + "/links/:itemName/:channelUID"
        },
        unlink: {
            method: "DELETE",
            params: {itemName: "@itemName", channelUID: "@channelUID"},
            url: restConfig.restPath + "/links/:itemName/:channelUID"
        }
    })
}).factory("thingService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/things", {}, {
        getAll: {method: "GET", isArray: !0},
        getByUid: {method: "GET", params: {bindingId: "@thingUID"}, url: restConfig.restPath + "/things/:thingUID"},
        remove: {method: "DELETE", params: {thingUID: "@thingUID"}, url: restConfig.restPath + "/things/:thingUID"},
        add: {method: "POST", url: restConfig.restPath + "/things", headers: {"Content-Type": "application/json"}},
        update: {
            method: "PUT",
            params: {thingUID: "@thingUID"},
            url: restConfig.restPath + "/things/:thingUID",
            headers: {"Content-Type": "application/json"}
        },
        updateConfig: {
            method: "PUT",
            params: {thingUID: "@thingUID"},
            url: restConfig.restPath + "/things/:thingUID/config",
            headers: {"Content-Type": "application/json"}
        },
        link: {
            method: "POST",
            params: {thingUID: "@thingUID", channelId: "@channelId"},
            url: restConfig.restPath + "/things/:thingUID/channels/:channelId/link",
            headers: {"Content-Type": "text/plain"}
        },
        unlink: {
            method: "DELETE",
            params: {thingUID: "@thingUID", channelId: "@channelId"},
            url: restConfig.restPath + "/things/:thingUID/channels/:channelId/link"
        },
        getFirmwareStatus: {
            method: "GET",
            params: {thingUID: "@thingUID"},
            url: restConfig.restPath + "/things/:thingUID/firmware/status"
        },
        installFirmware: {
            method: "PUT",
            params: {thingUID: "@thingUID", firmwareVersion: "@firmwareVersion"},
            url: restConfig.restPath + "/things/:thingUID/firmware/:firmwareVersion"
        },
        cancel: {method: "GET", params: {thingUID: "@thingUID"}, url: restConfig.restPath + "/update/:thingUID/cancel"}
    })
}).factory("serviceConfigService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/services", {}, {
        getAll: {method: "GET", isArray: !0},
        getById: {method: "GET", params: {id: "@id"}, url: restConfig.restPath + "/services/:id"},
        getConfigById: {
            method: "GET", params: {id: "@id"}, interceptor: {
                response: function (response) {
                    return response.data
                }
            }, url: restConfig.restPath + "/services/:id/config"
        },
        updateConfig: {
            method: "PUT",
            headers: {"Content-Type": "application/json"},
            params: {id: "@id"},
            url: restConfig.restPath + "/services/:id/config"
        },
        deleteConfig: {method: "DELETE", params: {id: "@id"}, url: restConfig.restPath + "/services/:id/config"}
    })
}).factory("configDescriptionService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/config-descriptions", {}, {
        getAll: {method: "GET", isArray: !0},
        getByUri: {
            method: "GET", params: {uri: "@uri"}, transformResponse: function (response, headerGetter, status) {
                var response = angular.fromJson(response);
                return 404 == status && (response.showError = !1), response
            }, url: restConfig.restPath + "/config-descriptions/:uri"
        }
    })
}).factory("extensionService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/extensions", {}, {
        getAll: {
            method: "GET",
            isArray: !0,
            transformResponse: function (response, headerGetter, status) {
                return 503 == status ? {showError: !1} : angular.fromJson(response)
            }
        },
        getByUri: {method: "GET", params: {uri: "@id"}, url: restConfig.restPath + "/extensions/:id"},
        getAllTypes: {method: "GET", isArray: !0, url: restConfig.restPath + "/extensions/types"},
        install: {method: "POST", params: {id: "@id"}, url: restConfig.restPath + "/extensions/:id/install"},
        installFromURL: {
            method: "POST",
            params: {url: "@url"},
            url: restConfig.restPath + "/extensions/url/:url/install"
        },
        uninstall: {method: "POST", params: {id: "@id"}, url: restConfig.restPath + "/extensions/:id/uninstall"}
    })
}).factory("ruleService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/rules", {}, {
        getAll: {method: "GET", isArray: !0},
        getByUid: {method: "GET", params: {ruleUID: "@ruleUID"}, url: restConfig.restPath + "/rules/:ruleUID"},
        add: {method: "POST", headers: {"Content-Type": "application/json"}},
        remove: {method: "DELETE", params: {ruleUID: "@ruleUID"}, url: restConfig.restPath + "/rules/:ruleUID"},
        getModuleConfigParameter: {
            method: "GET",
            params: {ruleUID: "@ruleUID"},
            transformResponse: function (data, headersGetter, status) {
                return {content: data}
            },
            url: restConfig.restPath + "/rules/:ruleUID/actions/action/config/script"
        },
        setModuleConfigParameter: {
            method: "PUT",
            params: {ruleUID: "@ruleUID"},
            url: restConfig.restPath + "/rules/:ruleUID/actions/action/config/script",
            headers: {"Content-Type": "text/plain"}
        },
        update: {
            method: "PUT",
            params: {ruleUID: "@ruleUID"},
            url: restConfig.restPath + "/rules/:ruleUID",
            headers: {"Content-Type": "application/json"}
        },
        setEnabled: {
            method: "POST",
            params: {ruleUID: "@ruleUID"},
            url: restConfig.restPath + "/rules/:ruleUID/enable",
            headers: {"Content-Type": "text/plain"}
        },
        runRule: {
            method: "POST",
            params: {ruleUID: "@ruleUID"},
            url: restConfig.restPath + "/rules/:ruleUID/runnow",
            headers: {"Content-Type": "text/plain"}
        }
    })
}).factory("moduleTypeService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/module-types", {}, {
        getAll: {method: "GET", isArray: !0},
        getByType: {
            method: "GET",
            params: {mtype: "@mtype"},
            url: restConfig.restPath + "/module-types?type=:mtype",
            isArray: !0
        },
        getByUid: {method: "GET", params: {ruleUID: "@ruleUID"}, url: restConfig.restPath + "/rules/:ruleUID"},
        getModuleConfigByUid: {
            method: "GET",
            params: {ruleUID: "@ruleUID", moduleCategory: "@moduleCategory", id: "@id"},
            url: restConfig.restPath + "/rules/:ruleUID/:moduleCategory/:id/config"
        },
        add: {method: "POST", headers: {"Content-Type": "application/json"}},
        remove: {method: "DELETE", params: {ruleUID: "@ruleUID"}, url: restConfig.restPath + "/rules/:ruleUID"},
        getModuleConfigParameter: {
            method: "GET",
            params: {ruleUID: "@ruleUID"},
            transformResponse: function (data, headersGetter, status) {
                return {content: data}
            },
            url: restConfig.restPath + "/rules/:ruleUID/actions/action/config/script"
        },
        setModuleConfigParameter: {
            method: "PUT",
            params: {ruleUID: "@ruleUID"},
            url: restConfig.restPath + "/rules/:ruleUID/actions/action/config/script",
            headers: {"Content-Type": "text/plain"}
        }
    })
}).factory("channelTypeService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/channel-types", {}, {
        getAll: {method: "GET", isArray: !0},
        getByUri: {
            method: "GET",
            params: {channelTypeUID: "@channelTypeUID"},
            url: restConfig.restPath + "/channel-types/:channelTypeUID"
        }
    })
}).factory("templateService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/channel-types", {}, {
        getAll: {
            method: "GET",
            url: restConfig.restPath + "/templates",
            isArray: !0
        },
        getByUid: {
            method: "GET",
            params: {templateUID: "@templateUID"},
            url: restConfig.restPath + "/templates/:templateUID"
        }
    })
}).factory("imageService", function (restConfig, $http) {
    return {
        getItemState: function (itemName) {
            var promise = $http.get(restConfig.restPath + "/items/" + itemName + "/state").then(function (response) {
                return response.data
            });
            return promise
        }
    }
}).factory("firmwareService", function ($resource, restConfig) {
    return $resource(restConfig.restPath + "/firmware", {}, {
        getStatus: {method: "GET", isArray: !0},
        getByUid: {method: "GET", params: {thingUID: "@thingUID"}, url: restConfig.restPath + "/:thingUID"},
        update: {method: "GET", params: {thingUID: "@thingUID"}, url: restConfig.restPath + "/update/:thingUID"},
        cancel: {method: "GET", params: {thingUID: "@thingUID"}, url: restConfig.restPath + "/update/:thingUID/cancel"}
    })
});
var Repository = function ($q, $rootScope, remoteService, dataType, staticData) {
    var self = this, cacheEnabled = !0;
    this.setDirty = function () {
        this.dirty = !0
    }, this.getAll = function (callback, refresh) {
        "boolean" == typeof callback && (refresh = !0, callback = null);
        var deferred = $q.defer();
        return deferred.promise.then(function (res) {
            return callback && "No update" !== res ? callback(res) : void 0
        }, function (res) {
        }, function (res) {
            return callback ? callback(res) : void 0
        }), cacheEnabled && staticData && self.initialFetch && !refresh && !self.dirty ? deferred.resolve($rootScope.data[dataType]) : (remoteService.getAll(function (data) {
            !cacheEnabled || data.length != $rootScope.data[dataType].length || self.dirty || refresh ? (self.initialFetch = !0, $rootScope.data[dataType] = data, self.dirty = !1, deferred.resolve(data)) : (self.initialFetch || (self.initialFetch = !0, $rootScope.data[dataType] = data, self.dirty = !1), deferred.resolve("No update"))
        }), cacheEnabled && self.initialFetch && deferred.notify($rootScope.data[dataType])), deferred.promise
    }, this.getOne = function (condition, callback, refresh) {
        var element = self.find(condition);
        null == element || this.dirty || refresh ? self.getAll(null, !0).then(function (res) {
            return callback ? void callback(self.find(condition)) : void 0
        }, function (res) {
            callback(null)
        }, function (res) {
        }) : callback(element)
    }, this.find = function (condition) {
        for (var i = 0; i < $rootScope.data[dataType].length; i++) {
            var element = $rootScope.data[dataType][i];
            if (condition(element))return element
        }
        return null
    }, this.findByIndex = function (condition) {
        for (var i = 0; i < $rootScope.data[dataType].length; i++) {
            var element = $rootScope.data[dataType][i];
            if (condition(element))return i
        }
        return -1
    }, this.add = function (element) {
        $rootScope.data[dataType].push(element)
    }, this.remove = function (element, index) {
        "undefined" == typeof index && $rootScope.data[dataType].indexOf(element) !== -1 ? $rootScope.data[dataType].splice($rootScope.data[dataType].indexOf(element), 1) : "undefined" != typeof index && index !== -1 && $rootScope.data[dataType].splice(index, 1)
    }, this.update = function (element) {
        var index = $rootScope.data[dataType].indexOf(element);
        $rootScope.data[dataType][index] = element
    }
};
angular.module("PaperUI.services.repositories", []).factory("bindingRepository", function ($q, $rootScope, bindingService) {
    return $rootScope.data.bindings = [], new Repository($q, $rootScope, bindingService, "bindings", (!0))
}).factory("thingTypeRepository", function ($q, $rootScope, thingTypeService) {
    return $rootScope.data.thingTypes = [], new Repository($q, $rootScope, thingTypeService, "thingTypes", (!0))
}).factory("discoveryResultRepository", function ($q, $rootScope, inboxService, eventService) {
    var repository = new Repository($q, $rootScope, inboxService, "discoveryResults");
    return $rootScope.data.discoveryResults = [], eventService.onEvent("smarthome/inbox/*", function (topic, discoveryResult) {
        var index = repository.findByIndex(function (result) {
            return discoveryResult.thingUID == result.thingUID
        });
        topic.indexOf("added") > -1 && index == -1 && repository.add(discoveryResult), topic.indexOf("removed") > -1 && index != -1 && repository.remove(discoveryResult, index)
    }), repository
}).factory("thingRepository", function ($q, $rootScope, thingService, eventService) {
    var repository = new Repository($q, $rootScope, thingService, "things");
    $rootScope.data.things = [];
    var itemNameToThingUID = function (itemName) {
        return itemName.replace(/_/g, ":")
    }, updateInRepository = function (thingUID, mustExist, action) {
        var existing = repository.find(function (thing) {
            return thing.UID === thingUID
        });
        (existing && mustExist || !existing && !mustExist) && $rootScope.$apply(function (scope) {
            action(existing)
        })
    };
    return eventService.onEvent("smarthome/things/*/status", function (topic, statusInfo) {
        updateInRepository(topic.split("/")[2], !0, function (existingThing) {
            existingThing.statusInfo = statusInfo
        })
    }), eventService.onEvent("smarthome/things/*/added", function (topic, thing) {
        updateInRepository(topic.split("/")[2], !1, function (existingThing) {
            repository.add(thing)
        })
    }), eventService.onEvent("smarthome/things/*/updated", function (topic, thing) {
        updateInRepository(topic.split("/")[2], !0, function (existingThing) {
            if (thing.length > 0) {
                existingThing.label = thing[0].label, existingThing.configuration = existingThing.configuration;
                var updatedArr = [];
                thing[0].channels && (angular.forEach(thing[0].channels, function (newChannel) {
                    var channel = $.grep(existingThing.channels, function (existingChannel) {
                        return existingChannel.uid == newChannel.uid
                    });
                    0 == channel.length ? (channel[0] = newChannel, channel[0].linkedItems = []) : (channel[0].configuration = newChannel.configuration, channel[0].itemType = newChannel.itemType), updatedArr.push(channel[0])
                }), existingThing.channels = updatedArr)
            }
        })
    }), eventService.onEvent("smarthome/things/*/removed", function (topic, thing) {
        updateInRepository(topic.split("/")[2], !0, function (existingThing) {
            repository.remove(existingThing)
        })
    }), eventService.onEvent("smarthome/items/*/added", function (topic, item) {
        updateInRepository(itemNameToThingUID(topic.split("/")[2]), !0, function (existingThing) {
            existingThing.item = item
        })
    }), eventService.onEvent("smarthome/links/*/added", function (topic, link) {
        var thingUID, channelItem = link.channelUID.split(":");
        channelItem.length > 2 && (thingUID = channelItem[0] + ":" + channelItem[1] + ":" + channelItem[2]), thingUID && updateInRepository(thingUID, !0, function (existingThing) {
            var channel = $.grep(existingThing.channels, function (channel) {
                return channel.uid == link.channelUID
            });
            channel.length > 0 && (channel[0].linkedItems = channel[0].linkedItems ? channel[0].linkedItems : [], channel[0].linkedItems.push(link.itemName))
        })
    }), eventService.onEvent("smarthome/links/*/removed", function (topic, link) {
        var thingUID, channelItem = link.channelUID.split(":");
        channelItem.length > 2 && (thingUID = channelItem[0] + ":" + channelItem[1] + ":" + channelItem[2]), thingUID && updateInRepository(thingUID, !0, function (existingThing) {
            var channel = $.grep(existingThing.channels, function (channel) {
                return channel.uid == link.channelUID
            });
            channel.length > 0 && (channel[0].linkedItems = [])
        })
    }), repository
}).factory("itemRepository", function ($q, $rootScope, itemService, eventService) {
    var repository = new Repository($q, $rootScope, itemService, "items");
    return $rootScope.data.items = [], eventService.onEvent("smarthome/items/*/updated", function (topic, itemUpdate) {
        if (topic.split("/").length > 2) {
            var index = repository.findByIndex(function (item) {
                return item.name == topic.split("/")[2]
            });
            index !== -1 && $rootScope.$apply(function () {
                $rootScope.data.items[index] = itemUpdate[0]
            })
        }
    }), eventService.onEvent("smarthome/items/*/added", function (topic, itemAdded) {
        if (topic.split("/").length > 2) {
            var index = repository.findByIndex(function (item) {
                return item.name == itemAdded.name
            });
            index === -1 && $rootScope.data.items && $rootScope.$apply(function () {
                $rootScope.data.items.push(itemAdded)
            })
        }
    }), eventService.onEvent("smarthome/items/*/removed", function (topic, itemRemoved) {
        if (topic.split("/").length > 2) {
            var index = repository.findByIndex(function (item) {
                return item.name == itemRemoved.name
            });
            index !== -1 && $rootScope.$apply(function () {
                $rootScope.data.items.splice(index, 1)
            })
        }
    }), repository
}).factory("ruleRepository", function ($q, $rootScope, ruleService, eventService) {
    var repository = new Repository($q, $rootScope, ruleService, "rules", (!0));
    return $rootScope.data.rules = [], eventService.onEvent("smarthome/rules/*/updated", function (topic, ruleUpdate) {
        var existing = repository.find(function (rule) {
            return rule.uid === ruleUpdate[0].uid
        });
        $rootScope.$apply(function () {
            existing && (existing.name = ruleUpdate[0].name, existing.description = ruleUpdate[0].description, existing.triggers = ruleUpdate[0].triggers, existing.actions = ruleUpdate[0].actions, existing.conditions = ruleUpdate[0].conditions)
        })
    }), eventService.onEvent("smarthome/rules/*/added", function (topic, rule) {
        $rootScope.$apply(function () {
            repository.add(rule)
        })
    }), eventService.onEvent("smarthome/rules/*/removed", function (topic, removedRule) {
        var existing = repository.find(function (rule) {
            return rule.uid === removedRule.uid
        });
        $rootScope.$apply(function () {
            repository.remove(existing)
        })
    }), eventService.onEvent("smarthome/rules/*/state", function (topic, rule) {
        var existing = repository.find(function (rule) {
            return rule.uid === topic.split("/")[2]
        });
        $rootScope.$apply(function () {
            existing.status = {}, existing.status.status = rule.status, existing.status.statusDetail = rule.statusDetail, "DISABLED" === rule.status.toUpperCase() ? existing.enabled = !1 : existing.enabled = !0
        })
    }), repository
}).factory("templateRepository", function ($q, $rootScope, templateService) {
    var repository = new Repository($q, $rootScope, templateService, "templates");
    return $rootScope.data.templates = [], repository
}), angular.module("PaperUI.services", ["PaperUI.services.repositories", "PaperUI.constants"]).config(function ($httpProvider) {
    var language = localStorage.getItem("paperui.language");
    language && ($httpProvider.defaults.headers.common["Accept-Language"] = language), $httpProvider.interceptors.push(function ($q, $injector) {
        return {
            responseError: function (rejection) {
                var showError = rejection.data.showError;
                if (showError !== !1) {
                    var errorText = "";
                    errorText = rejection.data && rejection.data.customMessage ? rejection.data.customMessage : rejection.statusText, $injector.get("toastService").showErrorToast("ERROR: " + rejection.status + " - " + errorText)
                }
                return $q.reject(rejection)
            }
        }
    })
}).factory("eventService", function ($resource, $log, restConfig) {
    var eventSrc, callbacks = [], initializeEventService = function () {
        eventSrc = new EventSource(restConfig.eventPath), $log.debug("Initializing event service."), eventSrc.addEventListener("error", function (event) {
            2 === eventSrc.readyState && ($log.debug("Event connection broken. Trying to reconnect in 5 seconds."), setTimeout(initializeEventService, 5e3))
        }), eventSrc.addEventListener("message", function (event) {
            var data = JSON.parse(event.data);
            $log.debug("Event received: " + data.topic + " - " + data.payload), $.each(callbacks, function (index, element) {
                var match = data.topic.match(element.topic);
                null != match && match == data.topic && element.callback(data.topic, JSON.parse(data.payload))
            })
        })
    };
    return "undefined" != typeof EventSource && initializeEventService(), new function () {
        this.onEvent = function (topic, callback) {
            var topicRegex = topic.replace("/", "/").replace("*", ".*");
            callbacks.push({topic: topicRegex, callback: callback})
        }
    }
}).factory("toastService", function ($mdToast, $rootScope) {
    return new function () {
        var self = this;
        this.showToast = function (id, text, actionText, actionUrl) {
            var toast = $mdToast.simple().content(text);
            actionText ? (toast.action(actionText), toast.hideDelay(6e3)) : toast.hideDelay(3e3), toast.position("bottom right"), $mdToast.show(toast).then(function (value) {
                "ok" == value && $rootScope.$location.path(actionUrl)
            })
        }, this.showDefaultToast = function (text, actionText, actionUrl) {
            self.showToast("default", text, actionText, actionUrl)
        }, this.showErrorToast = function (text, actionText, actionUrl) {
            self.showToast("error", text, actionText, actionUrl)
        }, this.showSuccessToast = function (text, actionText, actionUrl) {
            self.showToast("success", text, actionText, actionUrl)
        }
    }
}).factory("configService", function (itemService, thingRepository, ruleRepository, $filter, itemRepository) {
    var applyParameterContext = function (parameter) {
        if (!parameter.context)return !1;
        var context = parameter.context.toUpperCase();
        switch (context) {
            case"ITEM":
            case"CHANNEL":
            case"THING":
            case"RULE":
                parameter.multiple ? (parameter.element = "multiSelect", parameter.limitToOptions = !0) : parameter.element = "select";
                break;
            case"DATE":
                "TEXT" === parameter.type.toUpperCase() ? parameter.element = "date" : (parameter.element = "input", parameter.context = "");
                break;
            case"TIME":
                parameter.element = "input", "TEXT" === parameter.type.toUpperCase() ? parameter.inputType = "time" : parameter.context = "";
                break;
            case"COLOR":
                parameter.element = "color", parameter.input = "TEXT", parameter.inputType = "color";
                break;
            case"SCRIPT":
                parameter.element = "textarea", parameter.inputType = "text", parameter.label = parameter.label && parameter.label.length > 0 ? parameter.label : "Script";
                break;
            case"DAYOFWEEK":
                parameter.element = "dayofweek", parameter.inputType = "text";
                break;
            case"PASSWORD":
                parameter.element = "input", parameter.inputType = "password";
                break;
            default:
                return !1
        }
        return "RULE" === context && (parameter.options = parameter.options ? parameter.options : [], ruleRepository.getAll(function (rules) {
            angular.forEach(rules, function (rule) {
                rule.value = rule.uid, rule.label = rule.name, parameter.options.push(rule)
            })
        })), !0
    }, applyParameterType = function (parameter) {
        var type = parameter.type ? parameter.type.toUpperCase() : "TEXT";
        switch (type) {
            case"TEXT":
            case"INTEGER":
            case"DECIMAL":
                parameter.inputType = "TEXT" === type ? "text" : "number", parameter.options = parameter.options && parameter.options.length > 0 ? parameter.options : [], parameter.multiple ? parameter.element = "multiSelect" : parameter.options.length > 0 ? parameter.limitToOptions ? parameter.element = "select" : parameter.element = "multiSelect" : parameter.element = "input";
                break;
            case"BOOLEAN":
                parameter.element = "switch";
                break;
            default:
                parameter.element = "input", parameter.inputType = "text"
        }
        "TEXT" === type && insertEmptyOption(parameter), "INTEGER" !== type && "DECIMAL" !== type || (angular.forEach(parameter.options, function (option) {
            option.value = parseInt(option.value)
        }), parameter.defaultValue && (parameter.defaultValue = parseInt(parameter.defaultValue)))
    };
    return {
        getRenderingModel: function (configParameters, configGroups) {
            if (!configParameters || 0 == configParameters.length)return [];
            configGroups || (configGroups = []), configGroups.push({name: "_default", label: "Others"});
            var groupNameIndexMap = {};
            angular.forEach(configGroups, function (configGroup, index) {
                groupNameIndexMap[configGroup.name] = index
            });
            var groupsList = [];
            angular.forEach(configParameters, function (parameter) {
                parameter.locale = window.localStorage.getItem("paperui.language"), parameter.filterText = "";
                var contextApplied = applyParameterContext(parameter);
                contextApplied || applyParameterType(parameter);
                var group = $filter("filter")(configGroups, function (configGroup) {
                    var groupName = groupNameIndexMap[parameter.groupName] >= 0 ? parameter.groupName : "_default";
                    return configGroup.name === groupName
                }), groupIndex = groupNameIndexMap[group[0].name];
                groupsList[groupIndex] || (groupsList[groupIndex] = {parameters: []}), groupsList[groupIndex].groupName = group[0].name, groupsList[groupIndex].groupLabel = group[0].label, groupsList[groupIndex].advanced = group[0].advanced, groupsList[groupIndex].parameters.push(parameter)
            });
            var renderingGroups = [];
            return renderingGroups.hasAdvanced = !1, angular.forEach(groupsList, function (group) {
                group.advanced && angular.forEach(group.parameters, function (parameter) {
                    parameter.advanced = !0
                }), group.advParam = $filter("filter")(group.parameters, function (parameter) {
                    return parameter.advanced
                }).length, group.advParam > 0 && (renderingGroups.hasAdvanced = !0), renderingGroups.push(group)
            }), renderingGroups = this.getItemConfigs(renderingGroups), this.getChannelsConfig(renderingGroups)
        }, getChannelsConfig: function (configParams) {
            function getChannelsFromThings(arr, filter) {
                for (var channels = [], i = 0; i < arr.length; i++) {
                    for (var filteredChannels = self.filterByAttributes(arr[i].channels, filter), j = 0; j < filteredChannels.length; j++)filteredChannels[j].label = arr[i].label, filteredChannels[j].value = filteredChannels[j].uid;
                    channels = channels.concat(filteredChannels)
                }
                return channels
            }

            for (var hasOneItem, self = this, configParameters = configParams, i = 0; !hasOneItem && i < configParameters.length; i++) {
                var parameterItems = $.grep(configParameters[i].parameters, function (value) {
                    return value.context && ("THING" == value.context.toUpperCase() || "CHANNEL" == value.context.toUpperCase())
                });
                parameterItems.length > 0 && (hasOneItem = !0), hasOneItem && thingRepository.getAll(function (things) {
                    for (var g_i = 0; g_i < configParameters.length; g_i++)for (var i = 0; i < configParameters[g_i].parameters.length; i++)configParameters[g_i].parameters[i].context && ("THING" === configParameters[g_i].parameters[i].context.toUpperCase() ? configParameters[g_i].parameters[i].options = self.filterByAttributes(things, configParameters[g_i].parameters[i].filterCriteria) : "CHANNEL" === configParameters[g_i].parameters[i].context.toUpperCase() && (configParameters[g_i].parameters[i].options = getChannelsFromThings(things, configParameters[g_i].parameters[i].filterCriteria)))
                })
            }
            return configParameters
        }, getItemConfigs: function (configParams) {
            for (var self = this, hasOneItem = !1, configParameters = configParams, i = 0; !hasOneItem && i < configParameters.length; i++) {
                var parameterItems = $.grep(configParameters[i].parameters, function (value) {
                    return value.context && "ITEM" == value.context.toUpperCase()
                });
                parameterItems.length > 0 && (hasOneItem = !0)
            }
            return hasOneItem && itemRepository.getAll(function (items) {
                for (var g_i = 0; g_i < configParameters.length; g_i++)for (var i = 0; i < configParameters[g_i].parameters.length; i++)if (configParameters[g_i].parameters[i].context && "ITEM" === configParameters[g_i].parameters[i].context.toUpperCase()) {
                    var filteredItems = self.filterByAttributes(items, configParameters[g_i].parameters[i].filterCriteria);
                    configParameters[g_i].parameters[i].options = $filter("orderBy")(filteredItems, "label")
                }
            }), configParameters
        }, filterByAttributes: function (arr, filters) {
            return filters && 0 != filters.length ? $.grep(arr, function (element, i) {
                return $.grep(filters, function (filter) {
                        if (arr[i].hasOwnProperty(filter.name) && "" != filter.value && null != filter.value) {
                            var filterValues = filter.value.split(",");
                            return $.grep(filterValues, function (filterValue) {
                                    return Array.isArray(arr[i][filter.name]) ? $.grep(arr[i][filter.name], function (arrValue) {
                                        return arrValue.toUpperCase().indexOf(filterValue.toUpperCase()) != -1
                                    }).length > 0 : arr[i][filter.name].toUpperCase().indexOf(filterValue.toUpperCase()) != -1
                                }).length > 0
                        }
                        return !1
                    }).length == filters.length
            }) : arr
        }, getConfigAsArray: function (config, paramGroups) {
            var configArray = [], self = this;
            return angular.forEach(config, function (value, name) {
                var value = config[name];
                if (paramGroups) {
                    var param = self.getParameter(paramGroups, name), date = Date.parse(value);
                    null !== param && param.context && !isNaN(date) && ("TIME" === param.context.toUpperCase() ? value = (value.getHours() < 10 ? "0" : "") + value.getHours() + ":" + (value.getMinutes() < 10 ? "0" : "") + value.getMinutes() : "DATE" === param.context.toUpperCase() && (value = value.getFullYear() + "-" + (value.getMonth() + 1 < 10 ? "0" : "") + (value.getMonth() + 1) + "-" + value.getDate()))
                }
                configArray.push({name: name, value: value})
            }), configArray
        }, getConfigAsObject: function (configArray, paramGroups, sending) {
            for (var config = {}, i = 0; configArray && i < configArray.length; i++) {
                var configEntry = configArray[i], param = this.getParameter(paramGroups, configEntry.name);
                if (null !== param && "BOOLEAN" == param.type.toUpperCase())configEntry.value = "TRUE" == String(configEntry.value).toUpperCase(); else if (null !== param && param.context)if ("TIME" === param.context.toUpperCase()) {
                    var time = configEntry.value ? configEntry.value.split(/[\s\/,.:-]+/) : [];
                    time.length > 1 && (configEntry.value = new Date(1970, 0, 1, time[0], time[1]))
                } else if ("DATE" === param.context.toUpperCase()) {
                    var dateParts = configEntry.value ? configEntry.value.split(/[\s\/,.:-]+/) : [];
                    dateParts.length > 2 ? configEntry.value = new Date(dateParts[1] + "/" + dateParts[2] + "/" + dateParts[0]) : configEntry.value = null
                }
                config[configEntry.name] = configEntry.value
            }
            return config
        }, getParameter: function (paramGroups, itemName) {
            for (var i = 0; i < paramGroups.length; i++)for (var j = 0; paramGroups[i].parameters && j < paramGroups[i].parameters.length; j++)if (paramGroups[i].parameters[j].name == itemName)return paramGroups[i].parameters[j];
            return null
        }, setDefaults: function (thing, thingType) {
            thingType && thingType.configParameters && $.each(thingType.configParameters, function (i, parameter) {
                if ("null" !== parameter.defaultValue)if ("TEXT" === parameter.type)thing.configuration[parameter.name] = parameter.defaultValue; else if ("BOOLEAN" === parameter.type) {
                    var value = null != thing.configuration[parameter.name] && "" != thing.configuration[parameter.name] ? thing.configuration[parameter.name] : null != parameter.defaultValue ? parameter.defaultValue : "";
                    String(value).length > 0 && (thing.configuration[parameter.name] = "TRUE" == String(value).toUpperCase())
                } else"INTEGER" === parameter.type || "DECIMAL" === parameter.type ? thing.configuration[parameter.name] = null != parameter.defaultValue && "" !== parameter.defaultValue ? parseInt(parameter.defaultValue) : "" : thing.configuration[parameter.name] = parameter.defaultValue; else thing.configuration[parameter.name] = null
            })
        }, setConfigDefaults: function (originalConfiguration, groups, sending) {
            var configuration = {};
            angular.copy(originalConfiguration, configuration);
            for (var i = 0; i < groups.length; i++)$.each(groups[i].parameters, function (i, parameter) {
                var hasValue = null != configuration[parameter.name] && String(configuration[parameter.name]).length > 0;
                if (!parameter.context || "DATE" !== parameter.context.toUpperCase() && "TIME" !== parameter.context.toUpperCase())if (hasValue || !parameter.context || "COLOR" !== parameter.context.toUpperCase() || sending)if (hasValue || "TEXT" !== parameter.type)if ("BOOLEAN" === parameter.type) {
                    var value = hasValue ? configuration[parameter.name] : parameter.defaultValue;
                    String(value).length > 0 && (configuration[parameter.name] = "TRUE" == String(value).toUpperCase())
                } else hasValue || "INTEGER" !== parameter.type && "DECIMAL" !== parameter.type ? hasValue || (configuration[parameter.name] = parameter.defaultValue) : configuration[parameter.name] = null != parameter.defaultValue && "" !== parameter.defaultValue ? parseInt(parameter.defaultValue) : null; else configuration[parameter.name] = parameter.defaultValue; else; else {
                    var date = hasValue ? configuration[parameter.name] : parameter.defaultValue ? parameter.defaultValue : null;
                    if (date)if ("undefined" != typeof sending && sending)"DATE" === parameter.context.toUpperCase() ? configuration[parameter.name] = date instanceof Date ? date.getFullYear() + "-" + (date.getMonth() + 1 < 10 ? "0" : "") + (date.getMonth() + 1) + "-" + date.getDate() : date : configuration[parameter.name] = date instanceof Date ? (date.getHours() < 10 ? "0" : "") + date.getHours() + ":" + (date.getMinutes() < 10 ? "0" : "") + date.getMinutes() : date; else if ("TIME" === parameter.context.toUpperCase()) {
                        var time = date.split(/[\s\/,.:-]+/);
                        time.length > 1 && (configuration[parameter.name] = new Date(1970, 0, 1, time[0], time[1]))
                    } else {
                        var dateParts = date.split(/[\s\/,.:-]+/);
                        dateParts.length > 2 ? configuration[parameter.name] = new Date(dateParts[1] + "/" + dateParts[2] + "/" + dateParts[0]) : configuration[parameter.name] = null
                    }
                }
                !parameter.limitToOptions && parameter.filterText && parameter.filterText.length > 0 && (Array.isArray(configuration[parameter.name]) ? configuration[parameter.name].push(parameter.filterText) : configuration[parameter.name] = parameter.filterText)
            });
            return this.replaceEmptyValues(configuration)
        }, convertValues: function (configurations, parameters) {
            return angular.forEach(configurations, function (value, name) {
                if (value && "boolean" != typeof value) {
                    var parsedValue = Number(value);
                    isNaN(parsedValue) ? configurations[name] = value : configurations[name] = parsedValue
                }
            }), configurations
        }, replaceEmptyValues: function (configurations) {
            return angular.forEach(configurations, function (value, name) {
                void 0 !== configurations[name] && null != configurations[name] && "" !== configurations[name] || (configurations[name] = null)
            }), configurations
        }
    }
}).factory("thingConfigService", function () {
    return {
        getThingChannels: function (thing, thingType, channelTypes, advanced) {
            var thingChannels = [], includedChannels = [];
            if (thingType && thingType.channelGroups && thingType.channelGroups.length > 0) {
                for (var i = 0; i < thingType.channelGroups.length; i++) {
                    var group = {};
                    group.name = thingType.channelGroups[i].label, group.description = thingType.channelGroups[i].description, group.channels = this.matchGroup(thing.channels, thingType.channelGroups[i].id), includedChannels = includedChannels.concat(group.channels), group.channels = advanced ? group.channels : this.filterAdvance(thingType, channelTypes, group.channels, !1), thingChannels.push(group)
                }
                for (var group = {
                    name: "Others",
                    description: "Other channels",
                    channels: []
                }, i = 0; i < thing.channels.length; i++)includedChannels.indexOf(thing.channels[i]) == -1 && group.channels.push(thing.channels[i]);
                group.channels && group.channels.length > 0 && thingChannels.push(group)
            } else {
                var group = {};
                group.channels = advanced ? thing.channels : this.filterAdvance(thingType, channelTypes, thing.channels, advanced), thingChannels.push(group)
            }
            return thingChannels = this.addTypeToChannels(thingChannels, channelTypes)
        }, filterAdvance: function (thingType, channelTypes, channels, advanced) {
            var self = this;
            return self.thingType = thingType, self.channelTypes = channelTypes, self.channels = channels, $.grep(channels, function (channel, i) {
                var channelType = self.getChannelTypeByUID(self.thingType, self.channelTypes, channel.channelTypeUID);
                return !channelType || advanced == channelType.advanced
            })
        }, getChannelTypeByUID: function (thingType, channelTypes, channelUID) {
            if (thingType) {
                if (thingType.channels && thingType.channels.length > 0) {
                    var c, c_i, c_l;
                    for (c_i = 0, c_l = thingType.channels.length; c_i < c_l; ++c_i)if (c = thingType.channels[c_i], c.typeUID == channelUID)return c
                }
                if (thingType.channelGroups && thingType.channelGroups.length > 0) {
                    var c, c_i, c_l, cg, cg_i, cg_l;
                    for (cg_i = 0, cg_l = thingType.channelGroups.length; cg_i < cg_l; ++cg_i)if (cg = thingType.channelGroups[cg_i], cg && cg.channels)for (c_i = 0, c_l = cg.channels.length; c_i < c_l; ++c_i)if (c = cg.channels[c_i], c.typeUID == channelUID)return c
                }
            }
            if (channelTypes) {
                var c_i, c_l, c = {};
                for (c_i = 0, c_l = channelTypes.length; c_i < c_l; ++c_i)if (c = channelTypes[c_i], c.UID == channelUID)return c
            }
        }, getChannelFromChannelTypes: function (channelTypes, channelUID) {
            if (channelTypes) {
                var c_i, c_l, c = {};
                for (c_i = 0, c_l = channelTypes.length; c_i < c_l; ++c_i)if (c = channelTypes[c_i], c.UID == channelUID)return c
            }
        }, matchGroup: function (arr, id) {
            for (var matched = [], i = 0; i < arr.length; i++)if (arr[i].id) {
                var sub = arr[i].id.split("#");
                sub[0] && sub[0] == id && matched.push(arr[i])
            }
            return matched
        }, addTypeToChannels: function (groups, channelTypes) {
            for (var g_i = 0; g_i < groups.length; g_i++)for (var c_i = 0; c_i < groups[g_i].channels.length; c_i++)groups[g_i].channels[c_i].channelType = this.getChannelFromChannelTypes(channelTypes, groups[g_i].channels[c_i].channelTypeUID);
            return groups
        }
    }
}).provider("dateTime", function () {
    var months, daysOfWeek, shortChars;
    return "de" == window.localStorage.getItem("paperui.language") ? (months = ["Januar", "Februar", "MÃ¤rz", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"], daysOfWeek = ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"], shortChars = 2) : (months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], daysOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], shortChars = 3), {
        getMonths: function (shortNames) {
            if (shortNames) {
                for (var shortMonths = [], i = 0; i < months.length; i++)shortMonths.push(months[i].substr(0, 3));
                return shortMonths
            }
            return months
        }, $get: function () {
            return {
                getMonths: function (shortNames) {
                    if (shortNames) {
                        for (var shortMonths = [], i = 0; i < months.length; i++)shortMonths.push(months[i].substr(0, 3));
                        return shortMonths
                    }
                    return months
                }, getDaysOfWeek: function (shortNames) {
                    if (shortNames) {
                        for (var shortDaysOfWeek = [], i = 0; i < daysOfWeek.length; i++)shortDaysOfWeek.push(daysOfWeek[i].substr(0, shortChars));
                        return shortDaysOfWeek
                    }
                    return daysOfWeek
                }
            }
        }
    }
});