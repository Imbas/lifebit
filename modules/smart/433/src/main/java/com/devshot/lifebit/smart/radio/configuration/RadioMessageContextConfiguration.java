package com.devshot.lifebit.smart.radio.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Oleg Ivashkevich
 * date: 08.05.2020
 */
@Configuration
@ComponentScan("com.devshot.lifebit.smart.radio.config")
@ComponentScan("com.devshot.lifebit.smart.radio.message")
public class RadioMessageContextConfiguration {
}
