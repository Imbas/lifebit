package com.devshot.lifebit.smart.radio.message;

import com.devshot.commons.message.data.BlankResult;
import com.devshot.commons.message.data.CommandConfiguration;
import com.devshot.lifebit.smart.radio.config.RadioConfig;
import com.devshot.lifebit.smart.radio.model.RadioEvent;
import org.springframework.stereotype.Component;

/**
 * @author Oleg Ivashkevich
 * date: 12.09.2020
 */
@Component
public class RadioEventCommandConfiguration extends CommandConfiguration<RadioEvent, BlankResult> {

    public RadioEventCommandConfiguration(RadioConfig config) {
        super("433-event", config.getEventTopic(), false, RadioEvent.class, BlankResult.class);
    }
}
