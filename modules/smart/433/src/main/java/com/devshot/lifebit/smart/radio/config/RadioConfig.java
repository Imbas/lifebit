package com.devshot.lifebit.smart.radio.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author Oleg Ivashkevich
 * date: 03.05.2020
 */
@Getter
@Component
public class RadioConfig {
    @Value("${radio.event.topic}")
    private String eventTopic;
}
