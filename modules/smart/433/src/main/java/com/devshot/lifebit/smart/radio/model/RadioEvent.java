package com.devshot.lifebit.smart.radio.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Oleg Ivashkevich
 * date: 09.05.2020
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RadioEvent {
    private String signal;
    private Long date;
}
