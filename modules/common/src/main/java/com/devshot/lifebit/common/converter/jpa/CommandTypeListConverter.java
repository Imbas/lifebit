package com.devshot.lifebit.common.converter.jpa;

import com.devshot.commons.general.conversion.jpa.ListIdentifiableEnumAttributeConverter;
import com.devshot.lifebit.model.enums.CommandBase;

import javax.persistence.Converter;

/**
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Converter(autoApply = true)
public final class CommandTypeListConverter extends ListIdentifiableEnumAttributeConverter<String, CommandBase> {
}
