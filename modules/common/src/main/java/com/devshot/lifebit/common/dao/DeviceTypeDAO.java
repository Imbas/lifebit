package com.devshot.lifebit.common.dao;

import com.devshot.lifebit.model.enums.DeviceProtocol;
import com.devshot.lifebit.model.pojo.DeviceModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DeviceTypeDAO extends JpaRepository<DeviceModel, Integer> {
    List<DeviceModel> findAllByProtocol(DeviceProtocol protocol);

    Optional<DeviceModel> findByExternalId(String externaId);
}
