package com.devshot.lifebit.common.dao;

import com.devshot.lifebit.model.pojo.ComponentType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ComponentTypeDAO extends JpaRepository<ComponentType, Integer> {
    Optional<ComponentType> findByExternalId(String externaId);
}
