package com.devshot.lifebit.common.converter.jpa;

import com.devshot.commons.general.conversion.jpa.JsonAttributeConverter;
import com.devshot.lifebit.model.domain.command.ScenarioCommand;

import javax.persistence.Converter;
import java.util.List;

/**
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Converter(autoApply = true)
public final class ScenarioCommandListConverter extends JsonAttributeConverter<List<ScenarioCommand>> {
}
