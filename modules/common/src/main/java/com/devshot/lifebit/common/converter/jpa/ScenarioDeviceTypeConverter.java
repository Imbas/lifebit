package com.devshot.lifebit.common.converter.jpa;

import com.devshot.commons.general.conversion.jpa.IdentifiableEnumAttributeConverter;
import com.devshot.lifebit.model.enums.ScenarioDeviceType;

import javax.persistence.Converter;

/**
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Converter(autoApply = true)
public final class ScenarioDeviceTypeConverter extends IdentifiableEnumAttributeConverter<String, ScenarioDeviceType> {
}
