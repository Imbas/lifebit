package com.devshot.lifebit.common.converter.jpa;

import com.devshot.commons.general.conversion.jpa.JsonAttributeConverter;
import com.devshot.lifebit.model.domain.Schedule;

import javax.persistence.Converter;

/**
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Converter(autoApply = true)
public final class ScheduleConverter extends JsonAttributeConverter<Schedule> {
}
