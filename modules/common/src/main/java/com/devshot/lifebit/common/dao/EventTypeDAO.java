package com.devshot.lifebit.common.dao;

import com.devshot.lifebit.model.enums.EventOwnerType;
import com.devshot.lifebit.model.pojo.ComponentType;
import com.devshot.lifebit.model.pojo.EventType;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventTypeDAO extends JpaRepository<EventType, Integer> {
    @Cacheable("eventTypesByComponentType")
    List<EventType> findAllByComponentType(ComponentType type);

    @Cacheable("eventTypesByOwnerType")
    List<EventType> findAllByOwnerType(EventOwnerType type);
}
