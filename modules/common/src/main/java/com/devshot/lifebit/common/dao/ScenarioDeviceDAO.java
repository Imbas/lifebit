package com.devshot.lifebit.common.dao;

import com.devshot.lifebit.model.pojo.ScenarioDevice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ScenarioDeviceDAO extends JpaRepository<ScenarioDevice, Integer> {
}
