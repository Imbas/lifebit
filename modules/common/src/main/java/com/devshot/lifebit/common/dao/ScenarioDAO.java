package com.devshot.lifebit.common.dao;

import com.devshot.lifebit.model.pojo.Scenario;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ScenarioDAO extends JpaRepository<Scenario, Integer> {
    @Override
    @Cacheable("scenarios")
    List<Scenario> findAll();
}
