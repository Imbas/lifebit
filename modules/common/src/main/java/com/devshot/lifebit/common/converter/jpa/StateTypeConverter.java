package com.devshot.lifebit.common.converter.jpa;

import com.devshot.commons.general.conversion.jpa.IdentifiableEnumAttributeConverter;
import com.devshot.lifebit.model.enums.ScenarioDeviceType;
import com.devshot.lifebit.model.enums.StateType;

import javax.persistence.Converter;

/**
 * @author Oleg Ivashkevich
 * date: 14.07.2017
 */
@Converter(autoApply = true)
public final class StateTypeConverter extends IdentifiableEnumAttributeConverter<String, StateType> {
}
